// Package messaging contains utility functions for dealing with a massaging service
package messaging

import (
	"context"

	cloudevents "github.com/cloudevents/sdk-go/v2"
	"gitlab.com/cyverse/cacao-common/common"
)

// QueryEventHandler is a function prototype for query event handlers
type QueryEventHandler func(subject common.QueryOp, transactionID common.TransactionID, jsonData []byte) ([]byte, error)

// QueryCloudEventHandler is a function prototype for query event handlers
type QueryCloudEventHandler func(event *cloudevents.Event) ([]byte, error)

// QueryEventHandlerMapping is a struct that maps query operation and query event handler
type QueryEventHandlerMapping struct {
	Subject common.QueryOp // required, empty string for default handler

	// optional, set CloudEventHandler or EventHandler
	CloudEventHandler QueryCloudEventHandler
	EventHandler      QueryEventHandler
}

// StreamingEventHandler is a function prototype for streaming event handlers
type StreamingEventHandler func(subject common.EventType, transactionID common.TransactionID, jsonData []byte) error

// StreamingCloudEventHandler is a function prototype for streaming event handlers
type StreamingCloudEventHandler func(event *cloudevents.Event) error

// StreamingEventHandlerMapping is a struct that maps event type and event handler
type StreamingEventHandlerMapping struct {
	Subject common.EventType // required, empty string for default handler

	// optional, set CloudEventHandler or EventHandler
	CloudEventHandler StreamingCloudEventHandler
	EventHandler      StreamingEventHandler
}

// QueryEventService is an interface for a query event service (e.g., Nats)
type QueryEventService interface {
	Disconnect() error

	AddEventHandler(subject common.QueryOp, eventHandler QueryEventHandler) error
	AddDefaultEventHandler(eventHandler QueryEventHandler) error
	AddCloudEventHandler(subject common.QueryOp, eventHandler QueryCloudEventHandler) error
	AddDefaultCloudEventHandler(eventHandler QueryCloudEventHandler) error
	Request(ctx context.Context, subject common.QueryOp, data interface{}) ([]byte, error)
	RequestWithTransactionID(ctx context.Context, subject common.QueryOp, data interface{}, transactionID common.TransactionID) ([]byte, error)
	RequestCloudEvent(ctx context.Context, ce *cloudevents.Event) ([]byte, error)
}

// StreamingEventService is an interface for a streaming event service (e.g., Stan)
type StreamingEventService interface {
	Disconnect() error

	AddEventHandler(subject common.EventType, eventHandler StreamingEventHandler) error
	AddDefaultEventHandler(eventHandler StreamingEventHandler) error
	AddCloudEventHandler(subject common.EventType, eventHandler StreamingCloudEventHandler) error
	AddDefaultCloudEventHandler(eventHandler StreamingCloudEventHandler) error
	Publish(subject common.EventType, data interface{}) error
	PublishWithTransactionID(subject common.EventType, data interface{}, transactionID common.TransactionID) error
	PublishCloudEvent(ce *cloudevents.Event) error
}
