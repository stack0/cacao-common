package git

import (
	"fmt"
	"os"
	"sync/atomic"

	billy "github.com/go-git/go-billy/v5"
	memfs "github.com/go-git/go-billy/v5/memfs"
	gogit "github.com/go-git/go-git/v5"
	plumbing "github.com/go-git/go-git/v5/plumbing"
	http "github.com/go-git/go-git/v5/plumbing/transport/http"
	storage "github.com/go-git/go-git/v5/storage"
	memory "github.com/go-git/go-git/v5/storage/memory"
	log "github.com/sirupsen/logrus"
)

// ReferenceType is a type for reference type of git
type ReferenceType string

const (
	// BranchReferenceType is for Branch reference type
	BranchReferenceType ReferenceType = "branch"
	// TagReferenceType is for Tag reference type
	TagReferenceType ReferenceType = "tag"
)

// RepositoryConfig is a configuration struct
type RepositoryConfig struct {
	URL           string
	Username      string // optional
	Password      string // optional
	ReferenceType ReferenceType
	ReferenceName string
}

// StorageType is a type for git local storage
type StorageType string

const (
	// DiskStorageType is a disk storage
	DiskStorageType StorageType = "disk"
	// RAMStorageType is a RAM storage
	RAMStorageType StorageType = "ram"
)

// DiskStorageOption contains options for DiskStorageType
type DiskStorageOption struct {
	// ClonePath is a file path to clone
	ClonePath string
}

// RAMStorageOption contains options for RAMStorageType
type RAMStorageOption struct {
	// SizeLimit is a size limit for data in Bytes, 0 for unlimited
	SizeLimit int64
}

// Clone is a struct for local git repository cloned
type Clone struct {
	Config         *RepositoryConfig
	StorageType    StorageType
	StorageOptions interface{} // Either GitDiskStorageOption or GitRAMStorageOption type
	Repository     *gogit.Repository
	FileSystem     billy.Filesystem
}

// NewGitCloneToDisk creates a clone of a git repository using local disk
func NewGitCloneToDisk(repoConfig *RepositoryConfig, storageOptions *DiskStorageOption) (*Clone, error) {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.git",
		"function": "NewGitCloneToDisk",
	})

	// check if ClonePath already exists
	_, err := os.Stat(storageOptions.ClonePath)
	if !os.IsNotExist(err) {
		// exists
		err = fmt.Errorf("a local clone path %s already exists", storageOptions.ClonePath)
		logger.Error(err)
		return nil, err
	}

	cloneOptions := &gogit.CloneOptions{
		URL:          repoConfig.URL,
		SingleBranch: true,
		Depth:        1,
	}

	// branch or tag
	switch repoConfig.ReferenceType {
	case BranchReferenceType:
		cloneOptions.ReferenceName = plumbing.NewBranchReferenceName(repoConfig.ReferenceName)
	case TagReferenceType:
		cloneOptions.ReferenceName = plumbing.NewTagReferenceName(repoConfig.ReferenceName)
	default:
	}

	// authentication
	if len(repoConfig.Username) > 0 {
		cloneOptions.Auth = &http.BasicAuth{
			Username: repoConfig.Username,
			Password: repoConfig.Password,
		}
	}

	r, err := gogit.PlainClone(storageOptions.ClonePath, false, cloneOptions)
	if err != nil {
		logger.WithError(err).Errorf("failed to clone a repo %s to %s", repoConfig.URL, storageOptions.ClonePath)
		return nil, err
	}

	headRef, err := r.Head()
	if err != nil {
		logger.WithError(err).Error("failed to find HEAD")
		return nil, err
	}

	_, err = r.CommitObject(headRef.Hash())
	if err != nil {
		logger.WithError(err).Errorf("failed to find commit object with hash %s", headRef.Hash().String())
		return nil, err
	}

	tree, err := r.Worktree()
	if err != nil {
		logger.WithError(err).Error("failed to get worktree")
		return nil, err
	}

	return &Clone{
		Config:         repoConfig,
		StorageType:    DiskStorageType,
		StorageOptions: storageOptions,
		Repository:     r,
		FileSystem:     tree.Filesystem,
	}, nil
}

// NewGitCloneToRAM creates a clone of a git repository using RAM
func NewGitCloneToRAM(repoConfig *RepositoryConfig, storageOptions *RAMStorageOption) (*Clone, error) {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.git",
		"function": "NewGitCloneToRAM",
	})

	var store storage.Storer
	if storageOptions.SizeLimit > 0 {
		store = newGitRAMStorage(storageOptions.SizeLimit, true)
	} else {
		store = newGitRAMStorage(0, false)
	}

	cloneOptions := &gogit.CloneOptions{
		URL:          repoConfig.URL,
		SingleBranch: true,
		Depth:        1,
	}

	// branch or tag
	switch repoConfig.ReferenceType {
	case BranchReferenceType:
		cloneOptions.ReferenceName = plumbing.NewBranchReferenceName(repoConfig.ReferenceName)
	case TagReferenceType:
		cloneOptions.ReferenceName = plumbing.NewTagReferenceName(repoConfig.ReferenceName)
	default:
	}

	// authentication
	if len(repoConfig.Username) > 0 {
		cloneOptions.Auth = &http.BasicAuth{
			Username: repoConfig.Username,
			Password: repoConfig.Password,
		}
	}

	fs := memfs.New()

	r, err := gogit.Clone(store, fs, cloneOptions)
	if err != nil {
		logger.WithError(err).Errorf("failed to clone a repo %s to RAM", repoConfig.URL)
		return nil, err
	}

	headRef, err := r.Head()
	if err != nil {
		logger.WithError(err).Error("failed to find HEAD")
		return nil, err
	}

	_, err = r.CommitObject(headRef.Hash())
	if err != nil {
		logger.WithError(err).Errorf("failed to find commit object with hash %s", headRef.Hash().String())
		return nil, err
	}

	return &Clone{
		Config:         repoConfig,
		StorageType:    RAMStorageType,
		StorageOptions: storageOptions,
		Repository:     r,
		FileSystem:     fs,
	}, nil
}

// GetFileSystem returns filesystem to access the git repository
func (clone *Clone) GetFileSystem() billy.Filesystem {
	return clone.FileSystem
}

// Release deletes local git repository and releases all resources
func (clone *Clone) Release() {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.template_repo",
		"function": "GitClone.Release",
	})

	if clone.StorageType == DiskStorageType {
		diskStorageOptions, ok := clone.StorageOptions.(*DiskStorageOption)
		if ok {
			// delete clone directory
			_, err := os.Stat(diskStorageOptions.ClonePath)
			if !os.IsNotExist(err) {
				// exists
				err = os.RemoveAll(diskStorageOptions.ClonePath)
				if err != nil {
					logger.WithError(err).Errorf("failed to delete a local repository %s", diskStorageOptions.ClonePath)
				}
			}
		}
	}
}

// ObjectStorage implements github.com/go-git/go-git/v5/plumbing/storer.EncodedObjectStorer
type ObjectStorage struct {
	limit        *SizeLimit
	enforceLimit bool
	// the original implementation
	memory.ObjectStorage
}

// MemStore augment the original memory storage implementation to set a size limit
type MemStore struct {
	limit        *SizeLimit
	enforceLimit bool

	memory.ConfigStorage
	ObjectStorage
	memory.ShallowStorage
	memory.IndexStorage
	memory.ReferenceStorage
	memory.ModuleStorage
}

// SizeLimit is a struct that stores size limit and current size of data
type SizeLimit struct {
	Limit     int64
	TotalSize int64
}

// IncrementSize increment the size counter atmoically
func (c *SizeLimit) IncrementSize(delta int64) {
	atomic.AddInt64(&c.TotalSize, delta)
}

// ExceeedLimit checks whether limit has been exceeded
func (c SizeLimit) ExceeedLimit() bool {
	return c.TotalSize > c.Limit
}

// SetEncodedObject override the SetEncodedObject implementation from memory.ObjectStorage
func (o *ObjectStorage) SetEncodedObject(obj plumbing.EncodedObject) (plumbing.Hash, error) {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.template_repo",
		"function": "GitClone.Release",
	})

	logger.Trace("reimplemented method called")
	logger.WithField("objSize", obj.Size()).Trace()
	o.limit.IncrementSize(obj.Size())

	if o.enforceLimit && o.limit.ExceeedLimit() {
		logger.WithField("totalSize", o.limit.TotalSize).Warn("Object Total Sizes Exceeed Limit")
		return plumbing.ZeroHash, fmt.Errorf("exceeds total sizes limit")
	}
	return o.ObjectStorage.SetEncodedObject(obj)
}

func newGitRAMStorage(limit int64, enforceLimit bool) storage.Storer {
	original := memory.NewStorage()
	sizeLimit := &SizeLimit{Limit: limit}
	storage := &MemStore{
		limit:         sizeLimit,
		enforceLimit:  enforceLimit,
		ConfigStorage: original.ConfigStorage,
		ObjectStorage: ObjectStorage{
			limit:         sizeLimit,
			enforceLimit:  enforceLimit,
			ObjectStorage: original.ObjectStorage,
		},
		ShallowStorage:   original.ShallowStorage,
		IndexStorage:     original.IndexStorage,
		ReferenceStorage: original.ReferenceStorage,
		ModuleStorage:    original.ModuleStorage,
	}
	return storage
}
