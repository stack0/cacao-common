package service

import (
	"encoding/json"
	cloudevents "github.com/cloudevents/sdk-go/v2"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging"
	"time"
)

// StanDependencyMediatorClient is an implementation of DependencyMediatorClient over STAN
type StanDependencyMediatorClient struct {
	natsConfig messaging.NatsConfig
	stanConfig messaging.StanConfig
}

// NewDependencyMediatorClient ...
func NewDependencyMediatorClient(natsConfig messaging.NatsConfig, stanConfig messaging.StanConfig) (DependencyMediatorClient, error) {
	return StanDependencyMediatorClient{natsConfig: natsConfig, stanConfig: stanConfig}, nil
}

// DeleteCredential attempts to delete a credential while ensuring the dependency integrity. Deletion will not succeed if there is any other objects (deployments, templates) that depends on this credential.
func (dm StanDependencyMediatorClient) DeleteCredential(session Session, credID string) (DepCheckDeletionResult, error) {
	if credID == "" {
		return DepCheckDeletionResult{}, NewCacaoInvalidParameterError("bad credential ID")
	}
	response, err := dm.tryDelete(session, EventDepMedDeleteCredential, string(credID))
	if err != nil {
		return DepCheckDeletionResult{}, err
	}
	return dm.responseToResult(response)
}

// DeleteTemplate attempts to delete a template while ensuring the dependency integrity. Deletion will not succeed if there is any other objects (deployments, providers) that depends on this template.
func (dm StanDependencyMediatorClient) DeleteTemplate(session Session, template common.ID) (DepCheckDeletionResult, error) {
	if !template.Validate() {
		return DepCheckDeletionResult{}, NewCacaoInvalidParameterError("bad template ID")
	}
	response, err := dm.tryDelete(session, EventDepMedDeleteTemplate, string(template))
	if err != nil {
		return DepCheckDeletionResult{}, err
	}
	return dm.responseToResult(response)
}

// DeleteProvider attempts to delete a provider while ensuring the dependency integrity.
// FIXME provider currently will mostly likely always has deployments depend on it. But there is no way to check if
// deployments of other user(not the owner/creator of the provider) are using the provider, so it is not possible to
// do a thorough check on dependents.
func (dm StanDependencyMediatorClient) DeleteProvider(session Session, provider common.ID) (DepCheckDeletionResult, error) {
	if !provider.Validate() {
		return DepCheckDeletionResult{}, NewCacaoInvalidParameterError("bad provider ID")
	}
	log.Debug("provider is assumed to always have dependents, therefore cannot be deleted")
	return DepCheckDeletionResult{
		HasDependent: true, // FIXME for now, always assume that the provider has dependents.
		Deleted:      false,
	}, nil
}

// DeleteWorkspace attempts to delete a workspace while ensuring the dependency integrity. Deletion will not succeed if there is any other objects (deployments) that depends on this template.
func (dm StanDependencyMediatorClient) DeleteWorkspace(session Session, workspace common.ID) (DepCheckDeletionResult, error) {
	if !workspace.Validate() {
		return DepCheckDeletionResult{}, NewCacaoInvalidParameterError("bad workspace ID")
	}
	response, err := dm.tryDelete(session, EventDepMedDeleteWorkspace, string(workspace))
	if err != nil {
		return DepCheckDeletionResult{}, err
	}
	return dm.responseToResult(response)
}

func (dm StanDependencyMediatorClient) tryDelete(session Session, eventType common.EventType, id string) (DepCheckDeletionResponse, error) {
	conn, err := dm.connectStan()
	if err != nil {
		return DepCheckDeletionResponse{}, err
	}

	responseChan, err := dm.listenForResponse(conn, dm.responseEventTypeMapping(eventType))
	if err != nil {
		return DepCheckDeletionResponse{}, err
	}
	err = dm.sendRequest(conn, eventType, DepCheckDeletionRequest{
		session,
		id,
	})
	if err != nil {
		return DepCheckDeletionResponse{}, err
	}
	response, err := dm.waitForResponse(responseChan)
	if err != nil {
		return DepCheckDeletionResponse{}, err
	}
	return response, nil
}

func (dm StanDependencyMediatorClient) connectStan() (*messaging.StanConnection, error) {
	return messaging.ConnectStanForService(&dm.natsConfig, &dm.stanConfig, []messaging.StreamingEventHandlerMapping{})
}

func (dm StanDependencyMediatorClient) responseEventTypeMapping(requestEventType common.EventType) common.EventTypeSet {
	switch requestEventType {
	case EventDepMedDeleteProvider:
		return common.NewEventTypeSet(EventDepMedProviderDeletionErrored, EventDepMedProviderHasDependent, EventDepMedProviderDeleted)
	case EventDepMedDeleteTemplate:
		return common.NewEventTypeSet(EventDepMedTemplateDeletionErrored, EventDepMedTemplateHasDependent, EventDepMedTemplateDeleted)
	case EventDepMedDeleteCredential:
		return common.NewEventTypeSet(EventDepMedCredentialDeletionErrored, EventDepMedCredentialHasDependent, EventDepMedCredentialDeleted)
	case EventDepMedDeleteWorkspace:
		return common.NewEventTypeSet(EventDepMedWorkspaceDeletionErrored, EventDepMedWorkspaceHasDependent, EventDepMedWorkspaceDeleted)
	default:
		panic("bad request event type, " + requestEventType)
	}
}

func (dm StanDependencyMediatorClient) listenForResponse(conn *messaging.StanConnection, eventTypeSet common.EventTypeSet) (<-chan DepCheckDeletionResponse, error) {
	respChan := make(chan DepCheckDeletionResponse)
	err := conn.AddDefaultCloudEventHandler(func(event *cloudevents.Event) error {
		_, ok := eventTypeSet[common.EventType(event.Type())]
		if !ok {
			return nil
		}
		var resp DepCheckDeletionResponse
		err := json.Unmarshal(event.Data(), &resp)
		if err != nil {
			log.WithError(err).Errorf("fail to parse event as DepCheckDeletionResponse")
			return nil
		}
		respChan <- resp
		return nil
	})
	if err != nil {
		return nil, err
	}
	return respChan, nil
}

func (dm StanDependencyMediatorClient) sendRequest(conn *messaging.StanConnection, requestEventType common.EventType, request DepCheckDeletionRequest) error {
	ce, err := messaging.CreateCloudEventWithTransactionID(request, string(requestEventType), "dependency-mediator-client", messaging.NewTransactionID())
	if err != nil {
		return err
	}
	err = conn.PublishCloudEvent(&ce)
	if err != nil {
		return err
	}
	return nil
}

func (dm StanDependencyMediatorClient) waitForResponse(respChan <-chan DepCheckDeletionResponse) (DepCheckDeletionResponse, error) {
	select {
	case resp := <-respChan:
		return resp, nil
	case <-time.After(time.Second * 10):
		return DepCheckDeletionResponse{}, NewCacaoTimeoutError("timeout on waiting for response from dependency mediator")
	}
}

func (dm StanDependencyMediatorClient) responseToResult(response DepCheckDeletionResponse) (DepCheckDeletionResult, error) {
	if response.Errored() {
		if response.Deleted {
			log.WithFields(log.Fields{
				"package":  "service",
				"function": "StanDependencyMediatorClient.responseToResult",
				"response": response,
			}).Error("deletion response should not be deleted and errored")
		}
		return DepCheckDeletionResult{
			HasDependent: response.HasDependent,
			Deleted:      response.Deleted,
		}, response.GetServiceError()
	}
	return DepCheckDeletionResult{
		HasDependent: response.HasDependent,
		Deleted:      response.Deleted,
	}, nil
}

// prefix for dependency mediator events
const depMedPrefix common.EventType = common.EventTypePrefix + "dependencyMediator."

const (
	// EventDepMedDeleteProvider is event type for deleting a provider with dependent checking
	EventDepMedDeleteProvider = depMedPrefix + "deleteProvider"

	EventDepMedProviderDeletionErrored = depMedPrefix + "providerDeletionErrored"
	EventDepMedProviderHasDependent    = depMedPrefix + "providerHasDependent"
	EventDepMedProviderDeleted         = depMedPrefix + "providerDeleted"
)

const (
	// EventDepMedDeleteTemplate is event type for deleting a template with dependent checking
	EventDepMedDeleteTemplate = depMedPrefix + "deleteTemplate"

	EventDepMedTemplateDeletionErrored = depMedPrefix + "templateDeletionErrored"
	EventDepMedTemplateHasDependent    = depMedPrefix + "templateHasDependent"
	EventDepMedTemplateDeleted         = depMedPrefix + "templateDeleted"
)

const (
	// EventDepMedDeleteWorkspace is event type for deleting a workspace with dependent checking
	EventDepMedDeleteWorkspace = depMedPrefix + "deleteWorkspace"

	EventDepMedWorkspaceDeletionErrored = depMedPrefix + "workspaceDeletionErrored"
	EventDepMedWorkspaceHasDependent    = depMedPrefix + "workspaceHasDependent"
	EventDepMedWorkspaceDeleted         = depMedPrefix + "workspaceDeleted"
)

const (
	// EventDepMedDeleteCredential is event type for deleting a credential with dependent checking
	EventDepMedDeleteCredential = depMedPrefix + "deleteCredential"

	EventDepMedCredentialDeletionErrored = depMedPrefix + "credentialDeletionErrored"
	EventDepMedCredentialHasDependent    = depMedPrefix + "credentialHasDependent"
	EventDepMedCredentialDeleted         = depMedPrefix + "credentialDeleted"
)

// DepCheckDeletionRequest is event body for deletion request with dependent checking.
// Note: the type of object to be deleted is not specified by fields in this struct, but by the event type.
type DepCheckDeletionRequest struct {
	Session
	ID string `json:"ID"`
}

// DepCheckDeletionResponse is event body for response to deletion request with dependent checking.
// Note: the type of object to be deleted is not specified by fields in this struct, but by the event type.
type DepCheckDeletionResponse struct {
	Session
	ID           string `json:"ID"`
	Deleted      bool   `json:"deleted"`
	HasDependent bool   `json:"has_dependent"`
}

// Errored return true if the response has errored
func (resp DepCheckDeletionResponse) Errored() bool {
	if resp.Session.ServiceError.StandardMessage != "" || resp.Session.ServiceError.ContextualMessage != "" {
		return true
	}
	return false
}
