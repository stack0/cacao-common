package service

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	cloudevents "github.com/cloudevents/sdk-go/v2"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging"
)

// Nats subjects
const (
	// NatsSubjectTemplate is the prefix of the queue name for Template queries
	NatsSubjectTemplate common.QueryOp = common.NatsQueryOpPrefix + "template"

	/////////////////////////////////////////////////////////////////////////
	// TemplateSourceType
	/////////////////////////////////////////////////////////////////////////
	// TemplateSourceTypeListQueryOp is the queue name for TemplateSourceTypeList query
	TemplateSourceTypeListQueryOp common.QueryOp = NatsSubjectTemplate + ".listSourceTypes"

	/////////////////////////////////////////////////////////////////////////
	// TemplateType
	/////////////////////////////////////////////////////////////////////////
	// TemplateTypeListQueryOp is the queue name for TemplateTypeList query
	TemplateTypeListQueryOp common.QueryOp = NatsSubjectTemplate + ".listTypes"
	// TemplateTypeGetQueryOp is the queue name for TemplateTypeGet query
	TemplateTypeGetQueryOp common.QueryOp = NatsSubjectTemplate + ".getType"

	// TemplateTypeCreateRequestedEvent is the cloudevent subject for TemplateTypeCreate
	TemplateTypeCreateRequestedEvent common.EventType = common.EventTypePrefix + "TemplateTypeCreateRequested"
	// TemplateTypeDeleteRequestedEvent is the cloudevent subject for TemplateTypeDelete
	TemplateTypeDeleteRequestedEvent common.EventType = common.EventTypePrefix + "TemplateTypeDeleteRequested"
	// TemplateTypeUpdateRequestedEvent is the cloudevent subject for TemplateTypeUpdate
	TemplateTypeUpdateRequestedEvent common.EventType = common.EventTypePrefix + "TemplateTypeUpdateRequested"

	// TemplateTypeCreatedEvent is the cloudevent subject for TemplateTypeCreated
	TemplateTypeCreatedEvent common.EventType = common.EventTypePrefix + "TemplateTypeCreated"
	// TemplateTypeDeletedEvent is the cloudevent subject for TemplateTypeDeleted
	TemplateTypeDeletedEvent common.EventType = common.EventTypePrefix + "TemplateTypeDeleted"
	// TemplateTypeUpdatedEvent is the loudevent subject for TemplateTypeUpdated
	TemplateTypeUpdatedEvent common.EventType = common.EventTypePrefix + "TemplateTypeUpdated"

	// TemplateTypeCreateFailedEvent is the cloudevent subject for TemplateTypeCreateFailed
	TemplateTypeCreateFailedEvent common.EventType = common.EventTypePrefix + "TemplateTypeCreateFailed"
	// TemplateTypeDeleteFailedEvent is the eloudevent subject for TemplateTypeDeleteFailed
	TemplateTypeDeleteFailedEvent common.EventType = common.EventTypePrefix + "TemplateTypeDeleteFailed"
	// TemplateTypeUpdateFailedEvent is the loudevent subject for TemplateTypeUpdateFailed
	TemplateTypeUpdateFailedEvent common.EventType = common.EventTypePrefix + "TemplateTypeUpdateFailed"

	/////////////////////////////////////////////////////////////////////////
	// Template
	/////////////////////////////////////////////////////////////////////////
	// TemplateListQueryOp is the queue name for TemplateList query
	TemplateListQueryOp common.QueryOp = NatsSubjectTemplate + ".list"
	// TemplateGetQueryOp is the queue name for TemplateGet query
	TemplateGetQueryOp common.QueryOp = NatsSubjectTemplate + ".get"

	// TemplateImportRequestedEvent is the cloudevent subject for TemplateImport
	TemplateImportRequestedEvent common.EventType = common.EventTypePrefix + "TemplateImportRequested"
	// TemplateDeleteRequestedEvent is the cloudevent subject for TemplateDelete
	TemplateDeleteRequestedEvent common.EventType = common.EventTypePrefix + "TemplateDeleteRequested"
	// TemplateUpdateRequestedEvent is the cloudevent subject for TemplateUpdate
	TemplateUpdateRequestedEvent common.EventType = common.EventTypePrefix + "TemplateUpdateRequested"
	// TemplateSyncRequestedEvent is the cloudevent subject for TemplateSync
	TemplateSyncRequestedEvent common.EventType = common.EventTypePrefix + "TemplateSyncRequested"

	// TemplateImportedEvent is the cloudevent subject for TemplateImported
	TemplateImportedEvent common.EventType = common.EventTypePrefix + "TemplateImported"
	// TemplateDeletedEvent is the cloudevent subject for TemplateDeleted
	TemplateDeletedEvent common.EventType = common.EventTypePrefix + "TemplateDeleted"
	// TemplateUpdatedEvent is the cloudevent subject for TemplateUpdated
	TemplateUpdatedEvent common.EventType = common.EventTypePrefix + "TemplateUpdated"
	// TemplateSyncedEvent is the cloudevent subject for TemplateSynced
	TemplateSyncedEvent common.EventType = common.EventTypePrefix + "TemplateSynced"

	// TemplateImportFailedEvent is the cloudevent subject for TemplateImportFailed
	TemplateImportFailedEvent common.EventType = common.EventTypePrefix + "TemplateImportFailed"
	// TemplateDeleteFailedEvent is the cloudevent subject for TemplateDeleteFailed
	TemplateDeleteFailedEvent common.EventType = common.EventTypePrefix + "TemplateDeleteFailed"
	// TemplateUpdateFailedEvent is the cloudevent subject for TemplateUpdateFailed
	TemplateUpdateFailedEvent common.EventType = common.EventTypePrefix + "TemplateUpdateFailed"
	// TemplateSyncFailedEvent is the cloudevent subject for TemplateSyncFailed
	TemplateSyncFailedEvent common.EventType = common.EventTypePrefix + "TemplateSyncFailed"
)

// Nats Client
type natsTemplateClient struct {
	actor      string
	emulator   string
	natsConfig messaging.NatsConfig
	stanConfig messaging.StanConfig
	ctx        context.Context
}

// NewNatsTemplateClient creates a client for template miroservice that uses NATS for communication
func NewNatsTemplateClient(ctx context.Context, actor string, emulator string, natsConfig messaging.NatsConfig, stanConfig messaging.StanConfig) (TemplateClient, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "NewNatsTemplateClient",
	})

	logger.Trace("Creating a NatsTemplateClient")

	if len(actor) == 0 {
		return nil, NewCacaoInvalidParameterError("actor is empty")
	}

	if ctx == nil {
		ctx = context.Background()
	}

	natsConfigCopy := natsConfig
	if natsConfigCopy.RequestTimeout == -1 {
		natsConfigCopy.RequestTimeout = int(messaging.DefaultNatsRequestTimeout.Seconds())
	}

	stanConfigCopy := stanConfig
	if stanConfigCopy.EventsTimeout == -1 {
		stanConfigCopy.EventsTimeout = int(messaging.DefaultStanEventsTimeout.Seconds())
	}

	client := natsTemplateClient{
		actor:      actor,
		emulator:   emulator,
		natsConfig: natsConfigCopy,
		stanConfig: stanConfigCopy,
		ctx:        ctx,
	}

	return &client, nil
}

func waitTemplateTypeResponse(c chan TemplateTypeModel, timeout time.Duration) (bool, TemplateTypeModel) {
	select {
	case res := <-c:
		return true, res
	case <-time.After(timeout):
		return false, TemplateTypeModel{}
	}
}

func waitTemplateResponse(c chan TemplateModel, timeout time.Duration) (bool, TemplateModel) {
	select {
	case res := <-c:
		return true, res
	case <-time.After(timeout):
		return false, TemplateModel{}
	}
}

// ListSourceTypes returns all template source types
func (client *natsTemplateClient) ListSourceTypes() ([]TemplateSourceType, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsTemplateClient.ListSourceTypes",
	})

	natsConn, err := messaging.ConnectNatsForServiceClient(&client.natsConfig)
	if err != nil {
		errorMessage := "Unable to connect to NATS"
		logger.WithError(err).Error(errorMessage)
		return nil, NewCacaoCommunicationError(errorMessage)
	}

	defer natsConn.Disconnect()

	request := Session{
		SessionActor:    client.actor,
		SessionEmulator: client.emulator,
	}

	timeout := time.Duration(client.natsConfig.RequestTimeout) * time.Second
	timeoutCtx, cancel := context.WithTimeout(client.ctx, timeout)
	defer cancel()

	responseBytes, err := natsConn.Request(timeoutCtx, TemplateSourceTypeListQueryOp, request)
	if err != nil {
		errorMessage := "unable to request a list template source types event"
		logger.WithError(err).Error(errorMessage)
		return nil, NewCacaoCommunicationError(errorMessage)
	}

	var templateSourceTypeListModel TemplateSourceTypeListModel
	err = json.Unmarshal(responseBytes, &templateSourceTypeListModel)
	if err != nil {
		errorMessage := "unable to unmarshal response JSON to template source type list object"
		logger.WithError(err).Error(errorMessage)
		return nil, NewCacaoMarshalError(errorMessage)
	}

	serviceError := templateSourceTypeListModel.GetServiceError()
	if serviceError != nil {
		logger.Error(serviceError)
		return nil, serviceError
	}

	return templateSourceTypeListModel.GetTemplateSourceTypes(), nil
}

// GetType returns the template type with the given template type name
func (client *natsTemplateClient) GetType(templateTypeName string) (TemplateType, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsTemplateClient.GetType",
	})

	if len(templateTypeName) == 0 {
		return nil, NewCacaoInvalidParameterError("invalid Template type name")
	}

	natsConn, err := messaging.ConnectNatsForServiceClient(&client.natsConfig)
	if err != nil {
		errorMessage := "unable to connect to NATS"
		logger.WithError(err).Error(errorMessage)
		return nil, NewCacaoCommunicationError(errorMessage)
	}

	defer natsConn.Disconnect()

	request := TemplateTypeModel{
		Session: Session{
			SessionActor:    client.actor,
			SessionEmulator: client.emulator,
		},
		Name: templateTypeName,
	}

	timeout := time.Duration(client.natsConfig.RequestTimeout) * time.Second
	timeoutCtx, cancel := context.WithTimeout(client.ctx, timeout)
	defer cancel()

	responseBytes, err := natsConn.Request(timeoutCtx, TemplateTypeGetQueryOp, request)
	if err != nil {
		errorMessage := "unable to request a get template event"
		logger.WithError(err).Error(errorMessage)
		return nil, NewCacaoCommunicationError(errorMessage)
	}

	var templateTypeModel TemplateTypeModel
	err = json.Unmarshal(responseBytes, &templateTypeModel)
	if err != nil {
		errorMessage := "unable to unmarshal response JSON to template type object"
		logger.WithError(err).Error(errorMessage)
		return nil, NewCacaoMarshalError(errorMessage)
	}

	serviceError := templateTypeModel.GetServiceError()
	if serviceError != nil {
		logger.Error(serviceError)
		return nil, serviceError
	}
	return &templateTypeModel, nil
}

// ListTypes returns all template types for the user
func (client *natsTemplateClient) ListTypes() ([]TemplateType, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsTemplateClient.ListTypes",
	})

	natsConn, err := messaging.ConnectNatsForServiceClient(&client.natsConfig)
	if err != nil {
		errorMessage := "unable to connect to NATS"
		logger.WithError(err).Error(errorMessage)
		return nil, NewCacaoCommunicationError(errorMessage)
	}

	defer natsConn.Disconnect()

	request := TemplateTypeModel{
		Session: Session{
			SessionActor:    client.actor,
			SessionEmulator: client.emulator,
		},
		ProviderTypes: []TemplateProviderType{},
	}

	timeout := time.Duration(client.natsConfig.RequestTimeout) * time.Second
	timeoutCtx, cancel := context.WithTimeout(client.ctx, timeout)
	defer cancel()

	responseBytes, err := natsConn.Request(timeoutCtx, TemplateTypeListQueryOp, request)
	if err != nil {
		errorMessage := "unable to request a list template type event"
		logger.WithError(err).Error(errorMessage)
		return nil, NewCacaoCommunicationError(errorMessage)
	}

	var templateTypeListModel TemplateTypeListModel
	err = json.Unmarshal(responseBytes, &templateTypeListModel)
	if err != nil {
		errorMessage := "unable to unmarshal response JSON to template type list object"
		logger.WithError(err).Error(errorMessage)
		return nil, NewCacaoMarshalError(errorMessage)
	}

	serviceError := templateTypeListModel.GetServiceError()
	if serviceError != nil {
		logger.Error(serviceError)
		return nil, serviceError
	}

	return templateTypeListModel.GetTemplateTypes(), nil
}

// ListTypesForProviderType returns all template types that support given provider type
func (client *natsTemplateClient) ListTypesForProviderType(providerType TemplateProviderType) ([]TemplateType, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsTemplateClient.ListTypesForProviderType",
	})

	if len(providerType) == 0 {
		return nil, NewCacaoInvalidParameterError("invalid Provider type")
	}

	natsConn, err := messaging.ConnectNatsForServiceClient(&client.natsConfig)
	if err != nil {
		errorMessage := "unable to connect to NATS"
		logger.WithError(err).Error()
		return nil, NewCacaoCommunicationError(errorMessage)
	}

	defer natsConn.Disconnect()

	request := TemplateTypeModel{
		Session: Session{
			SessionActor:    client.actor,
			SessionEmulator: client.emulator,
		},
		ProviderTypes: []TemplateProviderType{
			providerType,
		},
	}

	timeout := time.Duration(client.natsConfig.RequestTimeout) * time.Second
	timeoutCtx, cancel := context.WithTimeout(client.ctx, timeout)
	defer cancel()

	responseBytes, err := natsConn.Request(timeoutCtx, TemplateTypeListQueryOp, request)
	if err != nil {
		errorMessage := "unable to request a list template types event"
		logger.WithError(err).Error(errorMessage)
		return nil, NewCacaoCommunicationError(errorMessage)
	}

	var templateTypeListModel TemplateTypeListModel
	err = json.Unmarshal(responseBytes, &templateTypeListModel)
	if err != nil {
		errorMessage := "unable to unmarshal response JSON to template type list object"
		logger.WithError(err).Error(errorMessage)
		return nil, NewCacaoMarshalError(errorMessage)
	}

	serviceError := templateTypeListModel.GetServiceError()
	if serviceError != nil {
		logger.Error(serviceError)
		return nil, serviceError
	}

	return templateTypeListModel.GetTemplateTypes(), nil
}

// Create creates a new template type
func (client *natsTemplateClient) CreateType(templateType TemplateType) error {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsTemplateClient.CreateType",
	})

	if len(templateType.GetName()) == 0 {
		return NewCacaoInvalidParameterError("invalid Template Name")
	}

	if len(templateType.GetFormats()) == 0 {
		return NewCacaoInvalidParameterError("formats are empty")
	}

	if len(templateType.GetEngine()) == 0 {
		return NewCacaoInvalidParameterError("invalid Template Engine")
	}

	if len(templateType.GetProviderTypes()) == 0 {
		return NewCacaoInvalidParameterError("provider types are empty")
	}

	SenderNatsConfig := client.natsConfig
	SenderNatsConfig.ClientID = SenderNatsConfig.ClientID + "-" + string(common.NewID("Sender"))

	stanConn, err := messaging.ConnectStanForServiceClient(&SenderNatsConfig, &client.stanConfig)
	if err != nil {
		errorMessage := "unable to connect to NATS Streaming"
		logger.WithError(err).Error(errorMessage)
		return NewCacaoCommunicationError(errorMessage)
	}

	defer stanConn.Disconnect()

	eventSourceNatsConfig := client.natsConfig
	eventSourceNatsConfig.ClientID = eventSourceNatsConfig.ClientID + "-" + string(common.NewID("EventSource"))

	eventSource := messaging.NewEventSource(eventSourceNatsConfig, client.stanConfig)
	if err != nil {
		errorMessage := "unable to create EventSource"
		logger.WithError(err).Error(errorMessage)
		return NewCacaoCommunicationError(errorMessage)
	}

	request := TemplateTypeModel{
		Session: Session{
			SessionActor:    client.actor,
			SessionEmulator: client.emulator,
		},
		Name:          templateType.GetName(),
		Formats:       templateType.GetFormats(),
		Engine:        templateType.GetEngine(),
		ProviderTypes: templateType.GetProviderTypes(),
	}

	eventsSubscribe := []common.EventType{
		TemplateTypeCreatedEvent,
		TemplateTypeCreateFailedEvent,
	}

	transactionID := messaging.NewTransactionID()

	responseChannel := make(chan TemplateTypeModel)

	callback := func(ev common.EventType, ce cloudevents.Event) {
		if ev == TemplateTypeCreatedEvent || ev == TemplateTypeCreateFailedEvent {
			var templateTypeModel TemplateTypeModel
			err := json.Unmarshal(ce.Data(), &templateTypeModel)
			if err != nil {
				errorMessage := "unable to unmarshal a template type create response"
				logger.WithError(err).Error(errorMessage)
				responseChannel <- TemplateTypeModel{
					Session: Session{
						SessionActor:    client.actor,
						SessionEmulator: client.emulator,
						ServiceError:    NewCacaoMarshalError(errorMessage).GetBase(),
					},
				}
				return
			}

			responseChannel <- templateTypeModel
			return
		}

		errorMessage := fmt.Sprintf("unknown event type %s", ev)
		responseChannel <- TemplateTypeModel{
			Session: Session{
				SessionActor:    client.actor,
				SessionEmulator: client.emulator,
				ServiceError:    NewCacaoCommunicationError(errorMessage).GetBase(),
			},
		}
	}

	listener := messaging.Listener{
		Callback:   callback,
		ListenOnce: true,
	}

	listenerID, err := eventSource.AddListenerMultiEventType(eventsSubscribe, transactionID, listener)
	if err != nil {
		errorMessage := "unable to add an event listener"
		logger.WithError(err).Error(errorMessage)
		return NewCacaoCommunicationError(errorMessage)
	}
	defer eventSource.RemoveListenerByID(listenerID)

	err = stanConn.PublishWithTransactionID(TemplateTypeCreateRequestedEvent, request, transactionID)
	if err != nil {
		errorMessage := "unable to publish a create template type event"
		logger.WithError(err).Error()
		return NewCacaoCommunicationError(errorMessage)
	}

	// wait for response
	responseReceived, response := waitTemplateTypeResponse(responseChannel, time.Duration(client.natsConfig.RequestTimeout)*time.Second)
	if responseReceived {
		serviceError := response.GetServiceError()
		if serviceError == nil {
			return nil
		}

		logger.Error(serviceError)
		return serviceError
	}

	// timed out
	errorMessage := "unable to create a template type - template service is not responding"
	logger.Error(errorMessage)
	return NewCacaoTimeoutError(errorMessage)
}

// UpdateType updates the template type with the template type name
func (client *natsTemplateClient) UpdateType(templateType TemplateType) error {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsTemplateClient.UpdateType",
	})

	if len(templateType.GetName()) == 0 {
		return NewCacaoInvalidParameterError("invalid TemplateType Name")
	}

	if len(templateType.GetFormats()) == 0 {
		return NewCacaoInvalidParameterError("formats are empty")
	}

	if len(templateType.GetEngine()) == 0 {
		return NewCacaoInvalidParameterError("invalid Template Engine")
	}

	if len(templateType.GetProviderTypes()) == 0 {
		return NewCacaoInvalidParameterError("provider types are empty")
	}

	SenderNatsConfig := client.natsConfig
	SenderNatsConfig.ClientID = SenderNatsConfig.ClientID + "-" + string(common.NewID("Sender"))

	stanConn, err := messaging.ConnectStanForServiceClient(&SenderNatsConfig, &client.stanConfig)
	if err != nil {
		errorMessage := "unable to connect to NATS Streaming"
		logger.WithError(err).Error(errorMessage)
		return NewCacaoCommunicationError(errorMessage)
	}

	defer stanConn.Disconnect()

	eventSourceNatsConfig := client.natsConfig
	eventSourceNatsConfig.ClientID = eventSourceNatsConfig.ClientID + "-" + string(common.NewID("EventSource"))

	eventSource := messaging.NewEventSource(eventSourceNatsConfig, client.stanConfig)
	if err != nil {
		errorMessage := "unable to create EventSource"
		logger.WithError(err).Error(errorMessage)
		return NewCacaoCommunicationError(errorMessage)
	}

	request := TemplateTypeModel{
		Session: Session{
			SessionActor:    client.actor,
			SessionEmulator: client.emulator,
		},
		Name:          templateType.GetName(),
		Formats:       templateType.GetFormats(),
		Engine:        templateType.GetEngine(),
		ProviderTypes: templateType.GetProviderTypes(),
	}

	eventsSubscribe := []common.EventType{
		TemplateTypeUpdatedEvent,
		TemplateTypeUpdateFailedEvent,
	}

	transactionID := messaging.NewTransactionID()

	responseChannel := make(chan TemplateTypeModel)

	callback := func(ev common.EventType, ce cloudevents.Event) {
		if ev == TemplateTypeUpdatedEvent || ev == TemplateTypeUpdateFailedEvent {
			var templateTypeModel TemplateTypeModel
			err := json.Unmarshal(ce.Data(), &templateTypeModel)
			if err != nil {
				errorMessage := "unable to unmarshal a template type update response"
				logger.WithError(err).Error(errorMessage)
				responseChannel <- TemplateTypeModel{
					Session: Session{
						SessionActor:    client.actor,
						SessionEmulator: client.emulator,
						ServiceError:    NewCacaoMarshalError(errorMessage).GetBase(),
					},
				}
				return
			}

			responseChannel <- templateTypeModel
			return
		}

		errorMessage := fmt.Sprintf("unknown event type %s", ev)
		responseChannel <- TemplateTypeModel{
			Session: Session{
				SessionActor:    client.actor,
				SessionEmulator: client.emulator,
				ServiceError:    NewCacaoCommunicationError(errorMessage).GetBase(),
			},
		}
	}

	listener := messaging.Listener{
		Callback:   callback,
		ListenOnce: true,
	}

	listenerID, err := eventSource.AddListenerMultiEventType(eventsSubscribe, transactionID, listener)
	if err != nil {
		errorMessage := "unable to add an event listener"
		logger.WithError(err).Error(errorMessage)
		return NewCacaoCommunicationError(errorMessage)
	}
	defer eventSource.RemoveListenerByID(listenerID)

	err = stanConn.PublishWithTransactionID(TemplateTypeUpdateRequestedEvent, request, transactionID)
	if err != nil {
		errorMessage := "unable to publish a update template type event"
		logger.WithError(err).Error(errorMessage)
		return NewCacaoCommunicationError(errorMessage)
	}

	// wait for response
	responseReceived, response := waitTemplateTypeResponse(responseChannel, time.Duration(client.natsConfig.RequestTimeout)*time.Second)
	if responseReceived {
		serviceError := response.GetServiceError()
		if serviceError == nil {
			return nil
		}

		logger.Error(serviceError)
		return serviceError
	}

	// timed out
	errorMessage := "unable to update a template type - template service is not responding"
	logger.Error(errorMessage)
	return NewCacaoTimeoutError(errorMessage)
}

// UpdateTypeFields updates the selected fields of the template type with the template type name
func (client *natsTemplateClient) UpdateTypeFields(templateType TemplateType, updateFieldNames []string) error {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsTemplateClient.UpdateTypeFields",
	})

	if len(templateType.GetName()) == 0 {
		return NewCacaoInvalidParameterError("invalid TemplateType Name")
	}

	if len(updateFieldNames) == 0 {
		return nil
	}

	for _, updateFieldName := range updateFieldNames {
		switch updateFieldName {
		case "formats":
			if len(templateType.GetFormats()) == 0 {
				return NewCacaoInvalidParameterError("formats are empty")
			}
		case "engine":
			if len(templateType.GetEngine()) == 0 {
				return NewCacaoInvalidParameterError("invalid Template Engine")
			}
		case "provider_types":
			if len(templateType.GetProviderTypes()) == 0 {
				return NewCacaoInvalidParameterError("provider types are empty")
			}
		default:
			//pass
		}
	}

	SenderNatsConfig := client.natsConfig
	SenderNatsConfig.ClientID = SenderNatsConfig.ClientID + "-" + string(common.NewID("Sender"))

	stanConn, err := messaging.ConnectStanForServiceClient(&SenderNatsConfig, &client.stanConfig)
	if err != nil {
		errorMessage := "unable to connect to NATS Streaming"
		logger.WithError(err).Error(errorMessage)
		return NewCacaoCommunicationError(errorMessage)
	}

	defer stanConn.Disconnect()

	eventSourceNatsConfig := client.natsConfig
	eventSourceNatsConfig.ClientID = eventSourceNatsConfig.ClientID + "-" + string(common.NewID("EventSource"))

	eventSource := messaging.NewEventSource(eventSourceNatsConfig, client.stanConfig)
	if err != nil {
		errorMessage := "unable to create EventSource"
		logger.WithError(err).Error(errorMessage)
		return NewCacaoCommunicationError(errorMessage)
	}

	request := TemplateTypeModel{
		Session: Session{
			SessionActor:    client.actor,
			SessionEmulator: client.emulator,
		},
		Name:             templateType.GetName(),
		Formats:          templateType.GetFormats(),
		Engine:           templateType.GetEngine(),
		ProviderTypes:    templateType.GetProviderTypes(),
		UpdateFieldNames: updateFieldNames,
	}

	eventsSubscribe := []common.EventType{
		TemplateTypeUpdatedEvent,
		TemplateTypeUpdateFailedEvent,
	}

	transactionID := messaging.NewTransactionID()

	responseChannel := make(chan TemplateTypeModel)

	callback := func(ev common.EventType, ce cloudevents.Event) {
		if ev == TemplateTypeUpdatedEvent || ev == TemplateTypeUpdateFailedEvent {
			var templateTypeModel TemplateTypeModel
			err := json.Unmarshal(ce.Data(), &templateTypeModel)
			if err != nil {
				errorMessage := "unable to unmarshal a template type update response"
				logger.WithError(err).Error(errorMessage)
				responseChannel <- TemplateTypeModel{
					Session: Session{
						SessionActor:    client.actor,
						SessionEmulator: client.emulator,
						ServiceError:    NewCacaoMarshalError(errorMessage).GetBase(),
					},
				}
				return
			}

			responseChannel <- templateTypeModel
			return
		}

		errorMessage := fmt.Sprintf("unknown event type %s", ev)
		responseChannel <- TemplateTypeModel{
			Session: Session{
				SessionActor:    client.actor,
				SessionEmulator: client.emulator,
				ServiceError:    NewCacaoCommunicationError(errorMessage).GetBase(),
			},
		}
	}

	listener := messaging.Listener{
		Callback:   callback,
		ListenOnce: true,
	}

	listenerID, err := eventSource.AddListenerMultiEventType(eventsSubscribe, transactionID, listener)
	if err != nil {
		errorMessage := "unable to add an event listener"
		logger.WithError(err).Error(errorMessage)
		return NewCacaoCommunicationError(errorMessage)
	}
	defer eventSource.RemoveListenerByID(listenerID)

	err = stanConn.PublishWithTransactionID(TemplateTypeUpdateRequestedEvent, request, transactionID)
	if err != nil {
		errorMessage := "unable to publish a update template type event"
		logger.WithError(err).Error(errorMessage)
		return NewCacaoCommunicationError(errorMessage)
	}

	// wait for response
	responseReceived, response := waitTemplateTypeResponse(responseChannel, time.Duration(client.natsConfig.RequestTimeout)*time.Second)
	if responseReceived {
		serviceError := response.GetServiceError()
		if serviceError == nil {
			return nil
		}

		logger.Error(serviceError)
		return serviceError
	}

	// timed out
	logger.Error("unable to update a template type - template service is not responding")
	return NewCacaoTimeoutError("unable to update a template type - template service is not responding")
}

// DeleteType deletes the template type with the template type name
func (client *natsTemplateClient) DeleteType(templateTypeName string) error {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsTemplateClient.DeleteType",
	})

	if len(templateTypeName) == 0 {
		return NewCacaoInvalidParameterError("invalid Template Type Name")
	}

	SenderNatsConfig := client.natsConfig
	SenderNatsConfig.ClientID = SenderNatsConfig.ClientID + "-" + string(common.NewID("Sender"))

	stanConn, err := messaging.ConnectStanForServiceClient(&SenderNatsConfig, &client.stanConfig)
	if err != nil {
		errorMessage := "unable to connect to NATS Streaming"
		logger.WithError(err).Error(errorMessage)
		return NewCacaoCommunicationError(errorMessage)
	}

	defer stanConn.Disconnect()

	eventSourceNatsConfig := client.natsConfig
	eventSourceNatsConfig.ClientID = eventSourceNatsConfig.ClientID + "-" + string(common.NewID("EventSource"))

	eventSource := messaging.NewEventSource(eventSourceNatsConfig, client.stanConfig)
	if err != nil {
		errorMessage := "unable to create EventSource"
		logger.WithError(err).Error(errorMessage)
		return NewCacaoCommunicationError(errorMessage)
	}

	request := TemplateTypeModel{
		Session: Session{
			SessionActor:    client.actor,
			SessionEmulator: client.emulator,
		},
		Name: templateTypeName,
	}

	eventsSubscribe := []common.EventType{
		TemplateTypeDeletedEvent,
		TemplateTypeDeleteFailedEvent,
	}

	transactionID := messaging.NewTransactionID()

	responseChannel := make(chan TemplateTypeModel)

	callback := func(ev common.EventType, ce cloudevents.Event) {
		if ev == TemplateTypeDeletedEvent || ev == TemplateTypeDeleteFailedEvent {
			var templateTypeModel TemplateTypeModel
			err := json.Unmarshal(ce.Data(), &templateTypeModel)
			if err != nil {
				errorMessage := "unable to unmarshal a template type delete response"
				logger.WithError(err).Error(errorMessage)
				responseChannel <- TemplateTypeModel{
					Session: Session{
						SessionActor:    client.actor,
						SessionEmulator: client.emulator,
						ServiceError:    NewCacaoMarshalError(errorMessage).GetBase(),
					},
				}
				return
			}

			responseChannel <- templateTypeModel
			return
		}

		errorMessage := fmt.Sprintf("unknown event type %s", ev)
		responseChannel <- TemplateTypeModel{
			Session: Session{
				SessionActor:    client.actor,
				SessionEmulator: client.emulator,
				ServiceError:    NewCacaoCommunicationError(errorMessage).GetBase(),
			},
		}
	}

	listener := messaging.Listener{
		Callback:   callback,
		ListenOnce: true,
	}

	listenerID, err := eventSource.AddListenerMultiEventType(eventsSubscribe, transactionID, listener)
	if err != nil {
		errorMessage := "unable to add an event listener"
		logger.WithError(err).Error(errorMessage)
		return NewCacaoCommunicationError(errorMessage)
	}
	defer eventSource.RemoveListenerByID(listenerID)

	err = stanConn.PublishWithTransactionID(TemplateTypeDeleteRequestedEvent, request, transactionID)
	if err != nil {
		errorMessage := "unable to publish a delete template type event"
		logger.WithError(err).Error(errorMessage)
		return NewCacaoCommunicationError(errorMessage)
	}

	// wait for response
	responseReceived, response := waitTemplateTypeResponse(responseChannel, time.Duration(client.natsConfig.RequestTimeout)*time.Second)
	if responseReceived {
		serviceError := response.GetServiceError()
		if serviceError == nil {
			return nil
		}

		logger.Error(serviceError)
		return serviceError
	}

	// timed out
	logger.Error("unable to delete a template type - template service is not responding")
	return NewCacaoTimeoutError("unable to delete a template type - template service is not responding")
}

// Get returns the template with the given template ID
func (client *natsTemplateClient) Get(templateID common.ID) (Template, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsTemplateClient.Get",
	})

	if len(templateID) == 0 {
		return nil, NewCacaoInvalidParameterError("invalid Template ID")
	}

	natsConn, err := messaging.ConnectNatsForServiceClient(&client.natsConfig)
	if err != nil {
		errorMessage := "unable to connect to NATS"
		logger.WithError(err).Error(errorMessage)
		return nil, NewCacaoCommunicationError(errorMessage)
	}

	defer natsConn.Disconnect()

	request := TemplateModel{
		Session: Session{
			SessionActor:    client.actor,
			SessionEmulator: client.emulator,
		},
		ID: templateID,
	}

	timeout := time.Duration(client.natsConfig.RequestTimeout) * time.Second
	timeoutCtx, cancel := context.WithTimeout(client.ctx, timeout)
	defer cancel()

	responseBytes, err := natsConn.Request(timeoutCtx, TemplateGetQueryOp, request)
	if err != nil {
		errorMessage := "unable to request a get template event"
		logger.WithError(err).Error(errorMessage)
		return nil, NewCacaoCommunicationError(errorMessage)
	}

	var templateModel TemplateModel
	err = json.Unmarshal(responseBytes, &templateModel)
	if err != nil {
		errorMessage := "unable to unmarshal response JSON to template object"
		logger.WithError(err).Error(errorMessage)
		return nil, NewCacaoMarshalError(errorMessage)
	}

	serviceError := templateModel.GetServiceError()
	if serviceError != nil {
		logger.Error(serviceError)
		return nil, serviceError
	}
	return &templateModel, nil
}

// List returns all templates for the user
func (client *natsTemplateClient) List() ([]Template, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsTemplateClient.List",
	})

	natsConn, err := messaging.ConnectNatsForServiceClient(&client.natsConfig)
	if err != nil {
		errorMessage := "unable to connect to NATS"
		logger.WithError(err).Error(errorMessage)
		return nil, NewCacaoCommunicationError(errorMessage)
	}

	defer natsConn.Disconnect()

	request := Session{
		SessionActor:    client.actor,
		SessionEmulator: client.emulator,
	}

	timeout := time.Duration(client.natsConfig.RequestTimeout) * time.Second
	timeoutCtx, cancel := context.WithTimeout(client.ctx, timeout)
	defer cancel()

	responseBytes, err := natsConn.Request(timeoutCtx, TemplateListQueryOp, request)
	if err != nil {
		errorMessage := "unable to request a list templates event"
		logger.WithError(err).Error(errorMessage)
		return nil, NewCacaoCommunicationError(errorMessage)
	}

	var templateListModel TemplateListModel
	err = json.Unmarshal(responseBytes, &templateListModel)
	if err != nil {
		errorMessage := "unable to unmarshal response JSON to template list object"
		logger.WithError(err).Error(errorMessage)
		return nil, NewCacaoMarshalError(errorMessage)
	}

	serviceError := templateListModel.GetServiceError()
	if serviceError != nil {
		logger.Error(serviceError)
		return nil, serviceError
	}

	return templateListModel.GetTemplates(), nil
}

// Import imports a new template
func (client *natsTemplateClient) Import(template Template, credentialID string) (common.ID, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsTemplateClient.Import",
	})

	templateSource := template.GetSource()

	if len(templateSource.Type) == 0 {
		return common.ID(""), NewCacaoInvalidParameterError("invalid Template Source Type")
	}

	if len(templateSource.URI) == 0 {
		return common.ID(""), NewCacaoInvalidParameterError("invalid Template Source URI")
	}

	if templateSource.Visibility == TemplateSourceVisibilityPrivate {
		// private repo needs access - check credentialID
		if len(credentialID) == 0 {
			return common.ID(""), NewCacaoInvalidParameterError("invalid Credential ID for private template source")
		}
	}

	SenderNatsConfig := client.natsConfig
	SenderNatsConfig.ClientID = SenderNatsConfig.ClientID + "-" + string(common.NewID("Sender"))

	stanConn, err := messaging.ConnectStanForServiceClient(&SenderNatsConfig, &client.stanConfig)
	if err != nil {
		errorMessage := "unable to connect to NATS Streaming"
		logger.WithError(err).Error(errorMessage)
		return common.ID(""), NewCacaoCommunicationError(errorMessage)
	}

	defer stanConn.Disconnect()

	eventSourceNatsConfig := client.natsConfig
	eventSourceNatsConfig.ClientID = eventSourceNatsConfig.ClientID + "-" + string(common.NewID("EventSource"))

	eventSource := messaging.NewEventSource(eventSourceNatsConfig, client.stanConfig)
	if err != nil {
		errorMessage := "unable to create EventSource"
		logger.WithError(err).Error(errorMessage)
		return common.ID(""), NewCacaoCommunicationError(errorMessage)
	}

	templateID := template.GetID()
	if !templateID.Validate() {
		templateID = NewTemplateID()
	}

	request := TemplateModel{
		Session: Session{
			SessionActor:    client.actor,
			SessionEmulator: client.emulator,
		},
		ID:           templateID,
		Owner:        client.actor,
		Name:         template.GetName(),
		Description:  template.GetDescription(),
		Public:       template.IsPublic(),
		Source:       template.GetSource(),
		Metadata:     template.GetMetadata(),
		CredentialID: credentialID,
	}

	eventsSubscribe := []common.EventType{
		TemplateImportedEvent,
		TemplateImportFailedEvent,
	}

	transactionID := messaging.NewTransactionID()

	responseChannel := make(chan TemplateModel)

	callback := func(ev common.EventType, ce cloudevents.Event) {
		if ev == TemplateImportedEvent || ev == TemplateImportFailedEvent {
			var templateModel TemplateModel
			err := json.Unmarshal(ce.Data(), &templateModel)
			if err != nil {
				errorMessage := "unable to unmarshal a template import response"
				logger.WithError(err).Error(errorMessage)
				responseChannel <- TemplateModel{
					Session: Session{
						SessionActor:    client.actor,
						SessionEmulator: client.emulator,
						ServiceError:    NewCacaoMarshalError(errorMessage).GetBase(),
					},
				}
				return
			}

			responseChannel <- templateModel
			return
		}

		errorMessage := fmt.Sprintf("unknown event type %s", ev)
		responseChannel <- TemplateModel{
			Session: Session{
				SessionActor:    client.actor,
				SessionEmulator: client.emulator,
				ServiceError:    NewCacaoCommunicationError(errorMessage).GetBase(),
			},
		}
	}

	listener := messaging.Listener{
		Callback:   callback,
		ListenOnce: true,
	}

	listenerID, err := eventSource.AddListenerMultiEventType(eventsSubscribe, transactionID, listener)
	if err != nil {
		errorMessage := "unable to add an event listener"
		logger.WithError(err).Error(errorMessage)
		return common.ID(""), NewCacaoCommunicationError(errorMessage)
	}
	defer eventSource.RemoveListenerByID(listenerID)

	err = stanConn.PublishWithTransactionID(TemplateImportRequestedEvent, request, transactionID)
	if err != nil {
		errorMessage := "unable to publish an import template event"
		logger.WithError(err).Error(errorMessage)
		return common.ID(""), NewCacaoCommunicationError(errorMessage)
	}

	// wait for response
	responseReceived, response := waitTemplateResponse(responseChannel, time.Duration(client.natsConfig.RequestTimeout)*time.Second)
	if responseReceived {
		serviceError := response.GetServiceError()
		if serviceError == nil {
			return templateID, nil
		}

		logger.Error(serviceError)
		return common.ID(""), serviceError
	}

	// timed out
	errorMessage := "unable to import a template - template service is not responding"
	logger.Error(errorMessage)
	return common.ID(""), NewCacaoTimeoutError(errorMessage)
}

// Update updates the template with the template ID
func (client *natsTemplateClient) Update(template Template) error {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsTemplateClient.Update",
	})

	if !template.GetID().Validate() {
		return NewCacaoInvalidParameterError("invalid Template ID")
	}

	if len(template.GetName()) == 0 {
		return NewCacaoInvalidParameterError("invalid Template Name")
	}

	templateSource := template.GetSource()

	if len(templateSource.Type) == 0 {
		return NewCacaoInvalidParameterError("invalid Template Source Type")
	}

	if len(templateSource.URI) == 0 {
		return NewCacaoInvalidParameterError("invalid Template Source URI")
	}

	SenderNatsConfig := client.natsConfig
	SenderNatsConfig.ClientID = SenderNatsConfig.ClientID + "-" + string(common.NewID("Sender"))

	stanConn, err := messaging.ConnectStanForServiceClient(&SenderNatsConfig, &client.stanConfig)
	if err != nil {
		errorMessage := "unable to connect to NATS Streaming"
		logger.WithError(err).Error(errorMessage)
		return NewCacaoCommunicationError(errorMessage)
	}

	defer stanConn.Disconnect()

	eventSourceNatsConfig := client.natsConfig
	eventSourceNatsConfig.ClientID = eventSourceNatsConfig.ClientID + "-" + string(common.NewID("EventSource"))

	eventSource := messaging.NewEventSource(eventSourceNatsConfig, client.stanConfig)
	if err != nil {
		errorMessage := "unable to create EventSource"
		logger.WithError(err).Error(errorMessage)
		return NewCacaoCommunicationError(errorMessage)
	}

	request := TemplateModel{
		Session: Session{
			SessionActor:    client.actor,
			SessionEmulator: client.emulator,
		},
		ID:          template.GetID(),
		Name:        template.GetName(),
		Description: template.GetDescription(),
		Public:      template.IsPublic(),
		Source:      template.GetSource(),
	}

	eventsSubscribe := []common.EventType{
		TemplateUpdatedEvent,
		TemplateUpdateFailedEvent,
	}

	transactionID := messaging.NewTransactionID()

	responseChannel := make(chan TemplateModel)

	callback := func(ev common.EventType, ce cloudevents.Event) {
		if ev == TemplateUpdatedEvent || ev == TemplateUpdateFailedEvent {
			var templateModel TemplateModel
			err := json.Unmarshal(ce.Data(), &templateModel)
			if err != nil {
				errorMessage := "unable to unmarshal a template update response"
				logger.WithError(err).Error(errorMessage)
				responseChannel <- TemplateModel{
					Session: Session{
						SessionActor:    client.actor,
						SessionEmulator: client.emulator,
						ServiceError:    NewCacaoMarshalError(errorMessage).GetBase(),
					},
				}
				return
			}

			responseChannel <- templateModel
			return
		}

		errorMessage := fmt.Sprintf("unknown event type %s", ev)
		responseChannel <- TemplateModel{
			Session: Session{
				SessionActor:    client.actor,
				SessionEmulator: client.emulator,
				ServiceError:    NewCacaoCommunicationError(errorMessage).GetBase(),
			},
		}
	}

	listener := messaging.Listener{
		Callback:   callback,
		ListenOnce: true,
	}

	listenerID, err := eventSource.AddListenerMultiEventType(eventsSubscribe, transactionID, listener)
	if err != nil {
		errorMessage := "unable to add an event listener"
		logger.WithError(err).Error(errorMessage)
		return NewCacaoCommunicationError(errorMessage)
	}
	defer eventSource.RemoveListenerByID(listenerID)

	err = stanConn.PublishWithTransactionID(TemplateUpdateRequestedEvent, request, transactionID)
	if err != nil {
		errorMessage := "unable to publish an update template event"
		logger.WithError(err).Error(errorMessage)
		return NewCacaoCommunicationError(errorMessage)
	}

	// wait for response
	responseReceived, response := waitTemplateResponse(responseChannel, time.Duration(client.natsConfig.RequestTimeout)*time.Second)
	if responseReceived {
		serviceError := response.GetServiceError()
		if serviceError == nil {
			return nil
		}

		logger.Error(serviceError)
		return serviceError
	}

	// timed out
	logger.Error("unable to update a template - template service is not responding")
	return NewCacaoTimeoutError("unable to update a template - template service is not responding")
}

// UpdateFields updates the selected fields of the template with the template ID
func (client *natsTemplateClient) UpdateFields(template Template, updateFieldNames []string) error {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsTemplateClient.UpdateFields",
	})

	if !template.GetID().Validate() {
		return NewCacaoInvalidParameterError("invalid Template ID")
	}

	if len(updateFieldNames) == 0 {
		return nil
	}

	for _, updateFieldName := range updateFieldNames {
		switch updateFieldName {
		case "name":
			if len(template.GetName()) == 0 {
				return NewCacaoInvalidParameterError("invalid Template Name")
			}
		case "source":
			templateSource := template.GetSource()

			if len(templateSource.Type) == 0 {
				return NewCacaoInvalidParameterError("invalid Template Source Type")
			}

			if len(templateSource.URI) == 0 {
				return NewCacaoInvalidParameterError("invalid Template Source URI")
			}
		default:
			//pass
		}
	}

	SenderNatsConfig := client.natsConfig
	SenderNatsConfig.ClientID = SenderNatsConfig.ClientID + "-" + string(common.NewID("Sender"))

	stanConn, err := messaging.ConnectStanForServiceClient(&SenderNatsConfig, &client.stanConfig)
	if err != nil {
		errorMessage := "unable to connect to NATS Streaming"
		logger.WithError(err).Error(errorMessage)
		return NewCacaoCommunicationError(errorMessage)
	}

	defer stanConn.Disconnect()

	eventSourceNatsConfig := client.natsConfig
	eventSourceNatsConfig.ClientID = eventSourceNatsConfig.ClientID + "-" + string(common.NewID("EventSource"))

	eventSource := messaging.NewEventSource(eventSourceNatsConfig, client.stanConfig)
	if err != nil {
		errorMessage := "unable to create EventSource"
		logger.WithError(err).Error(errorMessage)
		return NewCacaoCommunicationError(errorMessage)
	}

	request := TemplateModel{
		Session: Session{
			SessionActor:    client.actor,
			SessionEmulator: client.emulator,
		},
		ID:               template.GetID(),
		Name:             template.GetName(),
		Description:      template.GetDescription(),
		Public:           template.IsPublic(),
		Source:           template.GetSource(),
		UpdateFieldNames: updateFieldNames,
	}

	eventsSubscribe := []common.EventType{
		TemplateUpdatedEvent,
		TemplateUpdateFailedEvent,
	}

	transactionID := messaging.NewTransactionID()

	responseChannel := make(chan TemplateModel)

	callback := func(ev common.EventType, ce cloudevents.Event) {
		if ev == TemplateUpdatedEvent || ev == TemplateUpdateFailedEvent {
			var templateModel TemplateModel
			err := json.Unmarshal(ce.Data(), &templateModel)
			if err != nil {
				errorMessage := "unable to unmarshal a template update response"
				logger.WithError(err).Error(errorMessage)
				responseChannel <- TemplateModel{
					Session: Session{
						SessionActor:    client.actor,
						SessionEmulator: client.emulator,
						ServiceError:    NewCacaoMarshalError(errorMessage).GetBase(),
					},
				}
				return
			}

			responseChannel <- templateModel
			return
		}

		errorMessage := fmt.Sprintf("unknown event type %s", ev)
		responseChannel <- TemplateModel{
			Session: Session{
				SessionActor:    client.actor,
				SessionEmulator: client.emulator,
				ServiceError:    NewCacaoCommunicationError(errorMessage).GetBase(),
			},
		}
	}

	listener := messaging.Listener{
		Callback:   callback,
		ListenOnce: true,
	}

	listenerID, err := eventSource.AddListenerMultiEventType(eventsSubscribe, transactionID, listener)
	if err != nil {
		errorMessage := "unable to add an event listener"
		logger.WithError(err).Error(errorMessage)
		return NewCacaoCommunicationError(errorMessage)
	}
	defer eventSource.RemoveListenerByID(listenerID)

	err = stanConn.PublishWithTransactionID(TemplateUpdateRequestedEvent, request, transactionID)
	if err != nil {
		errorMessage := "unable to publish an update template event"
		logger.WithError(err).Error(errorMessage)
		return NewCacaoCommunicationError(errorMessage)
	}

	// wait for response
	responseReceived, response := waitTemplateResponse(responseChannel, time.Duration(client.natsConfig.RequestTimeout)*time.Second)
	if responseReceived {
		serviceError := response.GetServiceError()
		if serviceError == nil {
			return nil
		}

		logger.Error(serviceError)
		return serviceError
	}

	// timed out
	logger.Error("unable to update a template - template service is not responding")
	return NewCacaoTimeoutError("unable to update a template - template service is not responding")
}

// Sync syncs the template with the template ID
func (client *natsTemplateClient) Sync(templateID common.ID, credentialID string) error {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsTemplateClient.Sync",
	})

	if !templateID.Validate() {
		return NewCacaoInvalidParameterError("invalid Template ID")
	}

	// credentialID may be empty if the template source is public

	SenderNatsConfig := client.natsConfig
	SenderNatsConfig.ClientID = SenderNatsConfig.ClientID + "-" + string(common.NewID("Sender"))

	stanConn, err := messaging.ConnectStanForServiceClient(&SenderNatsConfig, &client.stanConfig)
	if err != nil {
		errorMessage := "unable to connect to NATS Streaming"
		logger.WithError(err).Error(errorMessage)
		return NewCacaoCommunicationError(errorMessage)
	}

	defer stanConn.Disconnect()

	eventSourceNatsConfig := client.natsConfig
	eventSourceNatsConfig.ClientID = eventSourceNatsConfig.ClientID + "-" + string(common.NewID("EventSource"))

	eventSource := messaging.NewEventSource(eventSourceNatsConfig, client.stanConfig)
	if err != nil {
		errorMessage := "unable to create EventSource"
		logger.WithError(err).Error(errorMessage)
		return NewCacaoCommunicationError(errorMessage)
	}

	request := TemplateModel{
		Session: Session{
			SessionActor:    client.actor,
			SessionEmulator: client.emulator,
		},
		ID:           templateID,
		CredentialID: credentialID,
	}

	eventsSubscribe := []common.EventType{
		TemplateSyncedEvent,
		TemplateSyncFailedEvent,
	}

	transactionID := messaging.NewTransactionID()

	responseChannel := make(chan TemplateModel)

	callback := func(ev common.EventType, ce cloudevents.Event) {
		if ev == TemplateSyncedEvent || ev == TemplateSyncFailedEvent {
			var templateModel TemplateModel
			err := json.Unmarshal(ce.Data(), &templateModel)
			if err != nil {
				errorMessage := "unable to unmarshal a template sync response"
				logger.WithError(err).Error(errorMessage)
				responseChannel <- TemplateModel{
					Session: Session{
						SessionActor:    client.actor,
						SessionEmulator: client.emulator,
						ServiceError:    NewCacaoMarshalError(errorMessage).GetBase(),
					},
				}
				return
			}

			responseChannel <- templateModel
			return
		}

		errorMessage := fmt.Sprintf("unknown event type %s", ev)
		responseChannel <- TemplateModel{
			Session: Session{
				SessionActor:    client.actor,
				SessionEmulator: client.emulator,
				ServiceError:    NewCacaoCommunicationError(errorMessage).GetBase(),
			},
		}
	}

	listener := messaging.Listener{
		Callback:   callback,
		ListenOnce: true,
	}

	listenerID, err := eventSource.AddListenerMultiEventType(eventsSubscribe, transactionID, listener)
	if err != nil {
		errorMessage := "unable to add an event listener"
		logger.WithError(err).Error(errorMessage)
		return NewCacaoCommunicationError(errorMessage)
	}
	defer eventSource.RemoveListenerByID(listenerID)

	err = stanConn.PublishWithTransactionID(TemplateSyncRequestedEvent, request, transactionID)
	if err != nil {
		errorMessage := "unable to publish a sync template event"
		logger.WithError(err).Error(errorMessage)
		return NewCacaoCommunicationError(errorMessage)
	}

	// wait for response
	responseReceived, response := waitTemplateResponse(responseChannel, time.Duration(client.natsConfig.RequestTimeout)*time.Second)
	if responseReceived {
		serviceError := response.GetServiceError()
		if serviceError == nil {
			return nil
		}

		logger.Error(serviceError)
		return serviceError
	}

	// timed out
	logger.Error("unable to sync a template - template service is not responding")
	return NewCacaoTimeoutError("unable to sync a template - template service is not responding")
}

// Delete deletes the template with the template ID
func (client *natsTemplateClient) Delete(templateID common.ID) error {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsTemplateClient.Delete",
	})

	if !templateID.Validate() {
		return NewCacaoInvalidParameterError("invalid Template ID")
	}

	SenderNatsConfig := client.natsConfig
	SenderNatsConfig.ClientID = SenderNatsConfig.ClientID + "-" + string(common.NewID("Sender"))

	stanConn, err := messaging.ConnectStanForServiceClient(&SenderNatsConfig, &client.stanConfig)
	if err != nil {
		errorMessage := "unable to connect to NATS Streaming"
		logger.WithError(err).Error(errorMessage)
		return NewCacaoCommunicationError(errorMessage)
	}

	defer stanConn.Disconnect()

	eventSourceNatsConfig := client.natsConfig
	eventSourceNatsConfig.ClientID = eventSourceNatsConfig.ClientID + "-" + string(common.NewID("EventSource"))

	eventSource := messaging.NewEventSource(eventSourceNatsConfig, client.stanConfig)
	if err != nil {
		errorMessage := "unable to create EventSource"
		logger.WithError(err).Error(errorMessage)
		return NewCacaoCommunicationError(errorMessage)
	}

	request := TemplateModel{
		Session: Session{
			SessionActor:    client.actor,
			SessionEmulator: client.emulator,
		},
		ID: templateID,
	}

	eventsSubscribe := []common.EventType{
		TemplateDeletedEvent,
		TemplateDeleteFailedEvent,
	}

	transactionID := messaging.NewTransactionID()

	responseChannel := make(chan TemplateModel)

	callback := func(ev common.EventType, ce cloudevents.Event) {
		if ev == TemplateDeletedEvent || ev == TemplateDeleteFailedEvent {
			var templateModel TemplateModel
			err := json.Unmarshal(ce.Data(), &templateModel)
			if err != nil {
				errorMessage := "unable to unmarshal a template delete response"
				logger.WithError(err).Error(errorMessage)
				responseChannel <- TemplateModel{
					Session: Session{
						SessionActor:    client.actor,
						SessionEmulator: client.emulator,
						ServiceError:    NewCacaoMarshalError(errorMessage).GetBase(),
					},
				}
				return
			}

			responseChannel <- templateModel
			return
		}

		errorMessage := fmt.Sprintf("unknown event type %s", ev)
		responseChannel <- TemplateModel{
			Session: Session{
				SessionActor:    client.actor,
				SessionEmulator: client.emulator,
				ServiceError:    NewCacaoCommunicationError(errorMessage).GetBase(),
			},
		}
	}

	listener := messaging.Listener{
		Callback:   callback,
		ListenOnce: true,
	}

	listenerID, err := eventSource.AddListenerMultiEventType(eventsSubscribe, transactionID, listener)
	if err != nil {
		errorMessage := "unable to add an event listener"
		logger.WithError(err).Error(errorMessage)
		return NewCacaoCommunicationError(errorMessage)
	}
	defer eventSource.RemoveListenerByID(listenerID)

	err = stanConn.PublishWithTransactionID(TemplateDeleteRequestedEvent, request, transactionID)
	if err != nil {
		errorMessage := "unable to publish a delete template event"
		logger.WithError(err).Error(errorMessage)
		return NewCacaoCommunicationError(errorMessage)
	}

	// wait for response
	responseReceived, response := waitTemplateResponse(responseChannel, time.Duration(client.natsConfig.RequestTimeout)*time.Second)
	if responseReceived {
		serviceError := response.GetServiceError()
		if serviceError == nil {
			return nil
		}

		logger.Error(serviceError)
		return serviceError
	}

	// timed out
	errorMessage := "unable to delete a template - template service is not responding"
	logger.Error(errorMessage)
	return NewCacaoTimeoutError(errorMessage)
}
