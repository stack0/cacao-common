package service

import (
	"context"
	"encoding/json"
	"time"

	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging"
)

// Nats subjects
const (
	// NatsSubjectJS2Allocation is the prefix of the queue name for JS2Allocation queries
	NatsSubjectJS2Allocation common.QueryOp = common.NatsQueryOpPrefix + "js2allocation"

	// JS2UserGetQueryOp is the queue name for JS2UserGet query
	JS2UserGetQueryOp common.QueryOp = NatsSubjectJS2Allocation + ".getUser"
	// JS2ProjectListQueryOp is the queue name for JS2ProjectList query
	JS2ProjectListQueryOp common.QueryOp = NatsSubjectJS2Allocation + ".list"
)

// Nats Client
type natsJS2AllocationClient struct {
	actor      string
	emulator   string
	natsConfig messaging.NatsConfig
	ctx        context.Context
}

// NewNatsJS2AllocationClient creates a client for JS2Allocation microservice that uses NATS for communication
func NewNatsJS2AllocationClient(ctx context.Context, actor string, emulator string, natsConfig messaging.NatsConfig) (JS2AllocationClient, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "NewNatsJS2AllocationClient",
	})

	logger.Trace("Creating a NatsJS2AllocationClient")

	if len(actor) == 0 {
		return nil, NewCacaoInvalidParameterError("actor is empty")
	}

	if ctx == nil {
		ctx = context.Background()
	}

	natsConfigCopy := natsConfig
	if natsConfigCopy.RequestTimeout == -1 {
		natsConfigCopy.RequestTimeout = int(messaging.DefaultNatsRequestTimeout.Seconds())
	}

	client := natsJS2AllocationClient{
		actor:      actor,
		emulator:   emulator,
		natsConfig: natsConfigCopy,
		ctx:        ctx,
	}

	return &client, nil
}

// GetUser returns JS2 User for the user
func (client *natsJS2AllocationClient) GetUser(nocache bool) (JS2User, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsJS2AllocationClient.GetUser",
	})

	natsConn, err := messaging.ConnectNatsForServiceClient(&client.natsConfig)
	if err != nil {
		errorMessage := "unable to connect to NATS"
		logger.WithError(err).Error(errorMessage)
		return nil, NewCacaoCommunicationError(errorMessage)
	}

	defer natsConn.Disconnect()

	request := JS2UserModel{
		Session: Session{
			SessionActor:    client.actor,
			SessionEmulator: client.emulator,
		},
		NoCache: nocache,
	}

	timeout := time.Duration(client.natsConfig.RequestTimeout) * time.Second
	timeoutCtx, cancel := context.WithTimeout(client.ctx, timeout)
	defer cancel()

	responseBytes, err := natsConn.Request(timeoutCtx, JS2UserGetQueryOp, request)
	if err != nil {
		errorMessage := "unable to request a get user event"
		logger.WithError(err).Error(errorMessage)
		return nil, NewCacaoCommunicationError(errorMessage)
	}

	var userModel JS2UserModel
	err = json.Unmarshal(responseBytes, &userModel)
	if err != nil {
		errorMessage := "unable to unmarshal response JSON to JS2 user object"
		logger.WithError(err).Error(errorMessage)
		return nil, NewCacaoMarshalError(errorMessage)
	}

	serviceError := userModel.GetServiceError()
	if serviceError != nil {
		logger.Error(serviceError)
		return nil, serviceError
	}

	return &userModel, nil
}

// List returns all JS2 Projects for the user
func (client *natsJS2AllocationClient) List(nocache bool) ([]JS2Project, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsJS2AllocationClient.List",
	})

	natsConn, err := messaging.ConnectNatsForServiceClient(&client.natsConfig)
	if err != nil {
		errorMessage := "unable to connect to NATS"
		logger.WithError(err).Error(errorMessage)
		return nil, NewCacaoCommunicationError(errorMessage)
	}

	defer natsConn.Disconnect()

	request := JS2ProjectListModel{
		Session: Session{
			SessionActor:    client.actor,
			SessionEmulator: client.emulator,
		},
		NoCache: nocache,
	}

	timeout := time.Duration(client.natsConfig.RequestTimeout) * time.Second
	timeoutCtx, cancel := context.WithTimeout(client.ctx, timeout)
	defer cancel()

	responseBytes, err := natsConn.Request(timeoutCtx, JS2ProjectListQueryOp, request)
	if err != nil {
		errorMessage := "unable to request a list projects event"
		logger.WithError(err).Error(errorMessage)
		return nil, NewCacaoCommunicationError(errorMessage)
	}

	var projectListModel JS2ProjectListModel
	err = json.Unmarshal(responseBytes, &projectListModel)
	if err != nil {
		errorMessage := "unable to unmarshal response JSON to JS2 project list object"
		logger.WithError(err).Error(errorMessage)
		return nil, NewCacaoMarshalError(errorMessage)
	}

	serviceError := projectListModel.GetServiceError()
	if serviceError != nil {
		logger.Error(serviceError)
		return nil, serviceError
	}

	return projectListModel.GetProjects(), nil
}
