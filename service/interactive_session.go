package service

import (
	"encoding/json"
	"fmt"
	"strings"
	"time"

	"gitlab.com/cyverse/cacao-common/common"
)

// InteractiveSessionProtocol is the protocol of an interactive session
type InteractiveSessionProtocol string

const (
	// InteractiveSessionProtocolUnknown is for an unknown interactive session protocol
	InteractiveSessionProtocolUnknown InteractiveSessionProtocol = ""
	// InteractiveSessionProtocolSSH is for a SSH interactive session protocol
	InteractiveSessionProtocolSSH InteractiveSessionProtocol = "ssh"
	// InteractiveSessionProtocolVNC is for a VNC interactive session protocol
	InteractiveSessionProtocolVNC InteractiveSessionProtocol = "vnc"
)

// String returns the string representation of a InteractiveSessionProtocol.
func (p InteractiveSessionProtocol) String() string {
	return string(p)
}

// MarshalJSON returns the JSON representation of a InteractiveSessionProtocol.
func (p InteractiveSessionProtocol) MarshalJSON() ([]byte, error) {
	return json.Marshal(p.String())
}

// UnmarshalJSON converts the JSON representation of a InteractiveSessionProtocol to the appropriate enumeration constant.
func (p *InteractiveSessionProtocol) UnmarshalJSON(b []byte) error {
	// Template formats are represented as strings.
	var s string
	err := json.Unmarshal(b, &s)
	if err != nil {
		return err
	}

	// Validate and convert the value.
	switch strings.ToLower(s) {
	case "":
		*p = InteractiveSessionProtocolUnknown
	case "ssh":
		*p = InteractiveSessionProtocolSSH
	case "vnc":
		*p = InteractiveSessionProtocolVNC
	default:
		return fmt.Errorf("invalid interactive session format: %s", s)
	}

	return nil
}

// InteractiveSessionState is the state of an interactive session
type InteractiveSessionState string

const (
	// InteractiveSessionStateUnknown is for the unknown interactive session
	InteractiveSessionStateUnknown InteractiveSessionState = ""
	// InteractiveSessionStateCreating is for the interactive session in creation
	InteractiveSessionStateCreating InteractiveSessionState = "creating"
	// InteractiveSessionStateFailed is for the interactive session failed in creation
	InteractiveSessionStateFailed InteractiveSessionState = "failed"
	// InteractiveSessionStateActive is for the active interactive session
	InteractiveSessionStateActive InteractiveSessionState = "active"
	// InteractiveSessionStateInactive is for the inactive interactive session
	InteractiveSessionStateInactive InteractiveSessionState = "inactive"
)

// String returns the string representation of a InteractiveSessionState.
func (p InteractiveSessionState) String() string {
	return string(p)
}

// MarshalJSON returns the JSON representation of a InteractiveSessionState.
func (p InteractiveSessionState) MarshalJSON() ([]byte, error) {
	return json.Marshal(p.String())
}

// UnmarshalJSON converts the JSON representation of a InteractiveSessionState to the appropriate enumeration constant.
func (p *InteractiveSessionState) UnmarshalJSON(b []byte) error {
	// Template formats are represented as strings.
	var s string
	err := json.Unmarshal(b, &s)
	if err != nil {
		return err
	}

	// Validate and convert the value.
	switch strings.ToLower(s) {
	case "":
		*p = InteractiveSessionStateUnknown
	case "creating":
		*p = InteractiveSessionStateCreating
	case "failed":
		*p = InteractiveSessionStateFailed
	case "active":
		*p = InteractiveSessionStateActive
	case "inactive":
		*p = InteractiveSessionStateInactive
	default:
		return fmt.Errorf("invalid interactive session state: %s", s)
	}

	return nil
}

// InteractiveSessionClient is the client for InteractiveSession
// The ISMS Client contains three methods: Create, List and Check.
//		Get: Is used to get an interactive session stored in the database.
//		GetByInstanceID: Is used to get an interactive session stored in the database by instance ID.
//		GetByInstanceAddress: Is used to get an interactive session stored in the database by instance address.
//		ListAll: Is used to list all interactive sessions stored in the database.
//		List: Is used to list all active/creating interactive sessions.
//		CheckPrerequisites: Is used to check if the given interactive session can be created. Please provide the following fields when checking:
//			InstanceAddress // IP Address of the instance (String)
//			InstanceAdminUsername // Admin username of the instance (string)
//			Protocol // Either SSH or VNC (String)
//		Create: Is used to create and interactive session. Please provide the following fields when creating a session:
//			InstanceID // (String)
//			InstanceAddress // IP Address of the instance (String)
//			InstanceAdminUsername // Admin username of the instance (string)
//			Protocol // Either SSH or VNC (String)
//			CloudID // Identifier of the cloud where the instance is hosted. (String)
//		Deactivate: Is used to change the state of an interactive session to inactive.
type InteractiveSessionClient interface {
	Get(interactiveSessionID common.ID) (InteractiveSession, error)
	GetByInstanceID(instanceID string) (InteractiveSession, error)
	GetByInstanceAddress(instanceAddress string) (InteractiveSession, error)

	List() ([]InteractiveSession, error)
	CheckPrerequisites(protocol InteractiveSessionProtocol, instanceAddress string, instanceAdminUsername string) (bool, error)

	Create(interactiveSession InteractiveSession) (common.ID, error)
	Deactivate(interactiveSessionID common.ID) error
}

// InteractiveSession is the standard interface for all InteractiveSession implementations
// The proper InteractiveSession object containing basic setters and getters for the ISMS object.
type InteractiveSession interface {
	SessionContext

	GetPrimaryID() common.ID // points to ID
	GetID() common.ID
	GetOwner() string
	GetInstanceID() string
	GetInstanceAddress() string
	GetInstanceAdminUsername() string // 'ubuntu' for ubuntu images, 'centos' for centos images
	GetCloudID() string               // Must match an existing GuacLiteServer
	GetProtocol() InteractiveSessionProtocol
	GetRedirectURL() string
	GetState() InteractiveSessionState
	GetCreatedAt() time.Time
	GetUpdatedAt() time.Time

	SetPrimaryID(id common.ID)
	SetID(id common.ID)
	SetInstanceID(string)
	SetInstanceAddress(string)
	SetInstanceAdminUsername(string) // 'ubuntu' for ubuntu images, 'centos' for centos images
	SetCloudID(string)               // Must match an existing GuacLiteServer
	SetProtocol(protocol InteractiveSessionProtocol)
	SetRedirectURL(string)
	SetState(InteractiveSessionState)
}

// InteractiveSessionModel is the InteractiveSession model
// The proper model for the ISMS. Containing all needed fields both incoming and outgoing.
//		InstanceID: ID of the instance to connect to.
//		InstanceAddress: IP-Address of the instance to connect to.
//		InstanceAdminUsername: Admin username in the instance to login
//		CloudID: ID of the cloud to connect to.
//		Protocol: Protocol of the session
//		RedirectURL: Contains the URL containing both IP+Encrypted Key returned by the ISMS. Used to redirect the user
//			to a Guacamole Lite server.
//		State: state of the interactive session
type InteractiveSessionModel struct {
	Session

	ID                    common.ID                  `json:"id"`
	Owner                 string                     `json:"owner"`
	InstanceID            string                     `json:"instance_id"`
	InstanceAddress       string                     `json:"instance_address"`
	InstanceAdminUsername string                     `json:"instance_admin_username"`
	CloudID               string                     `json:"cloud_id"`
	Protocol              InteractiveSessionProtocol `json:"protocol"`
	RedirectURL           string                     `json:"redirect_url"`
	State                 InteractiveSessionState    `json:"state"`
	CreatedAt             time.Time                  `json:"created_at"`
	UpdatedAt             time.Time                  `json:"updated_at"`
}

// NewInteractiveSessionID generates a new InteractiveSessionID
func NewInteractiveSessionID() common.ID {
	return common.NewID("interactivesession")
}

// GetPrimaryID returns the primary ID of the InteractiveSession
func (i *InteractiveSessionModel) GetPrimaryID() common.ID {
	return i.ID
}

// GetID returns the ID of the InteractiveSession
func (i *InteractiveSessionModel) GetID() common.ID {
	return i.ID
}

// GetOwner returns the owner of the InteractiveSession
func (i *InteractiveSessionModel) GetOwner() string {
	return i.Owner
}

// GetInstanceID returns the instance ID of the InteractiveSession
func (i *InteractiveSessionModel) GetInstanceID() string {
	return i.InstanceID
}

// GetInstanceAddress returns the IP address of the instance of the InteractiveSession
func (i *InteractiveSessionModel) GetInstanceAddress() string {
	return i.InstanceAddress
}

// GetInstanceAdminUsername returns the admin username of the instance of the InteractiveSession
func (i *InteractiveSessionModel) GetInstanceAdminUsername() string {
	return i.InstanceAdminUsername
}

// GetCloudID returns the cloud ID of the InteractiveSession
func (i *InteractiveSessionModel) GetCloudID() string {
	return i.CloudID
}

// GetProtocol returns the protocol of the InteractiveSession
func (i *InteractiveSessionModel) GetProtocol() InteractiveSessionProtocol {
	return i.Protocol
}

// GetRedirectURL returns the redirect URL of the InteractiveSession
func (i *InteractiveSessionModel) GetRedirectURL() string {
	return i.RedirectURL
}

// GetState returns the state of the InteractiveSession
func (i *InteractiveSessionModel) GetState() InteractiveSessionState {
	return i.State
}

// GetCreatedAt returns the creation time of the InteractiveSession
func (i *InteractiveSessionModel) GetCreatedAt() time.Time {
	return i.CreatedAt
}

// GetUpdatedAt returns the modified time of the InteractiveSession
func (w *InteractiveSessionModel) GetUpdatedAt() time.Time {
	return w.UpdatedAt
}

// SetPrimaryID sets the primary ID of the InteractiveSession
func (i *InteractiveSessionModel) SetPrimaryID(id common.ID) {
	i.ID = id
}

// SetID sets the ID of the InteractiveSession
func (i *InteractiveSessionModel) SetID(id common.ID) {
	i.ID = id
}

// SetInstanceID sets the instanceID of the InteractiveSession
func (i *InteractiveSessionModel) SetInstanceID(instanceID string) {
	i.InstanceID = instanceID
}

// SetInstanceAddress sets the IP address of the InteractiveSession
func (i *InteractiveSessionModel) SetInstanceAddress(address string) {
	i.InstanceAddress = address
}

// SetInstanceAdminUsername sets the admin username of the instance of the InteractiveSession
func (i *InteractiveSessionModel) SetInstanceAdminUsername(adminUsername string) {
	i.InstanceAdminUsername = adminUsername
}

// SetCloudID sets the cloud ID of the InteractiveSession
func (i *InteractiveSessionModel) SetCloudID(cloudID string) {
	i.CloudID = cloudID
}

// SetProtocol sets the protocol of the InteractiveSession
func (i *InteractiveSessionModel) SetProtocol(protocol InteractiveSessionProtocol) {
	i.Protocol = protocol
}

// SetRedirectURL sets the redirct URL of the InteractiveSession
func (i *InteractiveSessionModel) SetRedirectURL(redirectURL string) {
	i.RedirectURL = redirectURL
}

// SetState sets the state of the InteractiveSession
func (i *InteractiveSessionModel) SetState(state InteractiveSessionState) {
	i.State = state
}

// InteractiveSessionListItemModel is the element type in InteractiveSessionListModel
type InteractiveSessionListItemModel struct {
	ID                    common.ID                  `json:"id"`
	Owner                 string                     `json:"owner"`
	InstanceID            string                     `json:"instance_id"`
	InstanceAddress       string                     `json:"instance_address"`
	InstanceAdminUsername string                     `json:"instance_admin_username"`
	CloudID               string                     `json:"cloud_id"`
	Protocol              InteractiveSessionProtocol `json:"protocol"`
	RedirectURL           string                     `json:"redirect_url"`
	State                 InteractiveSessionState    `json:"state"`
	CreatedAt             time.Time                  `json:"created_at"`
	UpdatedAt             time.Time                  `json:"updated_at"`
}

// InteractiveSessionListModel is the InteractiveSession list model
type InteractiveSessionListModel struct {
	Session

	InteractiveSessions []InteractiveSessionListItemModel `json:"interactive_sessions"`
}

// GetInteractiveSessions returns InteractiveSessions
func (i *InteractiveSessionListModel) GetInteractiveSessions() []InteractiveSession {
	interactiveSessions := make([]InteractiveSession, 0, len(i.InteractiveSessions))

	for _, interactiveSession := range i.InteractiveSessions {
		interactiveSessionModel := InteractiveSessionModel{
			Session:               i.Session,
			ID:                    interactiveSession.ID,
			Owner:                 interactiveSession.Owner,
			InstanceID:            interactiveSession.InstanceID,
			InstanceAddress:       interactiveSession.InstanceAddress,
			InstanceAdminUsername: interactiveSession.InstanceAdminUsername,
			CloudID:               interactiveSession.CloudID,
			Protocol:              interactiveSession.Protocol,
			RedirectURL:           interactiveSession.RedirectURL,
			State:                 interactiveSession.State,
			CreatedAt:             interactiveSession.CreatedAt,
			UpdatedAt:             interactiveSession.UpdatedAt,
		}

		interactiveSessions = append(interactiveSessions, &interactiveSessionModel)
	}

	return interactiveSessions
}
