package service

import (
	"time"

	"gitlab.com/cyverse/cacao-common/common"
)

// JS2AllocationClient is the client for JS2Allocation
type JS2AllocationClient interface {
	GetUser(nocache bool) (JS2User, error)
	List(nocache bool) ([]JS2Project, error)
}

// JS2User is the standard interface for all JS2 User implementations
type JS2User interface {
	SessionContext

	GetPrimaryID() string // points to Owner
	GetOwner() string
	GetTACCUsername() string
	GetRetrievedAt() time.Time
}

// JS2UserModel is the JS2 User model
type JS2UserModel struct {
	Session

	Owner        string    `json:"owner"`
	TACCUsername string    `json:"tacc_username"`
	RetrievedAt  time.Time `json:"retrieved_at"`

	NoCache bool `json:"nocache"`
}

// GetPrimaryID returns the primary ID of the JS2 User
func (u *JS2UserModel) GetPrimaryID() string {
	return u.Owner
}

// GetOwner returns the owner of the JS2 User
func (u *JS2UserModel) GetOwner() string {
	return u.Owner
}

// GetTACCUsername returns the TACCUsername of the JS2 User
func (u *JS2UserModel) GetTACCUsername() string {
	return u.TACCUsername
}

// GetRetrievedAt returns the retrival time of the JS2 User
func (u *JS2UserModel) GetRetrievedAt() time.Time {
	return u.RetrievedAt
}

// JS2PI is a struct for JetStream PI information
type JS2PI struct {
	Username  string `json:"username"`
	FirstName string `json:"firstname"`
	LastName  string `json:"lastname"`
}

// JS2Allocation is a struct for JetStream Allocation information
type JS2Allocation struct {
	ID               string    `json:"id"`
	Start            time.Time `json:"start"`
	End              time.Time `json:"end"`
	ProjectCode      string    `json:"project_code"`
	Resource         string    `json:"resource"`
	ComputeAllocated float64   `json:"compute_allocated"`
	ComputeUsed      float64   `json:"compute_used"`
	StorageAllocated float64   `json:"storage_allocated"`
}

// JS2Project is the standard interface for all JS2 Project implementations
type JS2Project interface {
	SessionContext

	GetPrimaryID() common.ID // points to ID
	GetID() common.ID
	GetOwner() string
	GetTitle() string
	GetDescription() string
	GetPI() JS2PI
	GetAllocations() []JS2Allocation
	GetRetrievedAt() time.Time
}

// JS2ProjectModel is the JS2 Project model
type JS2ProjectModel struct {
	Session

	ID          common.ID       `json:"id"`
	Owner       string          `json:"owner"`
	Title       string          `json:"title"`
	Description string          `json:"description,omitempty"`
	PI          JS2PI           `json:"pi"`
	Allocations []JS2Allocation `json:"allocations"`
	RetrievedAt time.Time       `json:"retrieved_at"`
}

// NewJS2ProjectID generates a new JS2ProjectID
func NewJS2ProjectID() common.ID {
	return common.NewID("js2project")
}

// GetPrimaryID returns the primary ID of the JS2 Project
func (p *JS2ProjectModel) GetPrimaryID() common.ID {
	return p.ID
}

// GetID returns the ID of the JS2 Project
func (p *JS2ProjectModel) GetID() common.ID {
	return p.ID
}

// GetOwner returns the owner of the JS2 Project
func (p *JS2ProjectModel) GetOwner() string {
	return p.Owner
}

// GetTitle returns the title of the JS2 Project
func (p *JS2ProjectModel) GetTitle() string {
	return p.Title
}

// GetDescription returns the description of the JS2 Project
func (p *JS2ProjectModel) GetDescription() string {
	return p.Description
}

// GetDescription returns the description of the JS2 Project
func (p *JS2ProjectModel) GetPI() JS2PI {
	return p.PI
}

// GetAllocations returns the allocations of the JS2 Project
func (p *JS2ProjectModel) GetAllocations() []JS2Allocation {
	return p.Allocations
}

// GetRetrievedAt returns the retrival time of the JS2 Project
func (p *JS2ProjectModel) GetRetrievedAt() time.Time {
	return p.RetrievedAt
}

// JS2ProjectListItemModel is the element type in JS2ProjectListModel
type JS2ProjectListItemModel struct {
	ID          common.ID       `json:"id"`
	Owner       string          `json:"owner"`
	Title       string          `json:"title"`
	Description string          `json:"description,omitempty"`
	PI          JS2PI           `json:"pi"`
	Allocations []JS2Allocation `json:"allocations"`
	RetrievedAt time.Time       `json:"retrieved_at"`
}

// JS2ProjectListModel is the JS2 Project list model
type JS2ProjectListModel struct {
	Session

	Projects []JS2ProjectListItemModel `json:"projects"`

	NoCache bool `json:"nocache"`
}

// GetProjects returns JS2 Projects
func (p *JS2ProjectListModel) GetProjects() []JS2Project {
	projects := make([]JS2Project, 0, len(p.Projects))

	for _, project := range p.Projects {
		projectModel := JS2ProjectModel{
			Session:     p.Session,
			ID:          project.ID,
			Owner:       project.Owner,
			Title:       project.Title,
			Description: project.Description,
			PI:          project.PI,
			Allocations: project.Allocations,
			RetrievedAt: project.RetrievedAt,
		}

		projects = append(projects, &projectModel)
	}

	return projects
}
