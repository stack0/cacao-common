package service

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	cloudevents "github.com/cloudevents/sdk-go/v2"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging"
)

// Nats subjects
const (
	// NatsSubjectProvider is the prefix of the queue name for Provider queries
	NatsSubjectProvider common.QueryOp = common.NatsQueryOpPrefix + "provider"

	// ProviderGetQueryOp is the queue name for ProviderGet query
	ProviderGetQueryOp common.QueryOp = NatsSubjectProvider + ".get"
	// ProviderListQueryOp is the queue name for ProviderList query
	ProviderListQueryOp common.QueryOp = NatsSubjectProvider + ".list"

	// ProviderCreateRequestedEvent is the cloudevent subject for ProviderCreate
	ProviderCreateRequestedEvent common.EventType = common.EventTypePrefix + "ProviderCreateRequested"
	// ProviderDeleteAdminRequestedEvent is the cloudevent subject for ProviderDeleteAdmin
	ProviderDeleteRequestedEvent common.EventType = common.EventTypePrefix + "ProviderDeleteRequested"
	// ProviderUpdateRequestedEvent is the cloudevent subject for ProviderUpdate
	ProviderUpdateRequestedEvent common.EventType = common.EventTypePrefix + "ProviderUpdateRequested"

	// ProviderCreatedEvent is the cloudevent subject for ProviderCreated
	ProviderCreatedEvent common.EventType = common.EventTypePrefix + "ProviderCreated"
	// ProviderDeletedEvent is the cloudevent subject for ProviderDeleted
	ProviderDeletedEvent common.EventType = common.EventTypePrefix + "ProviderDeleted"
	// ProviderUpdatedEvent is the cloudevent subject for ProviderUpdated
	ProviderUpdatedEvent common.EventType = common.EventTypePrefix + "ProviderUpdated"

	// ProviderCreateFailedEvent is the cloudevent subject for ProviderCreateFailed
	ProviderCreateFailedEvent common.EventType = common.EventTypePrefix + "ProviderCreateFailed"
	// ProviderDeleteFailedEvent is the cloudevent subject for ProviderDeleteFailed
	ProviderDeleteFailedEvent common.EventType = common.EventTypePrefix + "ProviderDeleteFailed"
	// ProviderUpdateFailedEvent is the cloudevent subject for ProviderUpdateFailed
	ProviderUpdateFailedEvent common.EventType = common.EventTypePrefix + "ProviderUpdateFailed"
)

// Nats Client
type natsProviderClient struct {
	actor      string
	emulator   string
	natsConfig messaging.NatsConfig
	stanConfig messaging.StanConfig
	ctx        context.Context
}

// NewNatsProviderClient creates a client for provider microservice that uses NATS for communication
func NewNatsProviderClient(ctx context.Context, actor string, emulator string, natsConfig messaging.NatsConfig, stanConfig messaging.StanConfig) (ProviderClient, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "NewNatsProviderClient",
	})

	logger.Trace("Creating a NatsProviderClient")

	if len(actor) == 0 {
		return nil, fmt.Errorf("actor is empty")
	}

	if ctx == nil {
		ctx = context.Background()
	}

	natsConfigCopy := natsConfig
	if natsConfigCopy.RequestTimeout == -1 {
		natsConfigCopy.RequestTimeout = int(messaging.DefaultNatsRequestTimeout.Seconds())
	}

	stanConfigCopy := stanConfig
	if stanConfigCopy.EventsTimeout == -1 {
		stanConfigCopy.EventsTimeout = int(messaging.DefaultStanEventsTimeout.Seconds())
	}

	client := natsProviderClient{
		actor:      actor,
		emulator:   emulator,
		natsConfig: natsConfigCopy,
		stanConfig: stanConfigCopy,
		ctx:        ctx,
	}

	return &client, nil
}

func waitProviderResponse(c chan common.ServiceRequestResult, timeout time.Duration) (bool, common.ServiceRequestResult) {
	select {
	case res := <-c:
		return true, res
	case <-time.After(timeout):
		return false, common.ServiceRequestResult{
			Data: nil,
			Status: common.HTTPStatus{
				Message: "response timeout",
				Code:    http.StatusRequestTimeout,
			},
		}
	}
}

// Get returns the provider with the given provider ID
func (client *natsProviderClient) Get(providerID common.ID) (Provider, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsProviderClient.Get",
	})

	if len(providerID) == 0 {
		return nil, fmt.Errorf("invalid Provider ID")
	}

	natsConn, err := messaging.ConnectNatsForServiceClient(&client.natsConfig)
	if err != nil {
		logger.WithError(err).Error("Unable to connect to NATS")
		return nil, err
	}

	defer natsConn.Disconnect()

	request := ProviderModel{
		ID: providerID,
	}

	timeout := time.Duration(client.natsConfig.RequestTimeout) * time.Second
	timeoutCtx, cancel := context.WithTimeout(client.ctx, timeout)
	defer cancel()

	responseBytes, err := natsConn.Request(timeoutCtx, ProviderGetQueryOp, request)
	if err != nil {
		logger.WithError(err).Error("unable to request a get provider event")
		return nil, err
	}

	var response common.ServiceRequestResult
	err = json.Unmarshal(responseBytes, &response)
	if err != nil {
		logger.WithError(err).Error("unable to unmarshal response JSON to object")
		return nil, err
	}

	if response.Status.Code == http.StatusOK {
		var providerModel ProviderModel

		providerJSON, err := json.Marshal(response.Data)
		if err != nil {
			logger.WithError(err).Error("unable to marshal response data to JSON")
			return nil, err
		}

		err = json.Unmarshal(providerJSON, &providerModel)
		if err != nil {
			logger.WithError(err).Error("unable to unmarshal response JSON to provider object")
			return nil, err
		}

		return &providerModel, nil
	}

	return nil, fmt.Errorf("unable to get provider for ID %s", providerID)
}

// List returns all providers for the user
func (client *natsProviderClient) List() ([]Provider, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsProviderClient.List",
	})

	natsConn, err := messaging.ConnectNatsForServiceClient(&client.natsConfig)
	if err != nil {
		logger.WithError(err).Error("Unable to connect to NATS")
		return nil, err
	}

	defer natsConn.Disconnect()

	request := Session{
		SessionActor:    client.actor,
		SessionEmulator: client.emulator,
	}

	timeout := time.Duration(client.natsConfig.RequestTimeout) * time.Second
	timeoutCtx, cancel := context.WithTimeout(client.ctx, timeout)
	defer cancel()

	responseBytes, err := natsConn.Request(timeoutCtx, ProviderListQueryOp, request)
	if err != nil {
		logger.WithError(err).Error("unable to request a list providers event")
		return nil, err
	}

	var response common.ServiceRequestResult
	err = json.Unmarshal(responseBytes, &response)
	if err != nil {
		logger.WithError(err).Error("unable to unmarshal response JSON to object")
		return nil, err
	}

	if response.Status.Code == http.StatusOK {
		var providerModels []ProviderModel

		providerJSON, err := json.Marshal(response.Data)
		if err != nil {
			logger.WithError(err).Error("unable to marshal response data to JSON")
			return nil, err
		}

		err = json.Unmarshal(providerJSON, &providerModels)
		if err != nil {
			logger.WithError(err).Error("unable to unmarshal response JSON to provider objects")
			return nil, err
		}

		providers := []Provider{}
		for i := range providerModels {
			providers = append(providers, &providerModels[i])
		}

		return providers, nil
	}

	return nil, fmt.Errorf("unable to list providers")
}

// Create creates a new provider
func (client *natsProviderClient) Create(provider Provider) (common.ID, common.TransactionID, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsProviderClient.Create",
	})

	if len(provider.GetName()) == 0 {
		return "", "", fmt.Errorf("invalid Provider Name")
	}

	SenderNatsConfig := client.natsConfig
	SenderNatsConfig.ClientID = SenderNatsConfig.ClientID + "-" + string(common.NewID("Sender"))

	stanConn, err := messaging.ConnectStanForServiceClient(&SenderNatsConfig, &client.stanConfig)
	if err != nil {
		logger.WithError(err).Error("Unable to connect to NATS Streaming")
		return "", "", err
	}

	defer stanConn.Disconnect()

	eventSourceNatsConfig := client.natsConfig
	eventSourceNatsConfig.ClientID = eventSourceNatsConfig.ClientID + "-" + string(common.NewID("EventSource"))

	eventSource := messaging.NewEventSource(eventSourceNatsConfig, client.stanConfig)
	if err != nil {
		logger.WithError(err).Error("Unable to connect to NATS Streaming")
		return "", "", err
	}

	request := ProviderRequest{
		Session: Session{
			SessionActor:    client.actor,
			SessionEmulator: client.emulator,
		},
		ProviderModel: ProviderModel{
			Name:     provider.GetName(),
			URL:      provider.GetURL(),
			Type:     provider.GetType(),
			Metadata: provider.GetMetadata(),
		},
	}

	eventsSubscribe := []common.EventType{
		ProviderCreatedEvent,
		ProviderCreateFailedEvent,
	}

	transactionID := messaging.NewTransactionID()

	responseChannel := make(chan common.ServiceRequestResult)

	callback := func(ev common.EventType, ce cloudevents.Event) {
		if ev == ProviderCreatedEvent || ev == ProviderCreateFailedEvent {
			var response common.ServiceRequestResult
			err := json.Unmarshal(ce.Data(), &response)
			if err != nil {
				logger.WithError(err).Error("unable to unmarshal a provider create response")
				responseChannel <- common.ServiceRequestResult{
					Data: nil,
					Status: common.HTTPStatus{
						Message: "unable to unmarshal a provider create response",
						Code:    http.StatusInternalServerError,
					},
				}
				return
			}

			responseChannel <- response
			return
		}

		responseChannel <- common.ServiceRequestResult{
			Data: nil,
			Status: common.HTTPStatus{
				Message: fmt.Sprintf("unknown event type %s", ev),
				Code:    http.StatusInternalServerError,
			},
		}
	}

	listener := messaging.Listener{
		Callback:   callback,
		ListenOnce: true,
	}

	listenerID, err := eventSource.AddListenerMultiEventType(eventsSubscribe, transactionID, listener)
	if err != nil {
		logger.WithError(err).Error("unable to add an event listener")
		return "", "", err
	}
	defer eventSource.RemoveListenerByID(listenerID)

	err = stanConn.PublishWithTransactionID(ProviderCreateRequestedEvent, request, transactionID)
	if err != nil {
		logger.WithError(err).Error("unable to publish a create provider event")
		return "", "", err
	}

	// wait for response
	responseReceived, response := waitProviderResponse(responseChannel, time.Duration(client.natsConfig.RequestTimeout)*time.Second)
	if responseReceived {
		if response.Status.Code == http.StatusAccepted || response.Status.Code == http.StatusOK {
			p := response.Data.(map[string]interface{})
			return common.ID(p["id"].(string)), transactionID, nil
		}

		err = fmt.Errorf(response.Status.Message)
		logger.Error(err)
		return "", "", err
	}

	// timed out
	err = fmt.Errorf("unable to create a provider - provider service is not responding")
	logger.WithError(err).Error("timeout")
	return "", "", err
}

// Delete deletes the provider with the provider ID
func (client *natsProviderClient) Delete(providerID common.ID) (common.TransactionID, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsProviderClient.Delete",
	})

	if len(providerID) == 0 {
		return "", fmt.Errorf("invalid Provider ID")
	}

	SenderNatsConfig := client.natsConfig
	SenderNatsConfig.ClientID = SenderNatsConfig.ClientID + "-" + string(common.NewID("Sender"))

	stanConn, err := messaging.ConnectStanForServiceClient(&SenderNatsConfig, &client.stanConfig)
	if err != nil {
		logger.WithError(err).Error("Unable to connect to NATS Streaming")
		return "", err
	}

	defer stanConn.Disconnect()

	eventSourceNatsConfig := client.natsConfig
	eventSourceNatsConfig.ClientID = eventSourceNatsConfig.ClientID + "-" + string(common.NewID("EventSource"))

	eventSource := messaging.NewEventSource(eventSourceNatsConfig, client.stanConfig)
	if err != nil {
		logger.WithError(err).Error("Unable to connect to NATS Streaming")
		return "", err
	}

	request := ProviderRequest{
		Session: Session{
			SessionActor:    client.actor,
			SessionEmulator: client.emulator,
		},
		ProviderModel: ProviderModel{
			ID: providerID,
		},
	}

	eventsSubscribe := []common.EventType{
		ProviderDeletedEvent,
		ProviderDeleteFailedEvent,
	}

	transactionID := messaging.NewTransactionID()

	responseChannel := make(chan common.ServiceRequestResult)

	callback := func(ev common.EventType, ce cloudevents.Event) {
		if ev == ProviderDeletedEvent || ev == ProviderDeleteFailedEvent {
			var response common.ServiceRequestResult
			err := json.Unmarshal(ce.Data(), &response)
			if err != nil {
				logger.WithError(err).Error("unable to unmarshal a provider delete response")
				responseChannel <- common.ServiceRequestResult{
					Data: nil,
					Status: common.HTTPStatus{
						Message: "unable to unmarshal a provider delete response",
						Code:    http.StatusInternalServerError,
					},
				}
				return
			}

			responseChannel <- response
			return
		}

		responseChannel <- common.ServiceRequestResult{
			Data: nil,
			Status: common.HTTPStatus{
				Message: fmt.Sprintf("unknown event type %s", ev),
				Code:    http.StatusInternalServerError,
			},
		}
	}

	listener := messaging.Listener{
		Callback:   callback,
		ListenOnce: true,
	}

	listenerID, err := eventSource.AddListenerMultiEventType(eventsSubscribe, transactionID, listener)
	if err != nil {
		logger.WithError(err).Error("unable to add an event listener")
		return "", err
	}
	defer eventSource.RemoveListenerByID(listenerID)

	err = stanConn.PublishWithTransactionID(ProviderDeleteRequestedEvent, request, transactionID)
	if err != nil {
		logger.WithError(err).Error("unable to publish a delete provider event")
		return "", err
	}

	// wait for response
	responseReceived, response := waitProviderResponse(responseChannel, time.Duration(client.natsConfig.RequestTimeout)*time.Second)
	if responseReceived {
		if response.Status.Code == http.StatusAccepted || response.Status.Code == http.StatusOK {
			return transactionID, nil
		}

		err = fmt.Errorf(response.Status.Message)
		logger.Error(err)
		return "", err
	}

	// timed out
	err = fmt.Errorf("unable to delete a provider - provider service is not responding")
	logger.WithError(err).Error("timeout")
	return "", err
}

// Update updates the provider with the provider ID
func (client *natsProviderClient) Update(provider Provider) (common.TransactionID, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsProviderClient.Update",
	})

	if len(provider.GetID()) == 0 {
		return "", fmt.Errorf("invalid Provider ID")
	}

	SenderNatsConfig := client.natsConfig
	SenderNatsConfig.ClientID = SenderNatsConfig.ClientID + "-" + string(common.NewID("Sender"))

	stanConn, err := messaging.ConnectStanForServiceClient(&SenderNatsConfig, &client.stanConfig)
	if err != nil {
		logger.WithError(err).Error("Unable to connect to NATS Streaming")
		return "", err
	}

	defer stanConn.Disconnect()

	eventSourceNatsConfig := client.natsConfig
	eventSourceNatsConfig.ClientID = eventSourceNatsConfig.ClientID + "-" + string(common.NewID("EventSource"))

	eventSource := messaging.NewEventSource(eventSourceNatsConfig, client.stanConfig)
	if err != nil {
		logger.WithError(err).Error("Unable to connect to NATS Streaming")
		return "", err
	}

	request := ProviderRequest{
		Session: Session{
			SessionActor:    client.actor,
			SessionEmulator: client.emulator,
		},
		ProviderModel: ProviderModel{
			ID:       provider.GetID(),
			Name:     provider.GetName(),
			URL:      provider.GetURL(),
			Type:     provider.GetType(),
			Metadata: provider.GetMetadata(),
		},
	}

	eventsSubscribe := []common.EventType{
		ProviderUpdatedEvent,
		ProviderUpdateFailedEvent,
	}

	transactionID := messaging.NewTransactionID()

	responseChannel := make(chan common.ServiceRequestResult)

	callback := func(ev common.EventType, ce cloudevents.Event) {
		if ev == ProviderUpdatedEvent || ev == ProviderUpdateFailedEvent {
			var response common.ServiceRequestResult
			err := json.Unmarshal(ce.Data(), &response)
			if err != nil {
				logger.WithError(err).Error("unable to unmarshal a provider update response")
				responseChannel <- common.ServiceRequestResult{
					Data: nil,
					Status: common.HTTPStatus{
						Message: "unable to unmarshal a provider update response",
						Code:    http.StatusInternalServerError,
					},
				}
				return
			}

			responseChannel <- response
			return
		}

		responseChannel <- common.ServiceRequestResult{
			Data: nil,
			Status: common.HTTPStatus{
				Message: fmt.Sprintf("unknown event type %s", ev),
				Code:    http.StatusInternalServerError,
			},
		}
	}

	listener := messaging.Listener{
		Callback:   callback,
		ListenOnce: true,
	}

	listenerID, err := eventSource.AddListenerMultiEventType(eventsSubscribe, transactionID, listener)
	if err != nil {
		logger.WithError(err).Error("unable to add an event listener")
		return "", err
	}
	defer eventSource.RemoveListenerByID(listenerID)

	err = stanConn.PublishWithTransactionID(ProviderUpdateRequestedEvent, request, transactionID)
	if err != nil {
		logger.WithError(err).Error("unable to publish an update provider event")
		return "", err
	}

	// wait for response
	responseReceived, response := waitProviderResponse(responseChannel, time.Duration(client.natsConfig.RequestTimeout)*time.Second)
	if responseReceived {
		if response.Status.Code == http.StatusAccepted || response.Status.Code == http.StatusOK {
			return transactionID, nil
		}

		err = fmt.Errorf(response.Status.Message)
		logger.Error(err)
		return "", err
	}

	// timed out
	err = fmt.Errorf("unable to update a provider - provider service is not responding")
	logger.WithError(err).Error("timeout")
	return "", err
}
