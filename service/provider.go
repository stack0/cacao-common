package service

import (
	"time"

	"gitlab.com/cyverse/cacao-common/common"
)

// ProviderClient
type ProviderClient interface {
	Create(provider Provider) (common.ID, common.TransactionID, error)
	Delete(id common.ID) (common.TransactionID, error)
	Get(id common.ID) (Provider, error)
	List() ([]Provider, error)
	Update(provider Provider) (common.TransactionID, error)
}

// Provider is the standard interface for all Provider implementations
type Provider interface {
	GetID() common.ID
	GetName() string
	GetType() string
	GetURL() string
	GetCreatedAt() time.Time
	GetUpdatedAt() time.Time
	GetMetadata() map[string]interface{}
}

// ProviderModel
type ProviderModel struct {
	ID        common.ID              `json:"id" bson:"_id"`
	Name      string                 `json:"name" bson:"name"`
	Type      string                 `json:"type" bson:"type"`
	URL       string                 `json:"url" bson:"url"`
	CreatedAt time.Time              `json:"created_at,omitempty" bson:"created_at"`
	UpdatedAt time.Time              `json:"updated_at,omitempty" bson:"updated_at"`
	Metadata  map[string]interface{} `json:"metadata,omitempty" bson:"metadata"`
}

// ProviderRequest
type ProviderRequest struct {
	Session
	ProviderModel
}

// NewProviderModel ...
func NewProviderModel(p Provider) ProviderModel {
	return ProviderModel{
		ID:        p.GetID(),
		Name:      p.GetName(),
		Type:      p.GetType(),
		URL:       p.GetURL(),
		CreatedAt: p.GetCreatedAt(),
		UpdatedAt: p.GetUpdatedAt(),
		Metadata:  p.GetMetadata(),
	}
}

func (p *ProviderModel) GetCreatedAt() time.Time {
	return p.CreatedAt
}

func (p *ProviderModel) GetID() common.ID {
	return p.ID
}

func (p *ProviderModel) GetMetadata() map[string]interface{} {
	return p.Metadata
}
func (p *ProviderModel) GetName() string {
	return p.Name
}

func (p *ProviderModel) GetType() string {
	return p.Type
}

func (p *ProviderModel) GetUpdatedAt() time.Time {
	return p.UpdatedAt
}

func (p *ProviderModel) GetURL() string {
	return p.URL
}
