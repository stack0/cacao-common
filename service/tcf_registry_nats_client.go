package service

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	cloudevents "github.com/cloudevents/sdk-go/v2"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging"
)

// Nats subjects
const (
	// NatsSubjectTCFRegistry is the prefix of the queue name for TCF Registry queries
	NatsSubjectTCFRegistry common.QueryOp = common.NatsQueryOpPrefix + "tcfregistry"

	// TCFRegistryEntryListQueryOp is the queue name for TCFRegistryEntryList query
	TCFRegistryEntryListQueryOp common.QueryOp = NatsSubjectTCFRegistry + ".list"
	// TCFRegistryEntryGetQueryOp is the queue name for TCFRegistryEntryGet query
	TCFRegistryEntryGetQueryOp common.QueryOp = NatsSubjectTCFRegistry + ".get"
	// TCFRegistryQueryQueryOp is the queue name for TCFRegistryEntryQuery query
	TCFRegistryQueryQueryOp common.QueryOp = NatsSubjectTCFRegistry + ".query"

	// TCFRegistryEntryCreateRequestedEvent is the cloudevent subject for TCFRegistryEntryCreate
	TCFRegistryEntryCreateRequestedEvent common.EventType = common.EventTypePrefix + "TCFRegistryEntryCreateRequested"
	// TCFRegistryEntryDeleteRequestedEvent is the cloudevent subject for TCFRegistryEntryDelete
	TCFRegistryEntryDeleteRequestedEvent common.EventType = common.EventTypePrefix + "TCFRegistryEntryDeleteRequested"
	// TCFRegistryEntryUpdateRequestedEvent is the cloudevent subject for TCFRegistryEntryUpdate
	TCFRegistryEntryUpdateRequestedEvent common.EventType = common.EventTypePrefix + "TCFRegistryEntryUpdateRequested"

	// TCFRegistryEntryCreatedEvent is the cloudevent subject for TCFRegistryEntryCreated
	TCFRegistryEntryCreatedEvent common.EventType = common.EventTypePrefix + "TCFRegistryEntryCreated"
	// TCFRegistryEntryDeletedEvent is the cloudevent subject for TCFRegistryEntryDeleted
	TCFRegistryEntryDeletedEvent common.EventType = common.EventTypePrefix + "TCFRegistryEntryDeleted"
	// TCFRegistryEntryUpdatedEvent is the cloudevent subject for TCFRegistryEntryUpdated
	TCFRegistryEntryUpdatedEvent common.EventType = common.EventTypePrefix + "TCFRegistryEntryUpdated"

	// TCFRegistryEntryCreateFailedEvent is the cloudevent subject for TCFRegistryEntryCreateFailed
	TCFRegistryEntryCreateFailedEvent common.EventType = common.EventTypePrefix + "TCFRegistryEntryCreateFailed"
	// TCFRegistryEntryDeleteFailedEvent is the cloudevent subject for TCFRegistryEntryDeleteFailed
	TCFRegistryEntryDeleteFailedEvent common.EventType = common.EventTypePrefix + "TCFRegistryEntryDeleteFailed"
	// TCFRegistryEntryUpdateFailedEvent is the cloudevent subject for TCFRegistryEntryUpdateFailed
	TCFRegistryEntryUpdateFailedEvent common.EventType = common.EventTypePrefix + "TCFRegistryEntryUpdateFailed"
)

// Nats Client
type natsTCFRegistryClient struct {
	actor      string
	emulator   string
	natsConfig messaging.NatsConfig
	stanConfig messaging.StanConfig
	ctx        context.Context
}

// NewNatsTCFRegistryClient creates a client for tcf registry microservice that uses NATS for communication
func NewNatsTCFRegistryClient(ctx context.Context, actor string, emulator string, natsConfig messaging.NatsConfig, stanConfig messaging.StanConfig) (TCFRegistryClient, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "NewNatsTCFRegistryClient",
	})

	logger.Trace("Creating a NatsTCFRegistryClient")

	if len(actor) == 0 {
		return nil, NewCacaoInvalidParameterError("actor is empty")
	}

	if ctx == nil {
		ctx = context.Background()
	}

	natsConfigCopy := natsConfig
	if natsConfigCopy.RequestTimeout == -1 {
		natsConfigCopy.RequestTimeout = int(messaging.DefaultNatsRequestTimeout.Seconds())
	}

	stanConfigCopy := stanConfig
	if stanConfigCopy.EventsTimeout == -1 {
		stanConfigCopy.EventsTimeout = int(messaging.DefaultStanEventsTimeout.Seconds())
	}

	client := natsTCFRegistryClient{
		actor:      actor,
		emulator:   emulator,
		natsConfig: natsConfigCopy,
		stanConfig: stanConfigCopy,
		ctx:        ctx,
	}

	return &client, nil
}

func waitTCFRegistryResponse(c chan TCFRegistryEntryModel, timeout time.Duration) (bool, TCFRegistryEntryModel) {
	select {
	case res := <-c:
		return true, res
	case <-time.After(timeout):
		return false, TCFRegistryEntryModel{}
	}
}

// Get returns the tcf registry entry with the given name
func (client *natsTCFRegistryClient) Get(name string) (TCFRegistryEntry, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsTCFRegistryClient.Get",
	})

	if len(name) == 0 {
		return nil, NewCacaoInvalidParameterError("invalid tcf registry entry name")
	}

	natsConn, err := messaging.ConnectNatsForServiceClient(&client.natsConfig)
	if err != nil {
		errorMessage := "unable to connect to NATS"
		logger.WithError(err).Error(errorMessage)
		return nil, NewCacaoCommunicationError(errorMessage)
	}

	defer natsConn.Disconnect()

	request := TCFRegistryEntryModel{
		Session: Session{
			SessionActor:    client.actor,
			SessionEmulator: client.emulator,
		},
		Name: name,
	}

	timeout := time.Duration(client.natsConfig.RequestTimeout) * time.Second
	timeoutCtx, cancel := context.WithTimeout(client.ctx, timeout)
	defer cancel()

	responseBytes, err := natsConn.Request(timeoutCtx, TCFRegistryEntryGetQueryOp, request)
	if err != nil {
		errorMessage := "unable to request a get tcf registry entry event"
		logger.WithError(err).Error(errorMessage)
		return nil, NewCacaoCommunicationError(errorMessage)
	}

	var tcfRegistryEntryModel TCFRegistryEntryModel
	err = json.Unmarshal(responseBytes, &tcfRegistryEntryModel)
	if err != nil {
		errorMessage := "unable to unmarshal response JSON to tcf registry entry object"
		logger.WithError(err).Error(errorMessage)
		return nil, NewCacaoMarshalError(errorMessage)
	}

	serviceError := tcfRegistryEntryModel.GetServiceError()
	if serviceError != nil {
		logger.Error(serviceError)
		return nil, serviceError
	}
	return &tcfRegistryEntryModel, nil
}

// Query queries to query target defined in the tcf registry entry with the given name and returns results
func (client *natsTCFRegistryClient) Query(name string, queryParams map[string]string) (TCFRegistryQueryResult, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsTCFRegistryClient.Query",
	})

	if len(name) == 0 {
		return nil, NewCacaoInvalidParameterError("invalid tcf registry entry name")
	}

	natsConn, err := messaging.ConnectNatsForServiceClient(&client.natsConfig)
	if err != nil {
		errorMessage := "unable to connect to NATS"
		logger.WithError(err).Error(errorMessage)
		return nil, NewCacaoCommunicationError(errorMessage)
	}

	defer natsConn.Disconnect()

	request := TCFRegistryEntryModel{
		Session: Session{
			SessionActor:    client.actor,
			SessionEmulator: client.emulator,
		},
		Name:        name,
		QueryParams: queryParams,
	}

	timeout := time.Duration(client.natsConfig.RequestTimeout) * time.Second
	timeoutCtx, cancel := context.WithTimeout(client.ctx, timeout)
	defer cancel()

	responseBytes, err := natsConn.Request(timeoutCtx, TCFRegistryQueryQueryOp, request)
	if err != nil {
		errorMessage := "unable to request a query tcf registry event"
		logger.WithError(err).Error(errorMessage)
		return nil, NewCacaoCommunicationError(errorMessage)
	}

	var tcfRegistryQueryResultModel TCFRegistryQueryResultModel
	err = json.Unmarshal(responseBytes, &tcfRegistryQueryResultModel)
	if err != nil {
		errorMessage := "unable to unmarshal response JSON to tcf registry query result object"
		logger.WithError(err).Error(errorMessage)
		return nil, NewCacaoMarshalError(errorMessage)
	}

	serviceError := tcfRegistryQueryResultModel.GetServiceError()
	if serviceError != nil {
		logger.Error(serviceError)
		return nil, serviceError
	}
	return &tcfRegistryQueryResultModel, nil
}

// List returns all tcf registry entries
func (client *natsTCFRegistryClient) List() ([]TCFRegistryEntry, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsTCFRegistryClient.List",
	})

	natsConn, err := messaging.ConnectNatsForServiceClient(&client.natsConfig)
	if err != nil {
		errorMessage := "unable to connect to NATS"
		logger.WithError(err).Error(errorMessage)
		return nil, NewCacaoCommunicationError(errorMessage)
	}

	defer natsConn.Disconnect()

	request := Session{
		SessionActor:    client.actor,
		SessionEmulator: client.emulator,
	}

	timeout := time.Duration(client.natsConfig.RequestTimeout) * time.Second
	timeoutCtx, cancel := context.WithTimeout(client.ctx, timeout)
	defer cancel()

	responseBytes, err := natsConn.Request(timeoutCtx, TCFRegistryEntryListQueryOp, request)
	if err != nil {
		errorMessage := "unable to request a list tcf registry entries event"
		logger.WithError(err).Error(errorMessage)
		return nil, NewCacaoCommunicationError(errorMessage)
	}

	var tcfRegistryEntryListModel TCFRegistryEntryListModel
	err = json.Unmarshal(responseBytes, &tcfRegistryEntryListModel)
	if err != nil {
		errorMessage := "unable to unmarshal response JSON to tcf registry entry list object"
		logger.WithError(err).Error(errorMessage)
		return nil, NewCacaoMarshalError(errorMessage)
	}

	serviceError := tcfRegistryEntryListModel.GetServiceError()
	if serviceError != nil {
		logger.Error(serviceError)
		return nil, serviceError
	}

	return tcfRegistryEntryListModel.GetEntries(), nil
}

// Create creates a new tcf registry entry
func (client *natsTCFRegistryClient) Create(entry TCFRegistryEntry) error {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsTCFRegistryClient.Create",
	})

	if len(entry.GetName()) == 0 {
		return NewCacaoInvalidParameterError("invalid TCFRegistryEntry Name")
	}

	if len(entry.GetType()) == 0 {
		return NewCacaoInvalidParameterError("invalid TCFRegistryEntry Type")
	}

	if len(entry.GetQueryTarget()) == 0 {
		return NewCacaoInvalidParameterError("invalid TCFRegistryEntry QueryTarget")
	}

	SenderNatsConfig := client.natsConfig
	SenderNatsConfig.ClientID = SenderNatsConfig.ClientID + "-" + string(common.NewID("Sender"))

	stanConn, err := messaging.ConnectStanForServiceClient(&SenderNatsConfig, &client.stanConfig)
	if err != nil {
		errorMessage := "unable to connect to NATS Streaming"
		logger.WithError(err).Error(errorMessage)
		return NewCacaoCommunicationError(errorMessage)
	}

	defer stanConn.Disconnect()

	eventSourceNatsConfig := client.natsConfig
	eventSourceNatsConfig.ClientID = eventSourceNatsConfig.ClientID + "-" + string(common.NewID("EventSource"))

	eventSource := messaging.NewEventSource(eventSourceNatsConfig, client.stanConfig)
	if err != nil {
		errorMessage := "unable to create EventSource"
		logger.WithError(err).Error(errorMessage)
		return NewCacaoCommunicationError(errorMessage)
	}

	request := TCFRegistryEntryModel{
		Session: Session{
			SessionActor:    client.actor,
			SessionEmulator: client.emulator,
		},
		Name:                      entry.GetName(),
		Description:               entry.GetDescription(),
		Type:                      entry.GetType(),
		QueryTarget:               entry.GetQueryTarget(),
		QueryData:                 entry.GetQueryData(),
		QueryResultJSONPathFilter: entry.GetQueryResultJSONPathFilter(),
	}

	eventsSubscribe := []common.EventType{
		TCFRegistryEntryCreatedEvent,
		TCFRegistryEntryCreateFailedEvent,
	}

	transactionID := messaging.NewTransactionID()

	responseChannel := make(chan TCFRegistryEntryModel)

	callback := func(ev common.EventType, ce cloudevents.Event) {
		if ev == TCFRegistryEntryCreatedEvent || ev == TCFRegistryEntryCreateFailedEvent {
			var tcfRegistryEntryModel TCFRegistryEntryModel
			err := json.Unmarshal(ce.Data(), &tcfRegistryEntryModel)
			if err != nil {
				errorMessage := "unable to unmarshal a tcf registry entry create response"
				logger.WithError(err).Error(errorMessage)
				responseChannel <- TCFRegistryEntryModel{
					Session: Session{
						SessionActor:    client.actor,
						SessionEmulator: client.emulator,
						ServiceError:    NewCacaoMarshalError(errorMessage).GetBase(),
					},
				}
				return
			}

			responseChannel <- tcfRegistryEntryModel
			return
		}

		errorMessage := fmt.Sprintf("unknown event type %s", ev)
		responseChannel <- TCFRegistryEntryModel{
			Session: Session{
				SessionActor:    client.actor,
				SessionEmulator: client.emulator,
				ServiceError:    NewCacaoCommunicationError(errorMessage).GetBase(),
			},
		}
	}

	listener := messaging.Listener{
		Callback:   callback,
		ListenOnce: true,
	}

	listenerID, err := eventSource.AddListenerMultiEventType(eventsSubscribe, transactionID, listener)
	if err != nil {
		errorMessage := "unable to add an event listener"
		logger.WithError(err).Error(errorMessage)
		return NewCacaoCommunicationError(errorMessage)
	}
	defer eventSource.RemoveListenerByID(listenerID)

	err = stanConn.PublishWithTransactionID(TCFRegistryEntryCreateRequestedEvent, request, transactionID)
	if err != nil {
		errorMessage := "unable to publish a create tcf registry entry event"
		logger.WithError(err).Error(errorMessage)
		return NewCacaoCommunicationError(errorMessage)
	}

	// wait for response
	responseReceived, response := waitTCFRegistryResponse(responseChannel, time.Duration(client.natsConfig.RequestTimeout)*time.Second)
	if responseReceived {
		serviceError := response.GetServiceError()
		if serviceError == nil {
			return nil
		}

		logger.Error(serviceError)
		return serviceError
	}

	// timed out
	logger.Error("unable to create a tcf registry entry - tcf registry service is not responding")
	return NewCacaoTimeoutError("unable to create a tcf registry entry - tcf registry service is not responding")
}

// Update updates the tcf registry entry with the name
func (client *natsTCFRegistryClient) Update(entry TCFRegistryEntry) error {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsTCFRegistryClient.Update",
	})

	if len(entry.GetName()) == 0 {
		return NewCacaoInvalidParameterError("invalid TCFRegistryEntry Name")
	}

	if len(entry.GetType()) == 0 {
		return NewCacaoInvalidParameterError("invalid TCFRegistryEntry Type")
	}

	if len(entry.GetQueryTarget()) == 0 {
		return NewCacaoInvalidParameterError("invalid TCFRegistryEntry QueryTarget")
	}

	SenderNatsConfig := client.natsConfig
	SenderNatsConfig.ClientID = SenderNatsConfig.ClientID + "-" + string(common.NewID("Sender"))

	stanConn, err := messaging.ConnectStanForServiceClient(&SenderNatsConfig, &client.stanConfig)
	if err != nil {
		errorMessage := "unable to connect to NATS Streaming"
		logger.WithError(err).Error(errorMessage)
		return NewCacaoCommunicationError(errorMessage)
	}

	defer stanConn.Disconnect()

	eventSourceNatsConfig := client.natsConfig
	eventSourceNatsConfig.ClientID = eventSourceNatsConfig.ClientID + "-" + string(common.NewID("EventSource"))

	eventSource := messaging.NewEventSource(eventSourceNatsConfig, client.stanConfig)
	if err != nil {
		errorMessage := "unable to create EventSource"
		logger.WithError(err).Error(errorMessage)
		return NewCacaoCommunicationError(errorMessage)
	}

	request := TCFRegistryEntryModel{
		Session: Session{
			SessionActor:    client.actor,
			SessionEmulator: client.emulator,
		},
		Name:                      entry.GetName(),
		Description:               entry.GetDescription(),
		Type:                      entry.GetType(),
		QueryTarget:               entry.GetQueryTarget(),
		QueryData:                 entry.GetQueryData(),
		QueryResultJSONPathFilter: entry.GetQueryResultJSONPathFilter(),
	}

	eventsSubscribe := []common.EventType{
		TCFRegistryEntryUpdatedEvent,
		TCFRegistryEntryUpdateFailedEvent,
	}

	transactionID := messaging.NewTransactionID()

	responseChannel := make(chan TCFRegistryEntryModel)

	callback := func(ev common.EventType, ce cloudevents.Event) {
		if ev == TCFRegistryEntryUpdatedEvent || ev == TCFRegistryEntryUpdateFailedEvent {
			var tcfRegistryEntryModel TCFRegistryEntryModel
			err := json.Unmarshal(ce.Data(), &tcfRegistryEntryModel)
			if err != nil {
				errorMessage := "unable to unmarshal a tcf registry entry update response"
				logger.WithError(err).Error(errorMessage)
				responseChannel <- TCFRegistryEntryModel{
					Session: Session{
						SessionActor:    client.actor,
						SessionEmulator: client.emulator,
						ServiceError:    NewCacaoMarshalError(errorMessage).GetBase(),
					},
				}
				return
			}

			responseChannel <- tcfRegistryEntryModel
			return
		}

		errorMessage := fmt.Sprintf("unknown event type %s", ev)
		responseChannel <- TCFRegistryEntryModel{
			Session: Session{
				SessionActor:    client.actor,
				SessionEmulator: client.emulator,
				ServiceError:    NewCacaoCommunicationError(errorMessage).GetBase(),
			},
		}
	}

	listener := messaging.Listener{
		Callback:   callback,
		ListenOnce: true,
	}

	listenerID, err := eventSource.AddListenerMultiEventType(eventsSubscribe, transactionID, listener)
	if err != nil {
		errorMessage := "unable to add an event listener"
		logger.WithError(err).Error(errorMessage)
		return NewCacaoCommunicationError(errorMessage)
	}
	defer eventSource.RemoveListenerByID(listenerID)

	err = stanConn.PublishWithTransactionID(TCFRegistryEntryUpdateRequestedEvent, request, transactionID)
	if err != nil {
		errorMessage := "unable to publish a update tcf registry entry event"
		logger.WithError(err).Error(errorMessage)
		return NewCacaoCommunicationError(errorMessage)
	}

	// wait for response
	responseReceived, response := waitTCFRegistryResponse(responseChannel, time.Duration(client.natsConfig.RequestTimeout)*time.Second)
	if responseReceived {
		serviceError := response.GetServiceError()
		if serviceError == nil {
			return nil
		}

		logger.Error(serviceError)
		return serviceError
	}

	// timed out
	logger.Error("unable to update a tcf registry entry - tcf registry service is not responding")
	return NewCacaoTimeoutError("unable to update a tcf registry entry - tcf registry service is not responding")
}

// UpdateFields updates the selected fields of the tcf registry entry with the name
func (client *natsTCFRegistryClient) UpdateFields(entry TCFRegistryEntry, updateFieldNames []string) error {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsTCFRegistryClient.UpdateFields",
	})

	if len(entry.GetName()) == 0 {
		return NewCacaoInvalidParameterError("invalid TCFRegistryEntry Name")
	}

	if len(updateFieldNames) == 0 {
		return nil
	}

	for _, updateFieldName := range updateFieldNames {
		switch updateFieldName {
		case "name":
			if len(entry.GetName()) == 0 {
				return NewCacaoInvalidParameterError("invalid TCFRegistryEntry Name")
			}
		case "type":
			if len(entry.GetType()) == 0 {
				return NewCacaoInvalidParameterError("invalid TCFRegistryEntry Type")
			}
		case "query_target":
			if len(entry.GetQueryTarget()) == 0 {
				return NewCacaoInvalidParameterError("invalid TCFRegistryEntry QueryTarget")
			}
		default:
			//pass
		}
	}

	SenderNatsConfig := client.natsConfig
	SenderNatsConfig.ClientID = SenderNatsConfig.ClientID + "-" + string(common.NewID("Sender"))

	stanConn, err := messaging.ConnectStanForServiceClient(&SenderNatsConfig, &client.stanConfig)
	if err != nil {
		errorMessage := "unable to connect to NATS Streaming"
		logger.WithError(err).Error(errorMessage)
		return NewCacaoCommunicationError(errorMessage)
	}

	defer stanConn.Disconnect()

	eventSourceNatsConfig := client.natsConfig
	eventSourceNatsConfig.ClientID = eventSourceNatsConfig.ClientID + "-" + string(common.NewID("EventSource"))

	eventSource := messaging.NewEventSource(eventSourceNatsConfig, client.stanConfig)
	if err != nil {
		errorMessage := "unable to create EventSource"
		logger.WithError(err).Error(errorMessage)
		return NewCacaoCommunicationError(errorMessage)
	}

	request := TCFRegistryEntryModel{
		Session: Session{
			SessionActor:    client.actor,
			SessionEmulator: client.emulator,
		},
		Name:                      entry.GetName(),
		Description:               entry.GetDescription(),
		Type:                      entry.GetType(),
		QueryTarget:               entry.GetQueryTarget(),
		QueryData:                 entry.GetQueryData(),
		QueryResultJSONPathFilter: entry.GetQueryResultJSONPathFilter(),
		UpdateFieldNames:          updateFieldNames,
	}

	eventsSubscribe := []common.EventType{
		TCFRegistryEntryUpdatedEvent,
		TCFRegistryEntryUpdateFailedEvent,
	}

	transactionID := messaging.NewTransactionID()

	responseChannel := make(chan TCFRegistryEntryModel)

	callback := func(ev common.EventType, ce cloudevents.Event) {
		if ev == TCFRegistryEntryUpdatedEvent || ev == TCFRegistryEntryUpdateFailedEvent {
			var tcfRegistryEntryModel TCFRegistryEntryModel
			err := json.Unmarshal(ce.Data(), &tcfRegistryEntryModel)
			if err != nil {
				errorMessage := "unable to unmarshal a tcf registry entry update response"
				logger.WithError(err).Error(errorMessage)
				responseChannel <- TCFRegistryEntryModel{
					Session: Session{
						SessionActor:    client.actor,
						SessionEmulator: client.emulator,
						ServiceError:    NewCacaoMarshalError(errorMessage).GetBase(),
					},
				}
				return
			}

			responseChannel <- tcfRegistryEntryModel
			return
		}

		errorMessage := fmt.Sprintf("unknown event type %s", ev)
		responseChannel <- TCFRegistryEntryModel{
			Session: Session{
				SessionActor:    client.actor,
				SessionEmulator: client.emulator,
				ServiceError:    NewCacaoCommunicationError(errorMessage).GetBase(),
			},
		}
	}

	listener := messaging.Listener{
		Callback:   callback,
		ListenOnce: true,
	}

	listenerID, err := eventSource.AddListenerMultiEventType(eventsSubscribe, transactionID, listener)
	if err != nil {
		errorMessage := "unable to add an event listener"
		logger.WithError(err).Error(errorMessage)
		return NewCacaoCommunicationError(errorMessage)
	}
	defer eventSource.RemoveListenerByID(listenerID)

	err = stanConn.PublishWithTransactionID(TCFRegistryEntryUpdateRequestedEvent, request, transactionID)
	if err != nil {
		errorMessage := "unable to publish a update tcf registry entry event"
		logger.WithError(err).Error(errorMessage)
		return NewCacaoCommunicationError(errorMessage)
	}

	// wait for response
	responseReceived, response := waitTCFRegistryResponse(responseChannel, time.Duration(client.natsConfig.RequestTimeout)*time.Second)
	if responseReceived {
		serviceError := response.GetServiceError()
		if serviceError == nil {
			return nil
		}

		logger.Error(serviceError)
		return serviceError
	}

	// timed out
	logger.Error("unable to update a tcf registry entry - tcf registry service is not responding")
	return NewCacaoTimeoutError("unable to update a tcf registry entry - tcf registry service is not responding")
}

// Delete deletes the tcf registry entry with the name
func (client *natsTCFRegistryClient) Delete(name string) error {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsTCFRegistryClient.Delete",
	})

	if len(name) == 0 {
		return NewCacaoInvalidParameterError("invalid TCFRegistryEntry Name")
	}

	SenderNatsConfig := client.natsConfig
	SenderNatsConfig.ClientID = SenderNatsConfig.ClientID + "-" + string(common.NewID("Sender"))

	stanConn, err := messaging.ConnectStanForServiceClient(&SenderNatsConfig, &client.stanConfig)
	if err != nil {
		errorMessage := "unable to connect to NATS Streaming"
		logger.WithError(err).Error(errorMessage)
		return NewCacaoCommunicationError(errorMessage)
	}

	defer stanConn.Disconnect()

	eventSourceNatsConfig := client.natsConfig
	eventSourceNatsConfig.ClientID = eventSourceNatsConfig.ClientID + "-" + string(common.NewID("EventSource"))

	eventSource := messaging.NewEventSource(eventSourceNatsConfig, client.stanConfig)
	if err != nil {
		errorMessage := "unable to create EventSource"
		logger.WithError(err).Error(errorMessage)
		return NewCacaoCommunicationError(errorMessage)
	}

	request := TCFRegistryEntryModel{
		Session: Session{
			SessionActor:    client.actor,
			SessionEmulator: client.emulator,
		},
		Name: name,
	}

	eventsSubscribe := []common.EventType{
		TCFRegistryEntryDeletedEvent,
		TCFRegistryEntryDeleteFailedEvent,
	}

	transactionID := messaging.NewTransactionID()

	responseChannel := make(chan TCFRegistryEntryModel)

	callback := func(ev common.EventType, ce cloudevents.Event) {
		if ev == TCFRegistryEntryDeletedEvent || ev == TCFRegistryEntryDeleteFailedEvent {
			var tcfRegistryModel TCFRegistryEntryModel
			err := json.Unmarshal(ce.Data(), &tcfRegistryModel)
			if err != nil {
				errorMessage := "unable to unmarshal a tcf registry entry delete response"
				logger.WithError(err).Error(errorMessage)
				responseChannel <- TCFRegistryEntryModel{
					Session: Session{
						SessionActor:    client.actor,
						SessionEmulator: client.emulator,
						ServiceError:    NewCacaoMarshalError(errorMessage).GetBase(),
					},
				}
				return
			}

			responseChannel <- tcfRegistryModel
			return
		}

		errorMessage := fmt.Sprintf("unknown event type %s", ev)
		responseChannel <- TCFRegistryEntryModel{
			Session: Session{
				SessionActor:    client.actor,
				SessionEmulator: client.emulator,
				ServiceError:    NewCacaoCommunicationError(errorMessage).GetBase(),
			},
		}
	}

	listener := messaging.Listener{
		Callback:   callback,
		ListenOnce: true,
	}

	listenerID, err := eventSource.AddListenerMultiEventType(eventsSubscribe, transactionID, listener)
	if err != nil {
		errorMessage := "unable to add an event listener"
		logger.WithError(err).Error(errorMessage)
		return NewCacaoCommunicationError(errorMessage)
	}
	defer eventSource.RemoveListenerByID(listenerID)

	err = stanConn.PublishWithTransactionID(TCFRegistryEntryDeleteRequestedEvent, request, transactionID)
	if err != nil {
		errorMessage := "unable to publish a delete tcf registry entry event"
		logger.WithError(err).Error(errorMessage)
		return NewCacaoCommunicationError(errorMessage)
	}

	// wait for response
	responseReceived, response := waitTCFRegistryResponse(responseChannel, time.Duration(client.natsConfig.RequestTimeout)*time.Second)
	if responseReceived {
		serviceError := response.GetServiceError()
		if serviceError == nil {
			return nil
		}

		logger.Error(serviceError)
		return serviceError
	}

	// timed out
	logger.Error("unable to delete a tcf registry entry - tcf registry service is not responding")
	return NewCacaoTimeoutError("unable to delete a tcf registry entry - tcf registry service is not responding")
}
