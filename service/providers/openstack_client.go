package providers

import (
	"context"
	"encoding/json"
	"fmt"
	cloudevents "github.com/cloudevents/sdk-go/v2"
	"time"

	"gitlab.com/cyverse/cacao-common/common"

	"gitlab.com/cyverse/cacao-common/messaging"
	"gitlab.com/cyverse/cacao-common/service"
)

// OpenStackProvider is the interface for all provider services.
// All operations will require a credential, if credential ID is empty (not provided),
// then provider-openstack-service will attempt to select a credential by searching through user's credentials.
// The credential ID needs to be under the name of the session actor.
type OpenStackProvider interface {
	Init() error
	AuthenticationTest(ctx context.Context, session *service.Session, variables map[string]string) error
	RegionList(ctx context.Context, session *service.Session, credOpt CredentialOption) ([]Region, error)
	ApplicationCredentialList(ctx context.Context, session *service.Session, credOpt CredentialOption) ([]ApplicationCredential, error)
	GetApplicationCredential(ctx context.Context, session *service.Session, applicationCredentialID string, credOpt CredentialOption) (*ApplicationCredential, error)
	CreateApplicationCredential(ctx context.Context, session *service.Session, scope ProjectScope, name string, credOpt CredentialOption) (string, error)
	DeleteApplicationCredential(ctx context.Context, session *service.Session, credentialID string) error
	FlavorList(ctx context.Context, session *service.Session, credOpt CredentialOption) ([]Flavor, error)
	GetFlavor(ctx context.Context, session *service.Session, flavorID string, credOpt CredentialOption) (*Flavor, error)
	ImageList(ctx context.Context, session *service.Session, credOpt CredentialOption) ([]Image, error)
	GetImage(ctx context.Context, session *service.Session, imageID string, credOpt CredentialOption) (*Image, error)
	ProjectList(ctx context.Context, session *service.Session, credOpt CredentialOption) ([]Project, error)
	GetProject(ctx context.Context, session *service.Session, projectID string, credOpt CredentialOption) (*Project, error)
	ListCatalog(ctx context.Context, session *service.Session, credOpt CredentialOption) ([]CatalogEntry, error)
	GetToken(ctx context.Context, session *service.Session, credOpt CredentialOption) (*Token, error)
	// PopulateCache(ctx context.Context, session *service.Session, credOpt CredentialOption) error
	Finish() error
}

// CredentialOption is option to specify credential to use for the operation
type CredentialOption func() ProviderCredential

// WithCredentialID is specifying credential via credential ID
func WithCredentialID(credID string) CredentialOption {
	return func() ProviderCredential {
		return ProviderCredential{
			Type: ProviderCredentialID,
			Data: credID,
		}
	}
}

// WithOpenStackToken is specifying credential via openstack token
func WithOpenStackToken(token string) CredentialOption {
	return func() ProviderCredential {
		return ProviderCredential{
			Type: ProviderOpenStackTokenCredential,
			Data: OpenStackTokenCredential{
				Token: token,
				Scope: ProjectScope{},
			},
		}
	}
}

// WithOpenStackTokenProjectScoped is specifying credential via openstack token, and also soped to a specific project.
func WithOpenStackTokenProjectScoped(token string, scope ProjectScope) CredentialOption {
	return func() ProviderCredential {
		return ProviderCredential{
			Type: ProviderOpenStackTokenCredential,
			Data: OpenStackTokenCredential{
				Token: token,
				Scope: scope,
			},
		}
	}
}

// natsOpenStack is an implementation of Provider that sends requests
// over NATS and waits for responses.
type natsOpenStack struct {
	provider common.ID
	// conn           *nats.Conn
	// credGetterType CredGetterType
	nats *messaging.NatsConfig
	stan *messaging.StanConfig
}

// NewOpenStackProvider returns a new instance of Provider. You will still need to call
// Init() on it.
func NewOpenStackProvider(
	provider common.ID,
	nats *messaging.NatsConfig,
	stan *messaging.StanConfig,
) OpenStackProvider {
	if stan.EventsTimeout <= 0 {
		stan.EventsTimeout = int(messaging.DefaultStanEventsTimeout)
	}
	return &natsOpenStack{
		provider: provider,
		nats:     nats,
		stan:     stan,
	}
}

// Init ...
func (p *natsOpenStack) Init() error {
	return nil
}

// Finish ...
func (p *natsOpenStack) Finish() error {
	return nil
}

// ApplicationCredentialList gets the listing of available credentials and returns it.
func (p *natsOpenStack) ApplicationCredentialList(ctx context.Context, session *service.Session, credOpt CredentialOption) ([]ApplicationCredential, error) {
	var reply ApplicationCredentialListReply
	if err := p.doOperation(
		ctx,
		*session,
		ApplicationCredentialsListOp,
		credOpt(),
		nil,
		&reply,
	); err != nil {
		return nil, err
	}
	if reply.Session.GetServiceError() != nil {
		return nil, reply.Session.GetServiceError()
	}
	return reply.ApplicationCredentials, nil
}

// GetApplicationCredential returns a specific application credential by its UUID.
func (p *natsOpenStack) GetApplicationCredential(ctx context.Context, session *service.Session, id string, credOpt CredentialOption) (*ApplicationCredential, error) {
	var reply GetApplicationCredentialReply
	if err := p.doOperation(
		ctx,
		*session,
		ApplicationCredentialsGetOp,
		credOpt(),
		id,
		&reply,
	); err != nil {
		return nil, err
	}
	if reply.Session.GetServiceError() != nil {
		return nil, reply.Session.GetServiceError()
	}
	return reply.ApplicationCredential, nil
}

// CreateApplicationCredential creates an application credential, and save it to credential microservice. This returns the credential ID on success.
func (p *natsOpenStack) CreateApplicationCredential(ctx context.Context, session *service.Session, scope ProjectScope, name string, credOpt CredentialOption) (credID string, err error) {

	if err = scope.CheckScoped(); err != nil {
		return "", err
	}
	var responseCe cloudevents.Event
	err = p.doStanOperation(
		ctx,
		*session,
		EventApplicationCredentialsCreationRequested,
		credOpt(),
		ApplicationCredentialCreationArgs{
			Scope:       scope,
			NamePostfix: name,
		},
		common.NewEventTypeSet(EventApplicationCredentialsCreateFailed, EventApplicationCredentialsCreated),
		&responseCe)
	if err != nil {
		return "", err
	}
	return p.handleAppCredCreationResponse(responseCe)
}

func (p *natsOpenStack) handleAppCredCreationResponse(responseCe cloudevents.Event) (credID string, err error) {

	switch common.EventType(responseCe.Type()) {
	case EventApplicationCredentialsCreateFailed:
		var response CreateApplicationCredentialResponse
		err = json.Unmarshal(responseCe.Data(), &response)
		if err != nil {
			return "", service.NewCacaoMarshalError(err.Error())
		}
		return "", fmt.Errorf("fail to create application credential, %w", response.BaseProviderReply.Session.GetServiceError())
	case EventApplicationCredentialsCreated:
		var response CreateApplicationCredentialResponse
		err = json.Unmarshal(responseCe.Data(), &response)
		if err != nil {
			return "", err
		}
		return response.CredentialID, nil
	default:
		return "", fmt.Errorf("unknown response event type, %s", responseCe.Type())
	}
}

// DeleteApplicationCredential deletes an application credential. This uses an application credential (stored in credential microservice) to delete itself.
// The application credential needs to be unrestricted.
// If the credential is not an application credential then the deletion will fail.
func (p *natsOpenStack) DeleteApplicationCredential(ctx context.Context, session *service.Session, credentialID string) error {
	if credentialID == "" {
		return service.NewCacaoInvalidParameterError("empty credential ID")
	}

	var responseCe cloudevents.Event
	err := p.doStanOperation(
		ctx,
		*session,
		EventApplicationCredentialsDeletionRequested,
		WithCredentialID(credentialID)(),
		nil, // no args for this operation
		common.NewEventTypeSet(EventApplicationCredentialsDeleteFailed, EventApplicationCredentialsDeleted),
		&responseCe)
	if err != nil {
		return err
	}
	return p.handleAppCredDeletionResponse(responseCe)
}

func (p *natsOpenStack) handleAppCredDeletionResponse(responseCe cloudevents.Event) error {
	switch common.EventType(responseCe.Type()) {
	case EventApplicationCredentialsDeleteFailed:
		var response DeleteApplicationCredentialResponse
		err := json.Unmarshal(responseCe.Data(), &response)
		if err != nil {
			return service.NewCacaoMarshalError(err.Error())
		}
		return fmt.Errorf("fail to delete application credential, %w", response.Session.GetServiceError())
	case EventApplicationCredentialsDeleted:
		var response DeleteApplicationCredentialResponse
		err := json.Unmarshal(responseCe.Data(), &response)
		if err != nil {
			return err
		}
		return nil
	default:
		return fmt.Errorf("unknown response event type, %s", responseCe.Type())
	}
}

// AuthenticationTest ...
func (p *natsOpenStack) AuthenticationTest(ctx context.Context, session *service.Session, variables map[string]string) error {
	var reply AuthenticationTestReply
	if err := p.doOperation(
		ctx,
		*session,
		AuthenticationTestOp,
		ProviderCredential{
			Type: ProviderCredentialID,
			Data: "",
		},
		variables,
		&reply,
	); err != nil {
		return err
	}
	if reply.Session.GetServiceError() != nil {
		return reply.Session.GetServiceError()
	}
	return nil
}

// ImageList gets the listing of available images and returns it.
// This operation will use the specified credential ID if provided (non-empty).
func (p *natsOpenStack) ImageList(ctx context.Context, session *service.Session, credOpt CredentialOption) ([]Image, error) {
	var reply ImageListReply
	if err := p.doOperation(
		ctx,
		*session,
		ImagesListOp,
		credOpt(),
		ImageListingArgs{}, // TODO make use of args after svc supports it
		&reply,
	); err != nil {
		return nil, err
	}
	if reply.Session.GetServiceError() != nil {
		return nil, reply.Session.GetServiceError()
	}
	return reply.Images, nil
}

// GetImage returns a specific image by its UUID.
// This operation will use the specified credential ID if provided (non-empty).
func (p *natsOpenStack) GetImage(ctx context.Context, session *service.Session, imageID string, credOpt CredentialOption) (*Image, error) {
	var reply GetImageReply
	if err := p.doOperation(
		ctx,
		*session,
		ImagesGetOp,
		credOpt(),
		imageID, // use image ID as args
		&reply,
	); err != nil {
		return nil, err
	}
	if reply.Session.GetServiceError() != nil {
		return nil, reply.Session.GetServiceError()
	}
	return reply.Image, nil
}

// FlavorList returns a list of the available flavors.
// This operation will use the specified credential ID if provided (non-empty).
func (p *natsOpenStack) FlavorList(ctx context.Context, session *service.Session, credOpt CredentialOption) ([]Flavor, error) {
	var reply FlavorListReply

	if err := p.doOperation(
		ctx,
		*session,
		FlavorsListOp,
		credOpt(),
		FlavorListingArgs{}, // TODO make use of args after svc supports it
		&reply,
	); err != nil {
		return nil, err
	}
	if reply.Session.GetServiceError() != nil {
		return nil, reply.Session.GetServiceError()
	}
	return reply.Flavors, nil
}

// GetFlavor returns a specific image by its UUID.
// This operation will use the specified credential ID if provided (non-empty).
func (p *natsOpenStack) GetFlavor(ctx context.Context, session *service.Session, flavorID string, credOpt CredentialOption) (*Flavor, error) {
	var reply GetFlavorReply
	if err := p.doOperation(
		ctx,
		*session,
		FlavorsGetOp,
		credOpt(),
		flavorID, // use flavorID as args
		&reply,
	); err != nil {
		return nil, err
	}
	if reply.Session.GetServiceError() != nil {
		return nil, reply.Session.GetServiceError()
	}
	return reply.Flavor, nil
}

// ProjectList returns a list of the available projects.
func (p *natsOpenStack) ProjectList(ctx context.Context, session *service.Session, credOpt CredentialOption) ([]Project, error) {
	var reply ProjectListReply

	if err := p.doOperation(
		ctx,
		*session,
		ProjectsListOp,
		credOpt(),
		nil,
		&reply,
	); err != nil {
		return nil, err
	}
	if reply.Session.GetServiceError() != nil {
		return nil, reply.Session.GetServiceError()
	}
	return reply.Projects, nil
}

// GetProject returns a specific project by its UUID.
func (p *natsOpenStack) GetProject(ctx context.Context, session *service.Session, id string, credOpt CredentialOption) (*Project, error) {
	var reply GetProjectReply
	if err := p.doOperation(
		ctx,
		*session,
		ProjectsGetOp,
		credOpt(),
		id,
		&reply,
	); err != nil {
		return nil, err
	}
	if reply.Session.GetServiceError() != nil {
		return nil, reply.Session.GetServiceError()
	}
	return reply.Project, nil
}

// RegionList returns a list of the available regions.
func (p *natsOpenStack) RegionList(ctx context.Context, session *service.Session, credOpt CredentialOption) ([]Region, error) {
	var reply RegionListReply

	if err := p.doOperation(
		ctx,
		*session,
		RegionsListOp,
		credOpt(),
		nil,
		&reply,
	); err != nil {
		return nil, err
	}
	if reply.Session.GetServiceError() != nil {
		return nil, reply.Session.GetServiceError()
	}
	return reply.Regions, nil
}

// ListCatalog returns a list of catalog entries.
func (p *natsOpenStack) ListCatalog(ctx context.Context, session *service.Session, credOpt CredentialOption) ([]CatalogEntry, error) {
	var reply CatalogListReply

	if err := p.doOperation(
		ctx,
		*session,
		CatalogListOp,
		credOpt(),
		nil,
		&reply,
	); err != nil {
		return nil, err
	}
	if reply.Session.GetServiceError() != nil {
		return nil, reply.Session.GetServiceError()
	}
	return reply.Catalog, nil
}

// GetToken issues a token with scope the same as the scope of the credential used.
func (p *natsOpenStack) GetToken(ctx context.Context, session *service.Session, credOpt CredentialOption) (*Token, error) {
	var reply GetTokenReply

	if err := p.doOperation(
		ctx,
		*session,
		TokenGetOp,
		credOpt(),
		nil,
		&reply,
	); err != nil {
		return nil, err
	}
	if reply.Session.GetServiceError() != nil {
		return nil, reply.Session.GetServiceError()
	}
	return &reply.Token, nil
}

// PopulateCache tells the OpenStack provider to fetch the images, flavors, projects, and application credentials for a user
// func (p *natsOpenStack) PopulateCache(ctx context.Context, session *service.Session, credOpt CredentialOption) error {
// 	var reply CachePopulateReply
// 	if err := p.doOperation(
// 		ctx,
// 		*session,
// 		CachePopulateOp,
// 		credOpt(),
// 		nil,
// 		&reply,
// 	); err != nil {
// 		return err
// 	}
// 	if reply.Session.GetServiceError() != nil {
// 		return reply.Session.GetServiceError()
// 	}
// 	return nil
// }

func (p *natsOpenStack) doOperation(
	ctx context.Context,
	session service.Session,
	op common.QueryOp,
	credential ProviderCredential,
	args interface{},
	output interface{},
) error {
	conn, err := messaging.ConnectNatsForServiceClient(p.nats)
	if err != nil {
		return service.NewCacaoCommunicationError(err.Error())
	}
	defer conn.Disconnect()
	var subject = p.subject(op)
	reply, err := conn.RequestWithTransactionID(ctx, subject, ProviderRequest{
		Session:    session,
		Operation:  op,
		Provider:   p.provider,
		Credential: credential,
		Args:       args,
	}, messaging.NewTransactionID())
	if err != nil {
		return service.NewCacaoCommunicationError(err.Error())
	}
	err = json.Unmarshal(reply, output)
	if err != nil {
		return service.NewCacaoMarshalError(err.Error())
	}
	return nil
}

func (p *natsOpenStack) subject(op common.QueryOp) common.QueryOp {
	return OpenStackQueryPrefix + op
}

// NATS Streaming (Stan) operation
func (p *natsOpenStack) doStanOperation(
	ctx context.Context,
	session service.Session,
	eventType common.EventType,
	credential ProviderCredential,
	args interface{},
	responseEventTypeSet common.EventTypeSet,
	output *cloudevents.Event,
) error {
	conn, err := messaging.ConnectStanForServiceClient(p.nats, p.stan)
	if err != nil {
		return service.NewCacaoCommunicationError(err.Error())
	}
	defer conn.Disconnect()

	tid := messaging.NewTransactionID()
	respCeChan := make(chan cloudevents.Event)

	eventSrc, err := messaging.NewEventSourceFromStanConnection(conn)
	if err != nil {
		return err
	}

	listenID, err := p.listenForStanResponse(eventSrc, tid, responseEventTypeSet, respCeChan)
	if err != nil {
		return err
	}
	defer eventSrc.RemoveListenerByID(listenID)

	err = conn.PublishWithTransactionID(eventType, ProviderRequest{
		Session:    session,
		Operation:  "", // operation is specified by EventType
		Provider:   p.provider,
		Credential: credential,
		Args:       args,
	}, tid)
	if err != nil {
		return service.NewCacaoCommunicationError(err.Error())
	}
	select {
	case ce := <-respCeChan:
		*output = ce
	case <-time.After(time.Duration(p.stan.EventsTimeout)):
		return service.NewCacaoTimeoutError("application credential creation timed out")
	}
	return nil
}

// listen for STAN response, only receive events that matched the transaction ID and whose event type is in the set.
func (p *natsOpenStack) listenForStanResponse(eventSrc *messaging.EventSource, tid common.TransactionID, responseEventTypeSet common.EventTypeSet, respCeChan chan<- cloudevents.Event) (messaging.ListenerID, error) {
	var err error

	callback := func(ev common.EventType, ce cloudevents.Event) {
		if messaging.GetTransactionID(&ce) != tid {
			// filter based on transaction ID
			return
		}
		select {
		case respCeChan <- ce:
		default:
			// channel full, the response is already received
			return
		}
		return
	}

	listener := messaging.Listener{
		Callback:   callback,
		ListenOnce: true,
	}

	var respEventTypes []common.EventType
	for eventType := range responseEventTypeSet {
		respEventTypes = append(respEventTypes, eventType)
	}

	listenerID, err := eventSrc.AddListenerMultiEventType(respEventTypes, tid, listener)
	if err != nil {
		errorMessage := fmt.Sprintf("unable to add an event listener, %s", err.Error())
		return "", service.NewCacaoCommunicationError(errorMessage)
	}

	return listenerID, nil
}
