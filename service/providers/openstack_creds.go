package providers

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/mitchellh/mapstructure"
)

// ProviderCredential specified the credential for the operation. There can be multiple ways of specifying credential, which is distinguished by the Type field.
type ProviderCredential struct {
	Type string      `json:"type"`
	Data interface{} `json:"data"` // TODO consider improve this with generics
}

const (
	ProviderCredentialID             = "credentialID"
	ProviderOpenStackTokenCredential = "openstackToken"
)

// OpenStackTokenCredential is an openstack token (scoped/unscoped), The project and domain fields are optional, and they are here to explicitly narrow down the scope.
type OpenStackTokenCredential struct {
	Token string       `json:"token"`
	Scope ProjectScope `json:"scope"`
}

// UnmarshalJSON custom Unmarshal based on Type field
func (c *ProviderCredential) UnmarshalJSON(b []byte) error {
	var tmp struct {
		Type string      `json:"type"`
		Data interface{} `json:"data"`
	}
	if err := json.Unmarshal(b, &tmp); err != nil {
		return err
	}

	var ok bool
	switch tmp.Type {
	default:
		return errors.New("unknown ProviderCredential type")
	case ProviderCredentialID:
		c.Data, ok = tmp.Data.(string)
		if !ok {
			return errors.New("credentialID is not string")
		}
	case ProviderOpenStackTokenCredential:
		var tokenCredMap map[string]interface{}
		tokenCredMap, ok := tmp.Data.(map[string]interface{})
		if !ok {
			return errors.New("openstackToken is not json object")
		}
		var tokenCred OpenStackTokenCredential
		err := mapstructure.Decode(tokenCredMap, &tokenCred)
		if err != nil {
			return fmt.Errorf("openstackToken cannot be unmarshal into OpenStackTokenCredential, %w", err)
		}
		if len(tokenCred.Token) == 0 {
			return errors.New("openstackToken cannot be empty")
		}
		c.Data = tokenCred
	}
	c.Type = tmp.Type
	return nil
}

// MarshalJSON custom marshal  based on Type field
func (c *ProviderCredential) MarshalJSON() ([]byte, error) {
	switch c.Type {
	default:
		return nil, errors.New("unknown ProviderCredential type")
	case ProviderCredentialID:
		_, ok := c.Data.(string)
		if !ok {
			return nil, errors.New("data for credentialID must be string")
		}
	case ProviderOpenStackTokenCredential:
		tokenCred, ok := c.Data.(OpenStackTokenCredential)
		if !ok {
			return nil, errors.New("data for openstackToken must be OpenStackTokenCredential")
		}
		if len(tokenCred.Token) == 0 {
			return nil, errors.New("token for openstackToken cannot be empty")
		}
	}
	var tmp = struct {
		Type string      `json:"type"`
		Data interface{} `json:"data"`
	}{
		Type: c.Type,
		Data: c.Data,
	}
	return json.Marshal(tmp) // convert to interface{} to invoke the default Marshal()
}
