package providers

import (
	"fmt"
	cloudevents "github.com/cloudevents/sdk-go/v2"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging"
	"gitlab.com/cyverse/cacao-common/service"
)

// Client is the interface to the provider service.
type Client interface {
	Image(string) error
	ListImages(*ImageListingArgs) ([]Image, error)
	Flavor(string) error
	ListFlavors(*FlavorListingArgs) ([]Flavor, error)
}

// OpenStackQueryPrefix is prefix for query operation for openstack provider
const OpenStackQueryPrefix common.QueryOp = common.NatsQueryOpPrefix + "providers.openstack."

// ApplicationCredentialsListOp is the operation for listing credentials.
const ApplicationCredentialsListOp common.QueryOp = "appcredentials.list"

// ApplicationCredentialsGetOp is the operation for getting a single credential.
const ApplicationCredentialsGetOp common.QueryOp = "appcredentials.get"

// AuthenticationTestOp is the operation for testing authentication.
const AuthenticationTestOp common.QueryOp = "authentication.test"

// ImagesListOp is the operation for listing images.
const ImagesListOp common.QueryOp = "images.list"

// ImagesGetOp is the operation for getting a single image.
const ImagesGetOp common.QueryOp = "images.get"

// FlavorsListOp is the operation for listing flavors.
const FlavorsListOp common.QueryOp = "flavors.list"

// FlavorsGetOp is the operation for getting a single flavor.
const FlavorsGetOp common.QueryOp = "flavors.get"

// ProjectsListOp is the operation for listing projects.
const ProjectsListOp common.QueryOp = "projects.list"

// ProjectsGetOp is the operation for getting a single project.
const ProjectsGetOp common.QueryOp = "projects.get"

// RegionsListOp is the operation for listing regions.
const RegionsListOp common.QueryOp = "regions.list"

// CatalogListOp is the operation for listing catalog.
const CatalogListOp common.QueryOp = "catalog.list"

// TokenGetOp is the operation for getting a single token.
const TokenGetOp common.QueryOp = "token.get"

// CachePopulateOp is the operation for forcing a cache population for a user.
// const CachePopulateOp common.QueryOp = "cache.populate"

// EventApplicationCredentialsCreationRequested request the creation of an application credential
const EventApplicationCredentialsCreationRequested common.EventType = common.EventTypePrefix + "OpenStackApplicationCredentialsCreationRequested"

// EventApplicationCredentialsCreated is success response to EventApplicationCredentialsCreationRequested
const EventApplicationCredentialsCreated common.EventType = common.EventTypePrefix + "OpenStackApplicationCredentialsCreated"

// EventApplicationCredentialsCreateFailed is failure response to EventApplicationCredentialsCreationRequested
const EventApplicationCredentialsCreateFailed common.EventType = common.EventTypePrefix + "OpenStackApplicationCredentialsCreateFailed"

// EventApplicationCredentialsDeletionRequested request the deletion of an application credential
const EventApplicationCredentialsDeletionRequested common.EventType = common.EventTypePrefix + "OpenStackApplicationCredentialsDeletionRequested"

// EventApplicationCredentialsDeleted is success response to EventApplicationCredentialsDeletionRequested
const EventApplicationCredentialsDeleted common.EventType = common.EventTypePrefix + "OpenStackApplicationCredentialsDeleted"

// EventApplicationCredentialsDeleteFailed is failure response to EventApplicationCredentialsDeletionRequested
const EventApplicationCredentialsDeleteFailed common.EventType = common.EventTypePrefix + "OpenStackApplicationCredentialsDeleteFailed"

// Credentials represents auth information about the user.
type Credentials struct {
	ID           string      `json:"id" mapstructure:"id"`
	OpenStackEnv Environment `json:"openstack_env,omitempty" mapstructure:"openstack_env"`
}

// ProviderRequest ...
type ProviderRequest struct {
	service.Session `json:",inline"`
	Operation       common.QueryOp     `json:"op"`
	Provider        common.ID          `json:"provider"`
	Credential      ProviderCredential `json:"credential"`
	// Args depends on the type of query operation
	Args interface{} `json:"args"`
}

// BaseProviderReply ...
type BaseProviderReply struct {
	Session   service.Session `json:",inline"`
	Operation common.QueryOp  `json:"op"`
	Provider  common.ID       `json:"provider"`
}

// GetApplicationCredentialReply ...
type GetApplicationCredentialReply struct {
	BaseProviderReply     `json:",inline"`
	ApplicationCredential *ApplicationCredential `json:"application_credential"`
}

// ToCloudEvent ...
func (r GetApplicationCredentialReply) ToCloudEvent(source string) (cloudevents.Event, error) {
	return messaging.CreateCloudEvent(r, string(OpenStackQueryPrefix+ApplicationCredentialsGetOp), source)
}

// ApplicationCredentialListReply ...
type ApplicationCredentialListReply struct {
	BaseProviderReply      `json:",inline"`
	ApplicationCredentials []ApplicationCredential `json:"application_credentials"`
}

// ToCloudEvent ...
func (r ApplicationCredentialListReply) ToCloudEvent(source string) (cloudevents.Event, error) {
	return messaging.CreateCloudEvent(r, string(OpenStackQueryPrefix+ApplicationCredentialsListOp), source)
}

// AuthenticationTestReply ...
type AuthenticationTestReply struct {
	BaseProviderReply `json:",inline"`
	Result            string `json:"result"`
}

// ToCloudEvent ...
func (r AuthenticationTestReply) ToCloudEvent(source string) (cloudevents.Event, error) {
	return messaging.CreateCloudEvent(r, string(OpenStackQueryPrefix+AuthenticationTestOp), source)
}

// GetImageReply ...
type GetImageReply struct {
	BaseProviderReply `json:",inline"`
	Image             *Image `json:"image"`
}

// ToCloudEvent ...
func (r GetImageReply) ToCloudEvent(source string) (cloudevents.Event, error) {
	return messaging.CreateCloudEvent(r, string(OpenStackQueryPrefix+ImagesGetOp), source)
}

// ImageListReply ...
type ImageListReply struct {
	BaseProviderReply `json:",inline"`
	Images            []Image `json:"images"`
}

// ToCloudEvent ...
func (r ImageListReply) ToCloudEvent(source string) (cloudevents.Event, error) {
	return messaging.CreateCloudEvent(r, string(OpenStackQueryPrefix+ImagesListOp), source)
}

// GetFlavorReply ...
type GetFlavorReply struct {
	BaseProviderReply `json:",inline"`
	Flavor            *Flavor `json:"flavor"`
}

// ToCloudEvent ...
func (r GetFlavorReply) ToCloudEvent(source string) (cloudevents.Event, error) {
	return messaging.CreateCloudEvent(r, string(OpenStackQueryPrefix+FlavorsGetOp), source)
}

// FlavorListReply ...
type FlavorListReply struct {
	BaseProviderReply `json:",inline"`
	Flavors           []Flavor `json:"flavors"`
}

// ToCloudEvent ...
func (r FlavorListReply) ToCloudEvent(source string) (cloudevents.Event, error) {
	return messaging.CreateCloudEvent(r, string(OpenStackQueryPrefix+FlavorsListOp), source)
}

// GetProjectReply ...
type GetProjectReply struct {
	BaseProviderReply `json:",inline"`
	Project           *Project `json:"project"`
}

// ToCloudEvent ...
func (r GetProjectReply) ToCloudEvent(source string) (cloudevents.Event, error) {
	return messaging.CreateCloudEvent(r, string(OpenStackQueryPrefix+ProjectsGetOp), source)
}

// ProjectListReply ...
type ProjectListReply struct {
	BaseProviderReply `json:",inline"`
	Projects          []Project `json:"projects"`
}

// ToCloudEvent ...
func (r ProjectListReply) ToCloudEvent(source string) (cloudevents.Event, error) {
	return messaging.CreateCloudEvent(r, string(OpenStackQueryPrefix+ProjectsListOp), source)
}

// RegionListReply ...
type RegionListReply struct {
	BaseProviderReply `json:",inline"`
	Regions           []Region `json:"regions"`
}

// ToCloudEvent ...
func (r RegionListReply) ToCloudEvent(source string) (cloudevents.Event, error) {
	return messaging.CreateCloudEvent(r, string(OpenStackQueryPrefix+RegionsListOp), source)
}

// CatalogListReply ...
type CatalogListReply struct {
	BaseProviderReply `json:",inline"`
	Catalog           []CatalogEntry `json:"catalog"`
}

// ToCloudEvent ...
func (r CatalogListReply) ToCloudEvent(source string) (cloudevents.Event, error) {
	return messaging.CreateCloudEvent(r, string(OpenStackQueryPrefix+CatalogListOp), source)
}

// GetTokenReply ...
type GetTokenReply struct {
	BaseProviderReply `json:",inline"`
	Token             Token `json:"token"`
}

// ToCloudEvent ...
func (r GetTokenReply) ToCloudEvent(source string) (cloudevents.Event, error) {
	return messaging.CreateCloudEvent(r, string(OpenStackQueryPrefix+TokenGetOp), source)
}

// CreateApplicationCredentialResponse is struct for response to application credential creation request. Event types: EventApplicationCredentialsCreated or EventApplicationCredentialsCreateFailed.
type CreateApplicationCredentialResponse struct {
	BaseProviderReply `json:",inline"`
	CredentialID      string `json:"credential_id"`
}

// DeleteApplicationCredentialResponse is struct for response to application credential deletion request,
type DeleteApplicationCredentialResponse struct {
	BaseProviderReply `json:",inline"`
	CredentialID      string `json:",inline"`
}

// CachePopulateReply ...
// type CachePopulateReply BaseProviderReply

// ToCloudEvent ...
// func (r CachePopulateReply) ToCloudEvent(source string) (cloudevents.Event, error) {
// 	return messaging.CreateCloudEvent(r, string(OpenStackQueryPrefix+CachePopulateOp), source)
// }

// Flavor represents the compute, memory and storage capacity of a computing
// instance in OpenStack (https://docs.openstack.org/nova/latest/user/flavors.html)
type Flavor struct {
	ID        string `json:"id"`
	Name      string `json:"name"`
	RAM       int64  `json:"ram"`
	Ephemeral int64  `json:"ephemeral"`
	VCPUs     int64  `json:"vcpus"`
	IsPublic  bool   `json:"is_public"`
	Disk      int64  `json:"disk"`
}

// FlavorListingArgs represents the available options that can be passed to
// `openstack flavor list`.
type FlavorListingArgs struct {
	Public  bool
	Private bool
	All     bool
	Limit   int64
}

// String implements the Stringer interface for FlavorListingArgs. Note: this
// is not the same as the command-line representation.
func (f *FlavorListingArgs) String() string {
	return fmt.Sprintf("%v %v %v %v", f.Public, f.Private, f.All, f.Limit)
}

// Environment contains a map of string keys and string values representing the environment
// in which OpenStack CLI commands should execute.  This is important for setting the
// appropriate environment variables for accessing OpenStack
type Environment map[string]string

// Image describes an image available through OpenStack. Adapted from the
// long listing format.
type Image struct {
	ID              string   `json:"id"`
	Name            string   `json:"name"`
	DiskFormat      string   `json:"disk_format"`
	ContainerFormat string   `json:"container_format"`
	Size            int64    `json:"size"`
	Checksum        string   `json:"checksum"`
	Status          string   `json:"status"`
	Visibility      string   `json:"visibility"`
	Protected       bool     `json:"protected"`
	Project         string   `json:"project"`
	Tags            []string `json:"tags"`
}

// ImageListingSortOption contains the key and direction for a sorting
// option passed to the openstack command-line.
type ImageListingSortOption struct {
	Key       string
	Direction string
}

// ImageListingArgs contains the possibly settings for listing images.
type ImageListingArgs struct {
	Public        bool
	Private       bool
	Community     bool
	Shared        bool
	Name          string
	Status        string
	MemberStatus  string
	Project       string
	ProjectDomain string
	Tag           string
	Limit         int64
	SortOpts      []ImageListingSortOption
	Properties    []string // should be in the key=value format
}

// String implements the String interface for ImageListingArgss. Note:
// this does not return the command-line arguments for the corresponding
// openstack command.
func (i *ImageListingArgs) String() string {
	return fmt.Sprintf("%v %v %v %v %v %v %v %v %v %v %v %v %v",
		i.Public,
		i.Private,
		i.Community,
		i.Shared,
		i.Name,
		i.Status,
		i.MemberStatus,
		i.Project,
		i.ProjectDomain,
		i.Tag,
		i.Limit,
		i.SortOpts,
		i.Properties,
	)
}

// ApplicationCredentialCreationArgs is the argument for ProviderRequest for creating application credential. It contains the possible settings for creating application credential.
type ApplicationCredentialCreationArgs struct {
	Scope       ProjectScope `json:"scope" mapstructure:"scope"`
	NamePostfix string       `json:"name" mapstructure:"name"` // this will be appended to the end of the generated names (the name of application credential, and ID of CACAO credential)
}

// ApplicationCredential is application credential for OpenStack
type ApplicationCredential struct {
	ID           string        `json:"id"`
	Name         string        `json:"name"`
	Description  string        `json:"description"`
	ExpiresAt    interface{}   `json:"expires_at"`
	ProjectID    string        `json:"project_id"`
	Roles        []interface{} `json:"roles"`
	Unrestricted bool          `json:"unrestricted"`
}

// Project is OpenStack Project
type Project struct {
	ID          string                 `json:"id"`
	Name        string                 `json:"name"`
	Description string                 `json:"description"`
	DomainID    string                 `json:"domain_id"`
	Enabled     bool                   `json:"enabled"`
	IsDomain    bool                   `json:"is_domain"`
	ParentID    string                 `json:"parent_id"`
	Properties  map[string]interface{} `json:"properties"`
}

// Region is OpenStack Region. ID and Name generally have the same value.
type Region struct {
	ID   string `json:"id"`
	Name string `json:"name"`
}

// Token issued by OpenStack.
// Note this is used to represent tokens created by the service, rather than token used to authenticate with OpenStack.
type Token struct {
	Expires   string `json:"expires"`
	ID        string `json:"id"`
	ProjectID string `json:"project_id"`
	UserID    string `json:"user_id"`
}

// CatalogEntry is an entry in the catalog, it contains endpoint info for a service.
type CatalogEntry struct {
	Name      string            `json:"name"`
	Type      string            `json:"type"`
	Endpoints []CatalogEndpoint `json:"endpoints"`
}

// CatalogEndpoint is an API endpoint in the catalog
type CatalogEndpoint struct {
	ID        string `json:"id"`
	Interface string `json:"interface"`
	RegionID  string `json:"region_id"`
	URL       string `json:"url"`
	Region    string `json:"region"`
}

// OpenStackProviderError is what gets returned when an error occurs during the
// processing of a request.
type OpenStackProviderError struct {
	Error         string `json:"error"`
	TransactionID string `json:"transaction_id"`
}

// ProjectScope is scope for a specific project in a specific domain.
type ProjectScope struct {
	Project struct {
		Name string `json:"name,omitempty"`
		ID   string `json:"id,omitempty"`
	} `json:"project"`
	Domain struct {
		Name string `json:"name,omitempty"`
		ID   string `json:"id,omitempty"`
	} `json:"domain"`
}

// Scoped returns true if the scope is project scope.
func (s ProjectScope) Scoped() bool {
	if s.Project.Name == "" && s.Project.ID == "" {
		return false
	}
	if s.Domain.Name == "" && s.Domain.ID == "" {
		return false
	}
	return true
}

// Unscoped return true if the scope is unscoped.
func (s ProjectScope) Unscoped() bool {
	if s.Project.Name == "" && s.Project.ID == "" && s.Domain.Name == "" && s.Domain.ID == "" {
		return true
	}
	return false
}

// CheckScoped checks if the scope has necessary field populated.
func (s ProjectScope) CheckScoped() error {
	if s.Project.Name == "" && s.Project.ID == "" {
		return service.NewCacaoInvalidParameterError("missing project name or id")
	}
	if s.Domain.Name == "" && s.Domain.ID == "" {
		return service.NewCacaoInvalidParameterError("missing domain name or id")
	}
	return nil
}
