package service

import (
	"context"
	"encoding/json"
	"fmt"
	"strings"
	"time"

	cloudevents "github.com/cloudevents/sdk-go/v2"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging"
)

// Nats subjects
const (
	// NatsSubjectInteractiveSession is the prefix of the queue name for Interactivesession queries
	NatsSubjectInteractiveSession common.QueryOp = common.NatsQueryOpPrefix + "interactivesession"

	// InteractiveSessionListQueryOp is the queue name for InteractiveSessionList query
	InteractiveSessionListQueryOp common.QueryOp = NatsSubjectInteractiveSession + ".list"
	// InteractiveSessionGetQueryOp is the queue name for InteractiveSessionGet query
	InteractiveSessionGetQueryOp common.QueryOp = NatsSubjectInteractiveSession + ".get"
	// InteractiveSessionGetByInstanceIDQueryOp is the queue name for InteractiveSessionGetByInstanceID query
	InteractiveSessionGetByInstanceIDQueryOp common.QueryOp = NatsSubjectInteractiveSession + ".getByInstanceID"
	// InteractiveSessionGetByInstanceAddressQueryOp is the queue name for InteractiveSessionGetByInstanceAddress query
	InteractiveSessionGetByInstanceAddressQueryOp common.QueryOp = NatsSubjectInteractiveSession + ".getByInstanceAddress"
	// InteractiveSessionCheckPrerequisitesQueryOp is the queue name for InteractiveSessionCheckPrerequisites query
	InteractiveSessionCheckPrerequisitesQueryOp common.QueryOp = NatsSubjectInteractiveSession + ".checkPrerequisites"

	// InteractiveSessionCreateRequestedEvent is the cloudevent subject for InteractiveSessionCreate
	InteractiveSessionCreateRequestedEvent common.EventType = common.EventTypePrefix + "InteractiveSessionCreateRequested"
	// InteractiveSessionDeactivateRequestedEvent is the cloudevent subject for InteractiveSessionDeactivate
	InteractiveSessionDeactivateRequestedEvent common.EventType = common.EventTypePrefix + "InteractiveSessionDeactivateRequested"

	// InteractiveSessionCreatedEvent is the cloudevent subject for InteractiveSessionCreated
	InteractiveSessionCreatedEvent common.EventType = common.EventTypePrefix + "InteractiveSessionCreated"
	// InteractiveSessionDeactivatedEvent is the cloudevent subject for InteractiveSessionDeactivated
	InteractiveSessionDeactivatedEvent common.EventType = common.EventTypePrefix + "InteractiveSessionDeactivated"

	// InteractiveSessionCreateFailedEvent is the cloudevent subject for InteractiveSessionCreateFailed
	InteractiveSessionCreateFailedEvent common.EventType = common.EventTypePrefix + "InteractiveSessionCreateFailed"
	// InteractiveSessionDeactivateFailedEvent is the cloudevent subject for InteractiveSessionDeactivateFailed
	InteractiveSessionDeactivateFailedEvent common.EventType = common.EventTypePrefix + "InteractiveSessionDeactivateFailed"
)

// Nats Client
type natsInteractiveSessionClient struct {
	actor      string
	emulator   string
	natsConfig messaging.NatsConfig
	stanConfig messaging.StanConfig
	ctx        context.Context
}

// NewNatsInteractiveSessionClient creates a client for InteractiveSession Microservice that uses NATS for communication
func NewNatsInteractiveSessionClient(ctx context.Context, actor string, emulator string, natsConfig messaging.NatsConfig, stanConfig messaging.StanConfig) (InteractiveSessionClient, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "NewNatsInteractiveSessionClient",
	})

	logger.Trace("Creating a NatsInteractiveSessionClient")

	if len(actor) == 0 {
		return nil, NewCacaoInvalidParameterError("actor is empty")
	}

	if ctx == nil {
		ctx = context.Background()
	}

	natsConfigCopy := natsConfig
	if natsConfigCopy.RequestTimeout == -1 {
		natsConfigCopy.RequestTimeout = int(messaging.DefaultNatsRequestTimeout.Seconds())
	}

	stanConfigCopy := stanConfig
	if stanConfigCopy.EventsTimeout == -1 {
		stanConfigCopy.EventsTimeout = int(messaging.DefaultStanEventsTimeout.Seconds())
	}

	client := natsInteractiveSessionClient{
		actor:      actor,
		emulator:   emulator,
		natsConfig: natsConfigCopy,
		stanConfig: stanConfigCopy,
		ctx:        ctx,
	}

	return &client, nil
}

func waitInteractiveSessionResponse(c chan InteractiveSessionModel, timeout time.Duration) (bool, InteractiveSessionModel) {
	select {
	case res := <-c:
		return true, res
	case <-time.After(timeout):
		return false, InteractiveSessionModel{}
	}
}

// Get returns the interactive session with the given interactive session ID
func (client *natsInteractiveSessionClient) Get(interactiveSessionID common.ID) (InteractiveSession, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsInteractiveSessionClient.Get",
	})

	if !interactiveSessionID.Validate() {
		return nil, NewCacaoInvalidParameterError("invalid InteractiveSession ID")
	}

	natsConn, err := messaging.ConnectNatsForServiceClient(&client.natsConfig)
	if err != nil {
		errorMessage := "unable to connect to NATS"
		logger.WithError(err).Error(errorMessage)
		return nil, NewCacaoCommunicationError(errorMessage)
	}

	defer natsConn.Disconnect()

	request := InteractiveSessionModel{
		Session: Session{
			SessionActor:    client.actor,
			SessionEmulator: client.emulator,
		},
		ID: interactiveSessionID,
	}

	timeout := time.Duration(client.natsConfig.RequestTimeout) * time.Second
	timeoutCtx, cancel := context.WithTimeout(client.ctx, timeout)
	defer cancel()

	responseBytes, err := natsConn.Request(timeoutCtx, InteractiveSessionGetQueryOp, request)
	if err != nil {
		errorMessage := "unable to request a get interactive session event"
		logger.WithError(err).Error(errorMessage)
		return nil, NewCacaoCommunicationError(errorMessage)
	}

	var interactiveSessionModel InteractiveSessionModel
	err = json.Unmarshal(responseBytes, &interactiveSessionModel)
	if err != nil {
		errorMessage := "unable to unmarshal response JSON to interactive session object"
		logger.WithError(err).Error(errorMessage)
		return nil, NewCacaoMarshalError(errorMessage)
	}

	serviceError := interactiveSessionModel.GetServiceError()
	if serviceError != nil {
		logger.Error(serviceError)
		return nil, serviceError
	}
	return &interactiveSessionModel, nil
}

// GetByInstanceID returns the interactive session with the given instance ID
func (client *natsInteractiveSessionClient) GetByInstanceID(instanceID string) (InteractiveSession, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsInteractiveSessionClient.GetByInstanceID",
	})

	if len(instanceID) == 0 {
		return nil, NewCacaoInvalidParameterError("invalid Instance ID")
	}

	natsConn, err := messaging.ConnectNatsForServiceClient(&client.natsConfig)
	if err != nil {
		errorMessage := "unable to connect to NATS"
		logger.WithError(err).Error(errorMessage)
		return nil, NewCacaoCommunicationError(errorMessage)
	}

	defer natsConn.Disconnect()

	request := InteractiveSessionModel{
		Session: Session{
			SessionActor:    client.actor,
			SessionEmulator: client.emulator,
		},
		InstanceID: instanceID,
	}

	timeout := time.Duration(client.natsConfig.RequestTimeout) * time.Second
	timeoutCtx, cancel := context.WithTimeout(client.ctx, timeout)
	defer cancel()

	responseBytes, err := natsConn.Request(timeoutCtx, InteractiveSessionGetByInstanceIDQueryOp, request)
	if err != nil {
		errorMessage := "unable to request a getByInstanceID interactive session event"
		logger.WithError(err).Error(errorMessage)
		return nil, NewCacaoCommunicationError(errorMessage)
	}

	var interactiveSessionModel InteractiveSessionModel
	err = json.Unmarshal(responseBytes, &interactiveSessionModel)
	if err != nil {
		errorMessage := "unable to unmarshal response JSON to interactive session object"
		logger.WithError(err).Error(errorMessage)
		return nil, NewCacaoMarshalError(errorMessage)
	}

	serviceError := interactiveSessionModel.GetServiceError()
	if serviceError != nil {
		logger.Error(serviceError)
		return nil, serviceError
	}
	return &interactiveSessionModel, nil
}

// GetByInstanceAddress returns the interactive session with the given instance address
func (client *natsInteractiveSessionClient) GetByInstanceAddress(instanceAddress string) (InteractiveSession, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsInteractiveSessionClient.GetByInstanceAddress",
	})

	if len(instanceAddress) == 0 {
		return nil, NewCacaoInvalidParameterError("invalid Instance Address")
	}

	natsConn, err := messaging.ConnectNatsForServiceClient(&client.natsConfig)
	if err != nil {
		errorMessage := "unable to connect to NATS"
		logger.WithError(err).Error(errorMessage)
		return nil, NewCacaoCommunicationError(errorMessage)
	}

	defer natsConn.Disconnect()

	request := InteractiveSessionModel{
		Session: Session{
			SessionActor:    client.actor,
			SessionEmulator: client.emulator,
		},
		InstanceAddress: instanceAddress,
	}

	timeout := time.Duration(client.natsConfig.RequestTimeout) * time.Second
	timeoutCtx, cancel := context.WithTimeout(client.ctx, timeout)
	defer cancel()

	responseBytes, err := natsConn.Request(timeoutCtx, InteractiveSessionGetByInstanceAddressQueryOp, request)
	if err != nil {
		errorMessage := "unable to request a getByInstanceAddress interactive session event"
		logger.WithError(err).Error(errorMessage)
		return nil, NewCacaoCommunicationError(errorMessage)
	}

	var interactiveSessionModel InteractiveSessionModel
	err = json.Unmarshal(responseBytes, &interactiveSessionModel)
	if err != nil {
		errorMessage := "unable to unmarshal response JSON to interactive session object"
		logger.WithError(err).Error(errorMessage)
		return nil, NewCacaoMarshalError(errorMessage)
	}

	serviceError := interactiveSessionModel.GetServiceError()
	if serviceError != nil {
		logger.Error(serviceError)
		return nil, serviceError
	}
	return &interactiveSessionModel, nil
}

// List returns all active/creating interactive sessions for the user
func (client *natsInteractiveSessionClient) List() ([]InteractiveSession, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsInteractiveSessionClient.List",
	})

	natsConn, err := messaging.ConnectNatsForServiceClient(&client.natsConfig)
	if err != nil {
		errorMessage := "unable to connect to NATS"
		logger.WithError(err).Error(errorMessage)
		return nil, NewCacaoCommunicationError(errorMessage)
	}

	defer natsConn.Disconnect()

	request := Session{
		SessionActor:    client.actor,
		SessionEmulator: client.emulator,
	}

	timeout := time.Duration(client.natsConfig.RequestTimeout) * time.Second
	timeoutCtx, cancel := context.WithTimeout(client.ctx, timeout)
	defer cancel()

	responseBytes, err := natsConn.Request(timeoutCtx, InteractiveSessionListQueryOp, request)
	if err != nil {
		errorMessage := "unable to request a list interactive sessions event"
		logger.WithError(err).Error(errorMessage)
		return nil, NewCacaoCommunicationError(errorMessage)
	}

	var interactiveSessionListModel InteractiveSessionListModel
	err = json.Unmarshal(responseBytes, &interactiveSessionListModel)
	if err != nil {
		errorMessage := "unable to unmarshal response JSON to interactive session list object"
		logger.WithError(err).Error(errorMessage)
		return nil, NewCacaoMarshalError(errorMessage)
	}

	serviceError := interactiveSessionListModel.GetServiceError()
	if serviceError != nil {
		logger.Error(serviceError)
		return nil, serviceError
	}

	return interactiveSessionListModel.GetInteractiveSessions(), nil
}

// CheckPrerequisites checks if the given interactive session can be created
func (client *natsInteractiveSessionClient) CheckPrerequisites(protocol InteractiveSessionProtocol, instanceAddress string, instanceAdminUsername string) (bool, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsInteractiveSessionClient.CheckPrerequisites",
	})

	if len(instanceAddress) == 0 {
		return false, NewCacaoInvalidParameterError("invalid InteractiveSession InstanceAddress")
	}

	if len(instanceAdminUsername) == 0 {
		return false, NewCacaoInvalidParameterError("invalid InteractiveSession InstanceAdminUsername")
	}

	if len(protocol) == 0 {
		return false, NewCacaoInvalidParameterError("invalid InteractiveSession Protocol")
	}

	if protocol == InteractiveSessionProtocolUnknown {
		return false, NewCacaoInvalidParameterError("invalid InteractiveSession Protocol")
	}

	natsConn, err := messaging.ConnectNatsForServiceClient(&client.natsConfig)
	if err != nil {
		errorMessage := "unable to connect to NATS"
		logger.WithError(err).Error(errorMessage)
		return false, NewCacaoCommunicationError(errorMessage)
	}

	defer natsConn.Disconnect()

	request := InteractiveSessionModel{
		Session: Session{
			SessionActor:    client.actor,
			SessionEmulator: client.emulator,
		},
		Owner:                 client.actor,
		InstanceAddress:       instanceAddress,
		InstanceAdminUsername: instanceAdminUsername,
		Protocol:              protocol,
	}

	timeout := time.Duration(client.natsConfig.RequestTimeout) * time.Second
	timeoutCtx, cancel := context.WithTimeout(client.ctx, timeout)
	defer cancel()

	responseBytes, err := natsConn.Request(timeoutCtx, InteractiveSessionCheckPrerequisitesQueryOp, request)
	if err != nil {
		errorMessage := "unable to request a check prerequisites interactive session event"
		logger.WithError(err).Error(errorMessage)
		return false, NewCacaoCommunicationError(errorMessage)
	}

	var session Session
	err = json.Unmarshal(responseBytes, &session)
	if err != nil {
		errorMessage := "unable to unmarshal response JSON to session object"
		logger.WithError(err).Error(errorMessage)
		return false, NewCacaoMarshalError(errorMessage)
	}

	serviceError := session.GetServiceError()
	if serviceError != nil {
		if strings.Contains(serviceError.ContextualError(), "Prerequisites are not met") {
			// soft err
			return false, nil
		}

		logger.Error(serviceError)
		return false, serviceError
	}
	return true, nil
}

// Create creates a new interactive session
func (client *natsInteractiveSessionClient) Create(interactiveSession InteractiveSession) (common.ID, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsInteractiveSessionClient.Create",
	})

	if len(interactiveSession.GetInstanceID()) == 0 {
		return common.ID(""), NewCacaoInvalidParameterError("invalid InteractiveSession InstanceID")
	}

	if len(interactiveSession.GetInstanceAddress()) == 0 {
		return common.ID(""), NewCacaoInvalidParameterError("invalid InteractiveSession InstanceAddress")
	}

	if len(interactiveSession.GetInstanceAdminUsername()) == 0 {
		return common.ID(""), NewCacaoInvalidParameterError("invalid InteractiveSession InstanceAdminUsername")
	}

	if interactiveSession.GetProtocol() == InteractiveSessionProtocolUnknown {
		return common.ID(""), NewCacaoInvalidParameterError("invalid InteractiveSession Protocol")
	}

	if len(interactiveSession.GetCloudID()) == 0 {
		return common.ID(""), NewCacaoInvalidParameterError("invalid InteractiveSession CloudID")
	}

	SenderNatsConfig := client.natsConfig
	SenderNatsConfig.ClientID = SenderNatsConfig.ClientID + "-" + string(common.NewID("Sender"))

	stanConn, err := messaging.ConnectStanForServiceClient(&SenderNatsConfig, &client.stanConfig)
	if err != nil {
		errorMessage := "unable to connect to NATS Streaming"
		logger.WithError(err).Error(errorMessage)
		return common.ID(""), NewCacaoCommunicationError(errorMessage)
	}

	defer stanConn.Disconnect()

	eventSourceNatsConfig := client.natsConfig
	eventSourceNatsConfig.ClientID = eventSourceNatsConfig.ClientID + "-" + string(common.NewID("EventSource"))

	eventSource := messaging.NewEventSource(eventSourceNatsConfig, client.stanConfig)
	if err != nil {
		errorMessage := "unable to create EventSource"
		logger.WithError(err).Error(errorMessage)
		return common.ID(""), NewCacaoCommunicationError(errorMessage)
	}

	interactiveSessionID := interactiveSession.GetID()
	if !interactiveSessionID.Validate() {
		interactiveSessionID = NewInteractiveSessionID()
	}

	request := InteractiveSessionModel{
		Session: Session{
			SessionActor:    client.actor,
			SessionEmulator: client.emulator,
		},
		ID:                    interactiveSessionID,
		Owner:                 client.actor,
		InstanceID:            interactiveSession.GetInstanceID(),
		InstanceAddress:       interactiveSession.GetInstanceAddress(),
		InstanceAdminUsername: interactiveSession.GetInstanceAdminUsername(),
		CloudID:               interactiveSession.GetCloudID(),
		Protocol:              interactiveSession.GetProtocol(),
	}

	eventsSubscribe := []common.EventType{
		InteractiveSessionCreatedEvent,
		InteractiveSessionCreateFailedEvent,
	}

	transactionID := messaging.NewTransactionID()

	responseChannel := make(chan InteractiveSessionModel)

	callback := func(ev common.EventType, ce cloudevents.Event) {
		if ev == InteractiveSessionCreatedEvent || ev == InteractiveSessionCreateFailedEvent {
			var interactiveSessionModel InteractiveSessionModel
			err := json.Unmarshal(ce.Data(), &interactiveSessionModel)
			if err != nil {
				errorMessage := "unable to unmarshal an interactive session create response"
				logger.WithError(err).Error(errorMessage)
				responseChannel <- InteractiveSessionModel{
					Session: Session{
						SessionActor:    client.actor,
						SessionEmulator: client.emulator,
						ServiceError:    NewCacaoMarshalError(errorMessage).GetBase(),
					},
				}
				return
			}

			responseChannel <- interactiveSessionModel
			return
		}

		errorMessage := fmt.Sprintf("unknown event type %s", ev)
		responseChannel <- InteractiveSessionModel{
			Session: Session{
				SessionActor:    client.actor,
				SessionEmulator: client.emulator,
				ServiceError:    NewCacaoCommunicationError(errorMessage).GetBase(),
			},
		}
	}

	listener := messaging.Listener{
		Callback:   callback,
		ListenOnce: true,
	}

	listenerID, err := eventSource.AddListenerMultiEventType(eventsSubscribe, transactionID, listener)
	if err != nil {
		errorMessage := "unable to add an event listener"
		logger.WithError(err).Error(errorMessage)
		return common.ID(""), NewCacaoCommunicationError(errorMessage)
	}
	defer eventSource.RemoveListenerByID(listenerID)

	err = stanConn.PublishWithTransactionID(InteractiveSessionCreateRequestedEvent, request, transactionID)
	if err != nil {
		errorMessage := "unable to publish a create interactive session event"
		logger.WithError(err).Error(errorMessage)
		return common.ID(""), NewCacaoCommunicationError(errorMessage)
	}

	// wait for response
	responseReceived, response := waitInteractiveSessionResponse(responseChannel, time.Duration(client.natsConfig.RequestTimeout)*time.Second)
	if responseReceived {
		serviceError := response.GetServiceError()
		if serviceError == nil {
			return interactiveSessionID, nil
		}

		logger.Error(serviceError)
		return common.ID(""), serviceError
	}

	// timed out
	logger.Error("unable to create an interactive session - interactive session service is not responding")
	return common.ID(""), NewCacaoTimeoutError("unable to create an interactive session - interactive session service is not responding")
}

// Deactivate deactivates the interactive session
func (client *natsInteractiveSessionClient) Deactivate(interactiveSessionID common.ID) error {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsInteractiveSessionClient.Deactivate",
	})

	if !interactiveSessionID.Validate() {
		return NewCacaoInvalidParameterError("invalid InteractiveSessionID")
	}

	SenderNatsConfig := client.natsConfig
	SenderNatsConfig.ClientID = SenderNatsConfig.ClientID + "-" + string(common.NewID("Sender"))

	stanConn, err := messaging.ConnectStanForServiceClient(&SenderNatsConfig, &client.stanConfig)
	if err != nil {
		errorMessage := "unable to connect to NATS Streaming"
		logger.WithError(err).Error(errorMessage)
		return NewCacaoCommunicationError(errorMessage)
	}

	defer stanConn.Disconnect()

	eventSourceNatsConfig := client.natsConfig
	eventSourceNatsConfig.ClientID = eventSourceNatsConfig.ClientID + "-" + string(common.NewID("EventSource"))

	eventSource := messaging.NewEventSource(eventSourceNatsConfig, client.stanConfig)
	if err != nil {
		errorMessage := "unable to create EventSource"
		logger.WithError(err).Error(errorMessage)
		return NewCacaoCommunicationError(errorMessage)
	}

	request := InteractiveSessionModel{
		Session: Session{
			SessionActor:    client.actor,
			SessionEmulator: client.emulator,
		},
		ID: interactiveSessionID,
	}

	eventsSubscribe := []common.EventType{
		InteractiveSessionDeactivatedEvent,
		InteractiveSessionDeactivateFailedEvent,
	}

	transactionID := messaging.NewTransactionID()

	responseChannel := make(chan InteractiveSessionModel)

	callback := func(ev common.EventType, ce cloudevents.Event) {
		if ev == InteractiveSessionDeactivatedEvent || ev == InteractiveSessionDeactivateFailedEvent {
			var interactiveSessionModel InteractiveSessionModel
			err := json.Unmarshal(ce.Data(), &interactiveSessionModel)
			if err != nil {
				errorMessage := "unable to unmarshal an interactive session deactivate response"
				logger.WithError(err).Error(errorMessage)
				responseChannel <- InteractiveSessionModel{
					Session: Session{
						SessionActor:    client.actor,
						SessionEmulator: client.emulator,
						ServiceError:    NewCacaoMarshalError(errorMessage).GetBase(),
					},
				}
				return
			}

			responseChannel <- interactiveSessionModel
			return
		}

		errorMessage := fmt.Sprintf("unknown event type %s", ev)
		responseChannel <- InteractiveSessionModel{
			Session: Session{
				SessionActor:    client.actor,
				SessionEmulator: client.emulator,
				ServiceError:    NewCacaoCommunicationError(errorMessage).GetBase(),
			},
		}
	}

	listener := messaging.Listener{
		Callback:   callback,
		ListenOnce: true,
	}

	listenerID, err := eventSource.AddListenerMultiEventType(eventsSubscribe, transactionID, listener)
	if err != nil {
		errorMessage := "unable to add an event listener"
		logger.WithError(err).Error(errorMessage)
		return NewCacaoCommunicationError(errorMessage)
	}
	defer eventSource.RemoveListenerByID(listenerID)

	err = stanConn.PublishWithTransactionID(InteractiveSessionDeactivateRequestedEvent, request, transactionID)
	if err != nil {
		errorMessage := "unable to publish a deactivate interactive session event"
		logger.WithError(err).Error(errorMessage)
		return NewCacaoCommunicationError(errorMessage)
	}

	// wait for response
	responseReceived, response := waitInteractiveSessionResponse(responseChannel, time.Duration(client.natsConfig.RequestTimeout)*time.Second)
	if responseReceived {
		serviceError := response.GetServiceError()
		if serviceError == nil {
			return nil
		}

		logger.Error(serviceError)
		return serviceError
	}

	// timed out
	logger.Error("unable to deactivate an interactive session - interactive session service is not responding")
	return NewCacaoTimeoutError("unable to deactivate an interactive session - interactive session service is not responding")
}
