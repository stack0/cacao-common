package service

import (
	"encoding/json"
	"fmt"
	"strings"
)

// TCFRegistryEntryType is the type of a tcf registry entry
type TCFRegistryEntryType string

const (
	// TCFRegistryEntryTypeUnknown is for an unknown registry entry type
	TCFRegistryEntryTypeUnknown TCFRegistryEntryType = ""
	// TCFRegistryEntryTypeRESTAPIEndpoint is for an api endpoint registry entry type
	TCFRegistryEntryTypeRESTAPIEndpoint TCFRegistryEntryType = "rest_api_endpoint"
	// TCFRegistryEntryTypeNATSMessageChannel is for a NATS message channel registry entry type
	TCFRegistryEntryTypeNATSMessageChannel TCFRegistryEntryType = "nats_message_channel"
)

// String returns the string representation of a TCFRegistryEntryType.
func (t TCFRegistryEntryType) String() string {
	return string(t)
}

// MarshalJSON returns the JSON representation of a TCFRegistryEntryType.
func (t TCFRegistryEntryType) MarshalJSON() ([]byte, error) {
	return json.Marshal(t.String())
}

// UnmarshalJSON converts the JSON representation of a TCFRegistryEntryType to the appropriate enumeration constant.
func (t *TCFRegistryEntryType) UnmarshalJSON(b []byte) error {
	// Template formats are represented as strings.
	var s string
	err := json.Unmarshal(b, &s)
	if err != nil {
		return err
	}

	// Validate and convert the value.
	switch strings.ToLower(s) {
	case "":
		*t = TCFRegistryEntryTypeUnknown
	case "rest_api_endpoint":
		*t = TCFRegistryEntryTypeRESTAPIEndpoint
	case "nats_message_channel":
		*t = TCFRegistryEntryTypeNATSMessageChannel
	default:
		return fmt.Errorf("invalid tcf registry entry type: %s", s)
	}

	return nil
}

// TCFRegistryClient is the client for TCF Registry
type TCFRegistryClient interface {
	List() ([]TCFRegistryEntry, error)
	Get(name string) (TCFRegistryEntry, error)
	Query(name string, queryParams map[string]string) (TCFRegistryQueryResult, error)

	Create(entry TCFRegistryEntry) error
	Update(entry TCFRegistryEntry) error
	UpdateFields(entry TCFRegistryEntry, updateFieldNames []string) error
	Delete(name string) error
}

// TCFRegistryQueryResult is the standard interface for all TCF Registry Query Result implementations
type TCFRegistryQueryResult interface {
	SessionContext

	GetEntryName() string
	GetDataType() string
	GetValue() interface{}
}

// TCFRegistryQueryResultModel is the tcf registry query result model
type TCFRegistryQueryResultModel struct {
	Session

	EntryName string      `json:"entry_name"`
	DataType  string      `json:"data_type"`
	Value     interface{} `json:"value"`
}

// GetEntryName returns the name of the tcf registry entry
func (r *TCFRegistryQueryResultModel) GetEntryName() string {
	return r.EntryName
}

// GetDataType returns the data type of the tcf registry query result
func (r *TCFRegistryQueryResultModel) GetDataType() string {
	return r.DataType
}

// GetValue returns the values of the tcf registry query result
func (r *TCFRegistryQueryResultModel) GetValue() interface{} {
	return r.Value
}

// TCFRegistryEntry is the standard interface for all TCF Registry Entry implementations
type TCFRegistryEntry interface {
	SessionContext

	GetPrimaryID() string // points to Name
	GetName() string
	GetDescription() string
	GetType() TCFRegistryEntryType
	GetQueryTarget() string
	GetQueryData() string
	GetQueryResultJSONPathFilter() string
	GetUpdateFieldNames() []string

	SetPrimaryID(name string)
	SetName(name string)
	SetDescription(description string)
	SetType(entryType TCFRegistryEntryType)
	SetQueryTarget(target string)
	SetQueryData(data string)
	SetQueryResultJSONPathFilter(jsonpath string)
	SetUpdateFieldNames(fieldNames []string)
}

// TCFRegistryEntryModel is the tcf registry entry model
type TCFRegistryEntryModel struct {
	Session

	Name                      string               `json:"name"`
	Description               string               `json:"description,omitempty"`
	Type                      TCFRegistryEntryType `json:"type"`
	QueryTarget               string               `json:"query_target"`
	QueryData                 string               `json:"query_data,omitempty"`
	QueryResultJSONPathFilter string               `json:"query_result_jsonpath_filter,omitempty"`

	UpdateFieldNames []string          `json:"update_field_names,omitempty"`
	QueryParams      map[string]string `json:"query_params,omitempty"`
}

// GetPrimaryID returns the primary ID of the tcf registry entry
func (r *TCFRegistryEntryModel) GetPrimaryID() string {
	return r.Name
}

// GetName returns the name of the tcf registry entry
func (r *TCFRegistryEntryModel) GetName() string {
	return r.Name
}

// GetDescription returns the description of the tcf registry entry
func (r *TCFRegistryEntryModel) GetDescription() string {
	return r.Description
}

// GetType returns the data type of the tcf registry entry
func (r *TCFRegistryEntryModel) GetType() TCFRegistryEntryType {
	return r.Type
}

// GetQueryTarget returns the query target of the tcf registry entry
func (r *TCFRegistryEntryModel) GetQueryTarget() string {
	return r.QueryTarget
}

// GetQueryData returns the query data of the tcf registry entry
func (r *TCFRegistryEntryModel) GetQueryData() string {
	return r.QueryData
}

// GetQueryResultJSONPathFilter returns the JSONPath filter that filters query result data
func (r *TCFRegistryEntryModel) GetQueryResultJSONPathFilter() string {
	return r.QueryResultJSONPathFilter
}

// GetUpdateFieldNames returns the field names to be updated
func (r *TCFRegistryEntryModel) GetUpdateFieldNames() []string {
	return r.UpdateFieldNames
}

// GetQueryParams returns the query params for query API
func (r *TCFRegistryEntryModel) GetQueryParams() map[string]string {
	return r.QueryParams
}

// SetPrimaryID sets the primary ID of the tcf registry entry
func (r *TCFRegistryEntryModel) SetPrimaryID(name string) {
	r.Name = name
}

// SetName sets the name of the tcf registry entry
func (r *TCFRegistryEntryModel) SetName(name string) {
	r.Name = name
}

// SetDescription sets the description of the tcf registry entry
func (r *TCFRegistryEntryModel) SetDescription(description string) {
	r.Description = description
}

// SetType sets the type of the tcf registry entry data
func (r *TCFRegistryEntryModel) SetType(dataType TCFRegistryEntryType) {
	r.Type = dataType
}

// SetQueryTarget sets the query target of the tcf registry entry
func (r *TCFRegistryEntryModel) SetQueryTarget(target string) {
	r.QueryTarget = target
}

// SetQueryData sets the data of the tcf registry entry
func (r *TCFRegistryEntryModel) SetQueryData(data string) {
	r.QueryData = data
}

// SetQueryResultJSONPathFilter sets the JSONPath filter that filters query result data
func (r *TCFRegistryEntryModel) SetQueryResultJSONPathFilter(jsonpath string) {
	r.QueryResultJSONPathFilter = jsonpath
}

// SetUpdateFieldNames sets the field names to be updated
func (r *TCFRegistryEntryModel) SetUpdateFieldNames(fieldNames []string) {
	r.UpdateFieldNames = fieldNames
}

// SetQueryParams sets the query params for query API
func (r *TCFRegistryEntryModel) SetQueryParams(queryParams map[string]string) {
	r.QueryParams = queryParams
}

// TCFRegistryEntryListItemModel is the element type in TCFRegistryEntryListModel
type TCFRegistryEntryListItemModel struct {
	Name                      string               `json:"name"`
	Description               string               `json:"description,omitempty"`
	Type                      TCFRegistryEntryType `json:"type"`
	QueryTarget               string               `json:"query_target"`
	QueryData                 string               `json:"query_data,omitempty"`
	QueryResultJSONPathFilter string               `json:"query_result_jsonpath_filter,omitempty"`
}

// TCFRegistryEntryListModel is the tcf registry entry list model
type TCFRegistryEntryListModel struct {
	Session

	Entries []TCFRegistryEntryListItemModel `json:"entries"`
}

// GetEntries returns registry entries
func (r *TCFRegistryEntryListModel) GetEntries() []TCFRegistryEntry {
	entries := make([]TCFRegistryEntry, 0, len(r.Entries))

	for _, entry := range r.Entries {
		registryEntryModel := TCFRegistryEntryModel{
			Session:                   r.Session,
			Name:                      entry.Name,
			Description:               entry.Description,
			Type:                      entry.Type,
			QueryTarget:               entry.QueryTarget,
			QueryData:                 entry.QueryData,
			QueryResultJSONPathFilter: entry.QueryResultJSONPathFilter,
		}

		entries = append(entries, &registryEntryModel)
	}

	return entries
}
