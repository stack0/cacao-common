package service

import (
	"fmt"
	"gitlab.com/cyverse/cacao-common/common"
)

// DependencyMediatorClient is client interface for dependency mediator which checks and enforce for dependency relationship between different objects.
// e.g. a Credential cannot be deleted when there is a deployment dependent on it.
type DependencyMediatorClient interface {
	DeleteCredential(session Session, credID string) (DepCheckDeletionResult, error)
	DeleteTemplate(session Session, template common.ID) (DepCheckDeletionResult, error)
	DeleteProvider(session Session, provider common.ID) (DepCheckDeletionResult, error)
	DeleteWorkspace(session Session, workspace common.ID) (DepCheckDeletionResult, error)
	// TODO add capability to list all dependents of an object
}

// DepCheckDeletionResult is the result of a deletion operation with dependency check
type DepCheckDeletionResult struct {
	HasDependent bool
	Deleted      bool
}

// ToError is a utility method for checking the deletion result, it will not return error if object is deleted.
func (r DepCheckDeletionResult) ToError(objectType string, objectID string) error {
	if r.Deleted {
		return nil
	}
	if r.HasDependent {
		return fmt.Errorf("%s %s cannot be deleted, %s has dependent(s)", objectType, objectID, objectType)
	}
	return fmt.Errorf("fail to delete %s %s, invalid deletion result", objectType, objectID)
}
