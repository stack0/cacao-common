package service

import (
	"context"
	"encoding/json"
	"errors"
	"time"

	cloudevents "github.com/cloudevents/sdk-go/v2"
	"github.com/cloudevents/sdk-go/v2/event"
	"github.com/nats-io/nats.go"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging"
)

const (
	NatsSubjectUserConfigsGet   string = NatsSubjectUsers + ".getconfigs"
	NatsSubjectUserFavoritesGet string = NatsSubjectUsers + ".getfavorites"
	NatsSubjectUserRecentsGet   string = NatsSubjectUsers + ".getrecents"
	NatsSubjectUserSettingsGet  string = NatsSubjectUsers + ".getsettings"

	EventUserConfigSetRequested      common.EventType = common.EventTypePrefix + "UserConfigSetRequested"
	EventUserFavoriteAddRequested    common.EventType = common.EventTypePrefix + "UserFavoriteAddRequested"
	EventUserFavoriteDeleteRequested common.EventType = common.EventTypePrefix + "UserFavoriteDeleteRequested"
	EventUserRecentSetRequested      common.EventType = common.EventTypePrefix + "UserRecentSetRequested"

	EventUserConfigSet       common.EventType = common.EventTypePrefix + "UserConfigSet"
	EventUserFavoriteAdded   common.EventType = common.EventTypePrefix + "UserFavoriteAdded"
	EventUserFavoriteDeleted common.EventType = common.EventTypePrefix + "UserFavoriteDeleted"
	EventUserRecentSet       common.EventType = common.EventTypePrefix + "UserRecentSet"

	EventUserConfigSetError      common.EventType = common.EventTypePrefix + "UserConfigSetError"
	EventUserFavoriteAddError    common.EventType = common.EventTypePrefix + "UserFavoriteAddError"
	EventUserFavoriteDeleteError common.EventType = common.EventTypePrefix + "UserFavoriteDeleteError"
	EventUserRecentSetError      common.EventType = common.EventTypePrefix + "UserRecentSetError"
)

func (client *natsUserClient) userSettingsRequest(subject string, username string, key string, value string) (*event.Event, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsUserClient.userSettingsRequest",
	})
	logger.Trace("Get() started")

	if username == "" {
		return nil, errors.New(UserUsernameNotSetError)
	}

	logger.WithField("natsUrl", client.natsConfig.URL).Trace("connecting to nats")
	nc, err := nats.Connect(client.natsConfig.URL, nats.MaxReconnects(client.maxReconnect), nats.ReconnectWait(client.reconnectWait))
	if err != nil {
		return nil, err
	}
	defer nc.Close()

	logger.Trace("creating cloud event")
	var data = common.UserSettingsRequest{
		Username: username,
		Key:      key,
		Value:    value,
	}
	ce, err := messaging.CreateCloudEvent(data, subject, client.natsConfig.ClientID)
	if err != nil {
		return nil, err
	}
	payload, err := json.Marshal(ce)
	if err != nil {
		return nil, err
	}

	logger.Trace("sending request with context to " + subject)
	newctx, cancel := context.WithTimeout(client.ctx, client.requestTimeout)
	msg, err := nc.RequestWithContext(newctx, subject, payload)
	cancel() // no longer need the context, so cancel at this point
	if err != nil {
		return nil, err
	}

	// convert the message to a cloud event
	logger.Trace("received event, converting and unmarshalling")
	ce, err = messaging.ConvertNats(msg)
	if err != nil {
		return nil, err
	}
	return &ce, err
}

func (client *natsUserClient) GetConfigs(username string) (*map[string]string, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsUserClient.GetConfigs",
	})
	ce, err := client.userSettingsRequest(NatsSubjectUserConfigsGet, username, "", "")
	if err != nil {
		return nil, err
	}
	logger.Debugf("%+v", ce.Data())
	var configs map[string]string
	err = json.Unmarshal(ce.Data(), &configs)
	if err != nil {
		return nil, err
	}
	return &configs, nil
}

func (client *natsUserClient) GetFavorites(username string) (*map[string][]string, error) {
	ce, err := client.userSettingsRequest(NatsSubjectUserFavoritesGet, username, "", "")
	if err != nil {
		return nil, err
	}
	var favorites map[string][]string
	err = json.Unmarshal(ce.Data(), &favorites)
	if err != nil {
		return nil, err
	}
	return &favorites, nil
}

func (client *natsUserClient) GetRecents(username string) (*map[string]string, error) {
	ce, err := client.userSettingsRequest(NatsSubjectUserRecentsGet, username, "", "")
	if err != nil {
		return nil, err
	}
	var recents map[string]string
	err = json.Unmarshal(ce.Data(), &recents)
	if err != nil {
		return nil, err
	}
	return &recents, nil
}

func (client *natsUserClient) GetSettings(username string) (*common.UserSettings, error) {
	ce, err := client.userSettingsRequest(NatsSubjectUserSettingsGet, username, "", "")
	if err != nil {
		return nil, err
	}
	var us common.UserSettings
	err = json.Unmarshal(ce.Data(), &us)
	if err != nil {
		return nil, err
	}
	return &us, nil
}

func (client *natsUserClient) publishSettingsRequestEvent(username string, key string, value string, eventOpType common.EventType, waitForCompletion bool) error {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsUserClient.publishSettingsRequestEvent",
	})
	logger.WithFields(log.Fields{
		"username":          username,
		"eventOpType":       string(eventOpType),
		"waitForCompletion": waitForCompletion,
	}).Trace("publishSettingsRequestEvent() called")

	SenderNatsConfig := client.natsConfig
	SenderNatsConfig.ClientID = SenderNatsConfig.ClientID + "-" + string(common.NewID("Sender"))

	stanConn, err := messaging.ConnectStanForServiceClient(&SenderNatsConfig, &client.stanConfig)
	if err != nil {
		errorMessage := "unable to connect to NATS Streaming"
		logger.WithError(err).Error(errorMessage)
		return NewCacaoCommunicationError(errorMessage)
	}

	defer stanConn.Disconnect()

	eventSourceNatsConfig := client.natsConfig
	eventSourceNatsConfig.ClientID = eventSourceNatsConfig.ClientID + "-" + string(common.NewID("EventSource"))

	eventSource := messaging.NewEventSource(eventSourceNatsConfig, client.stanConfig)
	if err != nil {
		errorMessage := "unable to create EventSource"
		logger.WithError(err).Error(errorMessage)
		return NewCacaoCommunicationError(errorMessage)
	}

	transactionID := messaging.NewTransactionID()
	var data = common.UserSettingsRequest{
		Username: username,
		Key:      key,
		Value:    value,
	}
	if waitForCompletion {

		// note, since we need to handle a custom cancellation process, rather than
		// a simple cancel or timeout through context, let's just setup a context with
		// cancel enabled
		_, cancel := context.WithCancel(client.ctx)
		responseChannel := make(chan UserModel)

		handler := func(ev common.EventType, ce cloudevents.Event) {
			var eventUser UserModel
			err := json.Unmarshal(ce.Data(), &eventUser)
			if err != nil {
				errorMessage := "unable to unmarshal a user"
				logger.WithError(err).Error(errorMessage)
				responseChannel <- UserModel{
					Session: Session{
						SessionActor:    client.actor,
						SessionEmulator: client.emulator,
						ServiceError:    NewCacaoMarshalError(errorMessage).GetBase(),
					},
				}
				return
			}
			responseChannel <- eventUser
		}

		var listenerID messaging.ListenerID

		// listens to different event type depends on event operation
		switch eventOpType {
		case EventUserConfigSetRequested:
			listenerID, err = client.eventSrc.AddListenerMultiEventType([]common.EventType{EventUserConfigSet, EventUserConfigSetError},
				transactionID, messaging.Listener{Callback: handler, ListenOnce: true})
		case EventUserFavoriteAddRequested:
			listenerID, err = client.eventSrc.AddListenerMultiEventType([]common.EventType{EventUserFavoriteAdded, EventUserFavoriteAddError},
				transactionID, messaging.Listener{Callback: handler, ListenOnce: true})
		case EventUserFavoriteDeleteRequested:
			listenerID, err = client.eventSrc.AddListenerMultiEventType([]common.EventType{EventUserFavoriteDeleted, EventUserFavoriteDeleteError},
				transactionID, messaging.Listener{Callback: handler, ListenOnce: true})
		case EventUserRecentSetRequested:
			listenerID, err = client.eventSrc.AddListenerMultiEventType([]common.EventType{EventUserRecentSet, EventUserRecentSetError},
				transactionID, messaging.Listener{Callback: handler, ListenOnce: true})
		default:
			panic("unknown op") // unknown op must not be passed
		}
		if err != nil {
			cancel()
			return err
		}
		defer eventSource.RemoveListenerByID(listenerID)

		logger.Trace("sync publishing the event " + eventOpType)
		err = stanConn.PublishWithTransactionID(eventOpType, data, transactionID)
		if err != nil {
			cancel()
			errorMessage := "unable to publish a update user event"
			logger.WithError(err).Error(errorMessage)
			return NewCacaoCommunicationError(errorMessage)
		}
		defer cancel()

		// wait for response
		responseReceived, response := waitUserResponse(responseChannel, time.Duration(client.requestTimeout)*time.Second)
		if responseReceived {
			serviceError := response.GetServiceError()
			if serviceError == nil {
				return nil
			}

			logger.Error(serviceError)
			return serviceError
		}

		// timed out
		logger.Error("unable to create a user - user service is not responding")
		return NewCacaoTimeoutError("unable to create a user - user service is not responding")
		// }
	} else {
		logger.Trace("async publishing the event " + eventOpType)
		err = stanConn.PublishWithTransactionID(eventOpType, data, transactionID)
		if err != nil {
			errorMessage := "unable to publish a update user event"
			logger.WithError(err).Error(errorMessage)
			return NewCacaoCommunicationError(errorMessage)
		}
	}
	return nil
}

func (usersvc *natsUserClient) SetConfig(username string, key string, value string) error {
	log.Trace("natsUserClient.SetConfig() start")
	return usersvc.publishSettingsRequestEvent(username, key, value, EventUserConfigSetRequested, true)
}

func (usersvc *natsUserClient) AddFavorite(username string, key string, value string) error {
	log.Trace("natsUserClient.AddFavorite() start")
	return usersvc.publishSettingsRequestEvent(username, key, value, EventUserFavoriteAddRequested, true)
}

func (usersvc *natsUserClient) DeleteFavorite(username string, key string, value string) error {
	log.Trace("natsUserClient.DeleteFavorite() start")
	return usersvc.publishSettingsRequestEvent(username, key, value, EventUserFavoriteDeleteRequested, true)
}

func (usersvc *natsUserClient) SetRecent(username string, key string, value string) error {
	log.Trace("natsUserClient.SetRecent() start")
	return usersvc.publishSettingsRequestEvent(username, key, value, EventUserRecentSetRequested, true)
}
