// Package format -- this file contains utility functions for dealing with formats
package format

import (
	"bytes"
	"encoding/json"
	"fmt"
	"strings"

	"github.com/xeipuuv/gojsonschema"
)

// DecodeJSONStrictly will take data and a target ojbect and return an error if extra json fields are found in the target
// This function can be used for simple json format validation and decoding
func DecodeJSONStrictly(data []byte, target interface{}) error {
	if len(data) == 0 {
		return fmt.Errorf("data is empty")
	}

	decoder := json.NewDecoder(bytes.NewReader(data))
	decoder.DisallowUnknownFields()
	return decoder.Decode(target)
}

// ValidateJSONSchema validates json data using a schema and returns an error if the data is not valid
func ValidateJSONSchema(data []byte, schema []byte) error {
	dataLoader := gojsonschema.NewStringLoader(string(data))
	schemaLoader := gojsonschema.NewStringLoader(string(schema))

	result, err := gojsonschema.Validate(schemaLoader, dataLoader)
	if err != nil {
		return err
	}

	if !result.Valid() {
		msgBuilder := strings.Builder{}

		_, err := msgBuilder.WriteString("the data is not valid.")
		if err != nil {
			return err
		}
		for _, desc := range result.Errors() {
			_, err = msgBuilder.WriteString("\n- " + desc.String())
			if err != nil {
				return err
			}
		}
		return fmt.Errorf(msgBuilder.String())
	}

	return nil
}
