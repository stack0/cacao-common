package http

import (
	"encoding/json"
	"fmt"
	"strings"
	"time"
)

// KeyValue represents a key-value pair returned by the HTTP API
type KeyValue struct {
	Key   string `json:"key"`
	Value string `json:"value"`
}

// Parameters represents an object containing a list of key-value pairs.
type Parameters struct {
	Parameters []KeyValue `json:"parameters"`
}

// DeploymentResource represents a resource within a Deployment object returned by the HTTP API.
type DeploymentResource struct {
	ID                  string                 `json:"id"`
	Type                string                 `json:"type"`
	ProviderType        string                 `json:"provider_type"`
	Provider            string                 `json:"provider"`
	Attributes          map[string]interface{} `json:"attributes"`
	SensitiveAttributes interface{}            `json:"sensitive_attributes,omitempty"`
	AvailableActions    []string               `json:"available_actions"`
}

// DeploymentStateView represents a view of the state of a deployment.
type DeploymentStateView struct {
	Resources []DeploymentResource `json:"resources"`
}

// DeploymentBuild represents a build within a Deployment object returned by the HTTP API.
type DeploymentBuild struct {
	ID           string     `json:"id"`
	Owner        string     `json:"owner"`
	DeploymentID string     `json:"deployment_id"`
	Parameters   []KeyValue `json:"parameters"`
	Status       string     `json:"status"`
	Start        time.Time  `json:"start"`
	End          time.Time  `json:"end,omitempty"`
}

// DeploymentRun represents a run within a Deployment object returned by the HTTP API.
type DeploymentRun struct {
	ID               string              `json:"id"`
	Owner            string              `json:"owner"`
	DeploymentID     string              `json:"deployment_id"`
	BuildID          string              `json:"build_id"`
	Parameters       []KeyValue          `json:"parameters,omitempty"`
	CloudCredentials []string            `json:"cloud_credentials,omitempty"`
	GitCredentialID  string              `json:"git_credential_id,omitempty"`
	LastState        DeploymentStateView `json:"last_state,omitempty"`
	Status           string              `json:"status"`
	StatusMsg        string              `json:"status_msg"`
	Start            time.Time           `json:"start"`
	End              time.Time           `json:"end,omitempty"`
}

// Deployment represents a deployment returned by the HTTP API.
type Deployment struct {
	ID                string          `json:"id"`
	Owner             string          `json:"owner"`
	Name              string          `json:"name"`
	Description       string          `json:"description"`
	WorkspaceID       string          `json:"workspace_id"`
	TemplateID        string          `json:"template_id"`
	PrimaryProviderID string          `json:"primary_provider_id"`
	CreatedAt         time.Time       `json:"created_at"`
	UpdatedAt         time.Time       `json:"updated_at"`
	CurrentStatus     string          `json:"current_status"`
	PendingStatus     string          `json:"pending_status"`
	StatusMsg         string          `json:"status_msg"`
	Parameters        []KeyValue      `json:"parameters,omitempty"`
	CloudCredentials  []string        `json:"cloud_credentials,omitempty"`
	GitCredentialID   string          `json:"git_credential_id,omitempty"`
	CurrentBuild      DeploymentBuild `json:"current_build,omitempty"`
	CurrentRun        DeploymentRun   `json:"current_run,omitempty"`
}

// DeploymentUpdate is used for updating deployment, it contains fields that can potentially be updated.
// All fields should be nullable(ptr or slice), so that fields absent from the http request can be unmarshalled as nil.
// Whether these fields can truly be updated depends on the deployment services.
type DeploymentUpdate struct {
	Owner             *string    `json:"owner"`
	Name              *string    `json:"name"`
	Description       *string    `json:"description"`
	WorkspaceID       *string    `json:"workspace_id"`
	TemplateID        *string    `json:"template_id"`
	PrimaryProviderID *string    `json:"primary_provider_id"`
	Parameters        []KeyValue `json:"parameters,omitempty"`
	CloudCredentials  []string   `json:"cloud_credentials,omitempty"`
	GitCredentialID   *string    `json:"git_credential_id,omitempty"`
}

// StorageAction indicates an action that can be performed on a storage deployment.
type StorageAction string

// Enumeration constants for StorageAction.
const (
	// StorageActionAttach is used to attach storage to a running instance.
	StorageActionAttach StorageAction = ""

	// StorageActionDetach is used to detach storage from a running instance.
	StorageActionDetach StorageAction = "detach"
)

// String returns the string representation of a storage action.
func (sa StorageAction) String() string {
	// The switch allows us to return something other than the empty string for the default enumeration constant.
	switch sa {
	case StorageActionAttach:
		return "attach"
	default:
		return string(sa)
	}
}

// MarshalJSON returns the JSON representation of a StorageAction.
func (sa StorageAction) MarshalJSON() ([]byte, error) {
	return json.Marshal(sa.String())
}

// UnmarshalJSON converts the JSON representation of a StorageAction to the appropriate enumeration constant.
func (sa *StorageAction) UnmarshalJSON(b []byte) error {
	// Storage actions are represented as strings in JSON.
	var s string
	err := json.Unmarshal(b, &s)
	if err != nil {
		return err
	}

	// Validate and convert the value.
	switch strings.ToLower(s) {
	case "":
		*sa = StorageActionAttach
	case "attach":
		*sa = StorageActionAttach
	case "detach":
		*sa = StorageActionDetach
	default:
		return fmt.Errorf("invalid storage action: %s", s)
	}

	return nil
}

// AttachParamValue returns the value of the "attach" parameter to use for the storage action.
func (sa StorageAction) AttachParamValue() string {
	switch sa {
	case StorageActionAttach:
		return "true"
	default:
		return "false"
	}
}

// StorageActionRequest is used to attach storage to an instance or detach storage from an instance.
type StorageActionRequest struct {
	Action     StorageAction `json:"action"`
	InstanceID string        `json:"instance_id"`
}
