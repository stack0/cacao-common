package http

import (
	"time"

	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
)

// InteractiveSession represents an interactive session returned by the HTTP API.
type InteractiveSession struct {
	ID                    common.ID                          `json:"id"`
	Owner                 string                             `json:"owner"`
	InstanceID            string                             `json:"instance_id"`
	InstanceAddress       string                             `json:"instance_address"`
	InstanceAdminUsername string                             `json:"instance_admin_username"`
	CloudID               string                             `json:"cloud_id"`
	Protocol              service.InteractiveSessionProtocol `json:"protocol"`
	RedirectURL           string                             `json:"redirect_url"`
	State                 service.InteractiveSessionState    `json:"state"`
	CreatedAt             time.Time                          `json:"created_at"`
	UpdatedAt             time.Time                          `json:"updated_at"`
}

// InteractiveSessionPrerequisiteCheck represents a result type of prerequisites check returned by the HTTP API.
type InteractiveSessionPrerequisiteCheck struct {
	InstanceAddress       string                             `json:"instance_address"`
	InstanceAdminUsername string                             `json:"instance_admin_username"`
	Protocol              service.InteractiveSessionProtocol `json:"protocol"`
	Result                bool                               `json:"result"`
}
