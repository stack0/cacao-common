package http

import (
	"time"

	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
)

// JS2User represents a JetStream2 User returned by the HTTP API.
type JS2User struct {
	Owner        string    `json:"owner"`
	TACCUsername string    `json:"tacc_username"`
	RetrievedAt  time.Time `json:"retrieved_at"`
}

// JS2Project represents a JetStream2 Project returned by the HTTP API.
type JS2Project struct {
	ID          common.ID               `json:"id"`
	Owner       string                  `json:"owner"`
	Title       string                  `json:"title"`
	Description string                  `json:"description,omitempty"`
	PI          service.JS2PI           `json:"pi"`
	Allocations []service.JS2Allocation `json:"allocations"`
	RetrievedAt time.Time               `json:"retrieved_at"`
}
