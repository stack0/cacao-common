package http

import (
	"time"

	"gitlab.com/cyverse/cacao-common/common"
)

// Workspace represents a workspace returned by the HTTP API.
type Workspace struct {
	ID                common.ID `json:"id"`
	Owner             string    `json:"owner"`
	Name              string    `json:"name"`
	Description       string    `json:"description,omitempty"`
	DefaultProviderID common.ID `json:"default_provider_id,omitempty"`
	CreatedAt         time.Time `json:"created_at"`
	UpdatedAt         time.Time `json:"updated_at"`
}
