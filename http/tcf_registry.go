package http

import (
	"gitlab.com/cyverse/cacao-common/service"
)

// TCFRegistryQueryResult represents a tcf registry query result returned by the HTTP API.
type TCFRegistryQueryResult struct {
	EntryName string      `json:"entry_name"`
	DataType  string      `json:"data_type"`
	Value     interface{} `json:"value"`
}

// TCFRegistry represents a tcf registry entry returned by the HTTP API.
type TCFRegistryEntry struct {
	Name                      string                       `json:"name"`
	Description               string                       `json:"description,omitempty"`
	Type                      service.TCFRegistryEntryType `json:"type"`
	QueryTarget               string                       `json:"query_target"`
	QueryData                 string                       `json:"query_data,omitempty"`
	QueryResultJSONPathFilter string                       `json:"query_result_jsonpath_filter,omitempty"`

	QueryParams map[string]string `json:"query_params,omitempty"`
}
