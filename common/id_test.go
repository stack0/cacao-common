package common

import (
	"testing"

	"github.com/rs/xid"
	"github.com/stretchr/testify/assert"
)

func TestNewID(t *testing.T) {
	prefix := "user"
	id := NewID(prefix)
	assert.Equal(t, string(id[:len(prefix)+1]), prefix+"-")
	_, err := xid.FromString(string(id)[len(prefix)+1:])
	assert.NoError(t, err)
}

func TestNewIDFromXID(t *testing.T) {
	prefix := "prefix"
	newXID := xid.New()
	id := NewIDFromXID(prefix, newXID)
	assert.Equal(t, prefix+"-"+newXID.String(), id.String())
}

func TestID_Prefix(t *testing.T) {
	id := NewID("prefix")
	assert.Equal(t, "prefix", id.Prefix())
}

func TestIDToXID(t *testing.T) {
	earlierXID := xid.New()
	id := NewID("prefix")
	xid1, err := id.XID()
	assert.NoError(t, err)

	xid2 := id.XIDMustParse()
	assert.Equal(t, 0, xid1.Compare(xid2))
	assert.True(t, earlierXID.Compare(xid1) < 0)
	assert.True(t, xid1.Compare(earlierXID) > 0)

	newerXID := xid.New()
	assert.True(t, newerXID.Compare(xid1) > 0)
	assert.True(t, xid1.Compare(newerXID) < 0)
}

func TestID_Validate(t *testing.T) {
	tests := []struct {
		name string
		id   ID
		want bool
	}{
		{"normal", ID(NewID("foobar")), true},
		{"empty_prefix", ID(NewID("")), false},
		{"uppercase_in_prefix", ID(NewID("FooBar")), false},
		{"number_in_prefix", ID(NewID("foo123")), false},
		{"bad_xid1", ID("foo-c2iq0rg-oikfkguiqb90"), false},
		{"bad_xid2", ID("foo-c2iq0rg8oikfkg=iqb90"), false},
		{"bad_xid3", ID("foo-c2iq0rg8oikfkguiqb90aaa"), false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.id.Validate(); got != tt.want {
				t.Errorf("ID.Validate() = %v, want %v", got, tt.want)
			}
		})
	}
}
