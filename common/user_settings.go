package common

// UserSettings contain the Settings, Favorites, and Recents for a user
type UserSettings struct {
	Configs   map[string]string   `json:"configs,omitempty"`
	Favorites map[string][]string `json:"favorites,omitempty"`
	Recents   map[string]string   `json:"recents,omitempty"`
}

// UserSettingsRequest is used to pass requests over nats
type UserSettingsRequest struct {
	Username string `json:"username"`
	Key      string `json:"key"`
	Value    string `json:"value"`
}
